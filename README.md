Substrate Internals
===================

A bunch of messy notes covering the internals of some of the
[Substrate](https://github.com/paritytech/substrate) framework components.

This work mostly covers stuff that you may be interested in while working **ON**
Substrate, as opposed to work **WITH** Substrate.

If you are mostly interested in using the framework to create your own
blockchain then I suggested to go first through the
[official documentation](https://docs.substrate.io)
and then come back here when and if you decide to start hacking with the
Substrate codebase itself.

Important
---------

- **So far, this work doesn't want to be a reference for the general public**.
  The quality of some of the notes is still really low and there are a lot of
  assumption about your knowledge of the framework.
- Substrate is a fairly fast moving target and that is especially true with
  respect to its internals, some of the notes may not be 100% accurate.
- I'm not a native english speaker, please be kind :-). Any help fixing typos
  and stuff is highly appreciated.

Feel free to contribute to improve the notes quality ♥♥♥

References
----------

- [Stack exchange](https://substrate.stackexchange.com)
- [Official documentation](https://docs.substrate.io)
- [Rust API documentation](https://paritytech.github.io/substrate/master/sc_service/index.html)
