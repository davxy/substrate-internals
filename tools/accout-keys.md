Account Keys
============

First thing to keep in mind is that an SS58 address is just a sequence of bytes encoded in base 58.
This sequence can be dissected in three components:

- Network-Id
- Public Key (verbatim in case of sr25519/ed25519 or hashed in case of secp255k1 key)
- Checksum

The Network-Id is what is commonly called *prefix" in the official SS58
[registry](https://github.com/paritytech/ss58-registry/blob/main/ss58-registry.json).

For example a generic Substrate address has a prefix set to
[42](https://github.com/paritytech/ss58-registry/blob/809d71dd2da2683772e15a0432b93b5d23305760/ss58-registry.json#L374).

The same public key can be used to generate different addresses by changing the prefix (the checksum
changes as a consequence).

An example may help.

Generate an sr25519 keypair via subkey tool:

```bash
    $ subkey generate --scheme sr25519
    Secret phrase `trip embody match refuse exchange cabin brand try write eight lamp endorse` is account:
      Secret seed:      0xa444d232c8a70098d94add70f9ceb4702c58aca4558966e2ce7d59c153306389
      Public key (hex): 0x6a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e79
      Account ID:       0x6a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e79
      SS58 Address:     5EUAFw4JM21WgrjqEqBPXGSZ2zUCkxdCiaNTeuzC7t8twpiD
```

Convert the SS58 address from base 58 to hex to inspect the content (for example using this website).

```
    B58: 5EUAFw4JM21WgrjqEqBPXGSZ2zUCkxdCiaNTeuzC7t8twpiD
    Hex: 2a6a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e7973b2
```

The hex version can be decomposed as follows:

```
    2a = Network id (42 in decimal, i.e generic Substrate network)
    6a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e79 = sr25519 Public Key
    73b2 = checksum
```

The very same public key can be part of an address used in a different network.

For example, for Polkadot:

```bash
    $ subkey inspect --network polkadot --scheme sr25519 "trip embody match refuse exchange cabin brand try write eight lamp endorse"
    Secret phrase `trip embody match refuse exchange cabin brand try write eight lamp endorse` is account:
      Secret seed:      0xa444d232c8a70098d94add70f9ceb4702c58aca4558966e2ce7d59c153306389
      Public key (hex): 0x6a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e79
      Account ID:       0x6a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e79
      SS58 Address:     13QTQGKNCoGz8PkMCUEPfRGhtcTrTGBLo56wpCyYfyAR89hu
```

Convert the SS58 address from base 58 to hex:

```
    B58: 13QTQGKNCoGz8PkMCUEPfRGhtcTrTGBLo56wpCyYfyAR89hu
    Hex: 006a5bfc392d5479b306d85c1e03766207b5443466f8e4668eedb97c26918e2e79259c
```

Note that the public key component is still the same, while the Network-ID and the Checksum is
changed (Network id is now set to 0x00, i.e. the Polkadot prefix here).
