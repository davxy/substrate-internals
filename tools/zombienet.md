ZombieNet
=========

Alternative to `polkadot-launch`.

Manual Approach
---------------

- Parachain Registration

Automating
----------

- Orchestration of different components
- ZombieNet has been used to setup more complex test scenarios

Polkadot Launch
---------------

Is possible to focus on a single

- Polkadot launch nodes are run as local processes
- Supports only the built-in validators

ZombieNet
---------

- It supports an arbitrary number of validators
- Is meant to be a replacement
- Can be used within CI tests

- There is also a package in the repo

- The test file has *.feature extension

- Q - Is that used in the CI? Where?
- For the moment is based on regex interpretation

References
----------

- https://paritytech.github.io/zombienet/docs/book/
