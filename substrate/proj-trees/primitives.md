https://github.com/paritytech/substrate/issues/8692

PRIMITIVES
==========

### API

- Crates: sp-api, sp-api-proc-macro
- Interface between the node and the runtime. Every call that goes into the
  runtime is done with a runtime api.
- Substrate users can define its own api using `decl_runtime_apis!` and
  `impl_runtime_apis!`.
- Declaration of a runtime api is normally done outside of a runtime, while the
  implementation has to be done in the runtime.

primitives/api
├── Cargo.toml
├── proc-macro
│   ├── Cargo.toml
│   └── src
│       ├── decl_runtime_apis.rs
│       ├── impl_runtime_apis.rs
│       ├── lib.rs
│       ├── mock_impl_runtime_apis.rs
│       └── utils.rs
├── README.md
├── src
│   └── lib.rs
└── test
    ├── benches
    │   └── bench.rs
    ├── Cargo.toml
    └── tests
        ├── decl_and_impl.rs
        ├── runtime_calls.rs
        ├── trybuild.rs
        └── ui
            ├── adding_self_parameter.rs
            ├── adding_self_parameter.stderr
            ├── changed_in_no_default_method.rs
            ├── changed_in_no_default_method.stderr
            ├── changed_in_unknown_version.rs
            ├── changed_in_unknown_version.stderr
            ├── declaring_old_block.rs
            ├── declaring_old_block.stderr
            ├── declaring_own_block_with_different_name.rs
            ├── declaring_own_block_with_different_name.stderr
            ├── empty_impl_runtime_apis_call.rs
            ├── empty_impl_runtime_apis_call.stderr
            ├── impl_incorrect_method_signature.rs
            ├── impl_incorrect_method_signature.stderr
            ├── impl_two_traits_with_same_name.rs
            ├── impl_two_traits_with_same_name.stderr
            ├── invalid_api_version_2.rs
            ├── invalid_api_version_2.stderr
            ├── invalid_api_version_3.rs
            ├── invalid_api_version_3.stderr
            ├── invalid_api_version.rs
            ├── invalid_api_version.stderr
            ├── missing_block_generic_parameter.rs
            ├── missing_block_generic_parameter.stderr
            ├── missing_path_for_trait.rs
            ├── missing_path_for_trait.stderr
            ├── mock_advanced_block_id_by_value.rs
            ├── mock_advanced_block_id_by_value.stderr
            ├── mock_advanced_missing_blockid.rs
            ├── mock_advanced_missing_blockid.stderr
            ├── mock_only_one_block_type.rs
            ├── mock_only_one_block_type.stderr
            ├── mock_only_one_self_type.rs
            ├── mock_only_one_self_type.stderr
            ├── mock_only_self_reference.rs
            ├── mock_only_self_reference.stderr
            ├── no_default_implementation.rs
            ├── no_default_implementation.stderr
            ├── type_reference_in_impl_runtime_apis_call.rs
            └── type_reference_in_impl_runtime_apis_call.stderr

**Dependencies**

sp-api v4.0.0-dev
├── hash-db v0.15.2
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sp-api-proc-macro v4.0.0-dev (proc-macro)
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
├── sp-state-machine v0.10.0
├── sp-std v4.0.0
├── sp-version v4.0.0-dev
└── thiserror v1.0.30
[dev-dependencies]
└── sp-test-primitives v2.0.0

### Application-Crypto

- Crate: sp-application-crypto
- Traits and macros for constructing application specific strongly typed crypto
  wrappers.

primitives/application-crypto
├── Cargo.toml
├── README.md
├── src
│   ├── ecdsa.rs
│   ├── ed25519.rs
│   ├── lib.rs
│   ├── sr25519.rs
│   └── traits.rs
└── test
    ├── Cargo.toml
    └── src
        ├── ecdsa.rs
        ├── ed25519.rs
        ├── lib.rs
        └── sr25519.rs

**Dependencies**

sp-application-crypto v4.0.0
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-core v4.1.0-dev
├── sp-io v4.0.0
└── sp-std v4.0.0

### Arithmetic

- Crate: sp-arithmetic.
- Minimal fixed point arithmetic primitives and types for runtime.

primitives/arithmetic
├── benches
│   └── bench.rs
├── Cargo.toml
├── fuzzer
│   ├── Cargo.toml
│   └── src
│       ├── biguint.rs
│       ├── fixed_point.rs
│       ├── multiply_by_rational.rs
│       ├── normalize.rs
│       └── per_thing_rational.rs
├── README.md
└── src
    ├── biguint.rs
    ├── fixed_point.rs
    ├── helpers_128bit.rs
    ├── lib.rs
    ├── per_things.rs
    ├── rational.rs
    └── traits.rs

**Dependencies**

sp-arithmetic v4.0.0
├── integer-sqrt v0.1.5
├── num-traits v0.2.14
│   [build-dependencies]
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-debug-derive v4.0.0 (proc-macro)
├── sp-std v4.0.0
└── static_assertions v1.1.0
[dev-dependencies]
├── criterion v0.3.5
├── primitive-types v0.10.1
└── rand v0.7.3

### Authority-Discovery

- Crate: sp-authority-discovery.
- Runtime api to help discover authorities.
- The api entry-point will forward requests to "authority-discovery" pallet.

primitives/authority-discovery
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-authority-discovery v4.0.0-dev
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### Authorship

- Crate: sp-authoriship.
- Authorship primitives.
- Mostly 'uncles' inherent related stuff: InherentError, InherentData, ...

primitives/authorship
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-authorship v4.0.0-dev
├── async-trait v0.1.51 (proc-macro)
├── parity-scale-codec v2.3.1
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### Beefy

- Crate: beefy-primitives.
- Primitives for BEEFY protocol, a gadget that runs alongside another finality
  gadget (e.g. GRANDPA).
- Is this related to ETHEREUM?

primitives/beefy
├── Cargo.toml
└── src
    ├── commitment.rs
    ├── lib.rs
    ├── mmr.rs
    └── witness.rs

**Dependencies**

beefy-primitives v4.0.0-dev
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
└── sp-std v4.0.0
[dev-dependencies]
├── hex v0.4.3
├── hex-literal v0.3.4 (proc-macro)
└── sp-keystore v0.10.0

### Block-Builder

- Crate: sp-block-builder.
- Block builder runtime api.

primitives/block-builder
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-block-builder v4.0.0-dev
├── parity-scale-codec v2.3.1
├── sp-api v4.0.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### Blockchain

- Crate: sp-blockchain.
- Blockchain traints and primitives.
- Database backend traits.
- Tree backend, cached header metadata and algorithms to compute reoutes
  efficiently over the tree of headers.

primitives/blockchain
├── Cargo.toml
├── README.md
└── src
    ├── backend.rs
    ├── error.rs
    ├── header_metadata.rs
    └── lib.rs

**Dependencies**

sp-blockchain v4.0.0-dev
├── futures v0.3.16
├── log v0.4.14
├── lru v0.7.0
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── sp-api v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-database v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-state-machine v0.10.0
└── thiserror v1.0.30

### Consensus (Common)

- Crate: sp-consensus.
- Consensus specific InherentData, InherentDataProvider, Digest, runtime apis...
- Common utilities for building and using consensus engines in substrate.
- Block announcement validation, error types in consensus, chain selection,
  block evaluation and evaluation errors.

primitives/consensus/common
├── Cargo.toml
├── README.md
└── src
    ├── block_validation.rs
    ├── error.rs
    ├── evaluation.rs
    ├── lib.rs
    └── select_chain.rs

**Dependencies**

sp-consensus v0.10.0-dev
├── async-trait v0.1.51 (proc-macro)
├── futures v0.3.16
├── futures-timer v3.0.2
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sp-core v4.1.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-state-machine v0.10.0
├── sp-std v4.0.0
├── sp-version v4.0.0-dev
└── thiserror v1.0.30
[dev-dependencies]
├── futures v0.3.16 (*)
└── sp-test-primitives v2.0.0

### Consensus (Aura)

- Crates: sp-consensus-aura.
- Primitives for Aura.

primitives/consensus/aura
├── Cargo.toml
├── README.md
└── src
    ├── digests.rs
    ├── inherents.rs
    └── lib.rs

**Dependencies**

sp-consensus-aura v0.10.0-dev
├── async-trait v0.1.51 (proc-macro)
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-consensus v0.10.0-dev
├── sp-consensus-slots v0.10.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-std v4.0.0
└── sp-timestamp v4.0.0-dev

### Consensus (BABE)

- Crate: sp-consensus-babe.
- Primitives for BABE.

primitives/consensus/babe
├── Cargo.toml
├── README.md
└── src
    ├── digests.rs
    ├── inherents.rs
    └── lib.rs

**Dependencies**

sp-consensus-babe v0.10.0-dev
├── async-trait v0.1.51 (proc-macro)
├── merlin v2.0.1
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-consensus v0.10.0-dev
├── sp-consensus-slots v0.10.0-dev
├── sp-consensus-vrf v0.10.0-dev
├── sp-core v4.1.0-dev
├── sp-inherents v4.0.0-dev
├── sp-keystore v0.10.0
├── sp-runtime v4.0.0
├── sp-std v4.0.0
└── sp-timestamp v4.0.0-dev

### Consensus (PoW)

- Crate: sp-consensus-pow.
- Primitives for PoW.

primitives/consensus/pow
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-consensus-pow v0.10.0-dev
├── parity-scale-codec v2.3.1
├── sp-api v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### Consensus (Slots)

- Crate: sp-consensus-slots.
- Primitives for slots-based consensus engines.
- Slot and EquivocationProof definitions.

primitives/consensus/slots
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-consensus-slots v0.10.0-dev
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-arithmetic v4.0.0
└── sp-runtime v4.0.0

### Consensus (VRF)

- Crate: sp-consensus-vrf.
- Primitives for (Shnorrkel) VRF-based consensus engines.

primitives/consensus/vrf
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── schnorrkel.rs

**Dependencies**

sp-consensus-vrf v0.10.0-dev
├── parity-scale-codec v2.3.1
├── schnorrkel v0.9.1
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### Core

- Crates: sp-core, sp-core-hashing, sp-core-hashing-proc-macro.
- CodeExecutor, RuntimeCode, SpawnNamed, SpawnEssentialNamed, RuntimeSpawn
  traits.
- Hashing functions: twox, blake2, keccak, sha2.
- Cryptographic utilities: Pair/Public traits, known KeyTypeId(s) (e.g. AURA).
- Cryptographic implementations: Ecdsa, Ed25519, Sr25519.
- Offchain workers types: Externalities, TransactionPool, DbExternalities,
  OffchainStorage, traits... Implementation for InMemOffchainStorage.
- Sandbox environment definition.
- Uints from "primitive-types" crate: U256, U512.

primitives/core
├── benches
│   └── bench.rs
├── Cargo.toml
├── hashing
│   ├── Cargo.toml
│   ├── proc-macro
│   │   ├── Cargo.toml
│   │   └── src
│   │       ├── impls.rs
│   │       └── lib.rs
│   └── src
│       └── lib.rs
└── src
    ├── crypto.rs
    ├── ecdsa.rs
    ├── ed25519.rs
    ├── hasher.rs
    ├── hashing.rs
    ├── hash.rs
    ├── hexdisplay.rs
    ├── lib.rs
    ├── offchain
    │   ├── mod.rs
    │   ├── storage.rs
    │   └── testing.rs
    ├── sandbox.rs
    ├── sr25519.rs
    ├── testing.rs
    ├── traits.rs
    ├── u32_trait.rs
    └── uint.rs

**Dependencies**

sp-core v4.1.0-dev
├── base58 v0.2.0
├── bitflags v1.3.2
├── blake2-rfc v0.2.18
├── byteorder v1.3.4
├── dyn-clonable v0.9.0
├── ed25519-dalek v1.0.1
├── futures v0.3.16
├── hash-db v0.15.2
├── hash256-std-hasher v0.15.2
├── hex v0.4.3
├── impl-serde v0.3.1
├── lazy_static v1.4.0
├── libsecp256k1 v0.7.0
│   [build-dependencies]
├── log v0.4.14
├── merlin v2.0.1
├── num-traits v0.2.14
│   [build-dependencies]
├── parity-scale-codec v2.3.1
├── parity-util-mem v0.10.2
├── parking_lot v0.11.2
├── primitive-types v0.10.1
├── rand v0.7.3
├── regex v1.5.4
├── scale-info v1.0.0
├── schnorrkel v0.9.1
├── secrecy v0.8.0
├── serde v1.0.132
├── sha2 v0.9.8
├── sp-core-hashing v4.0.0
├── sp-debug-derive v4.0.0 (proc-macro)
├── sp-externalities v0.10.0
├── sp-runtime-interface v4.1.0-dev
├── sp-std v4.0.0
├── sp-storage v4.0.0
├── ss58-registry v1.10.0
│   [build-dependencies]
├── substrate-bip39 v0.4.4
├── thiserror v1.0.30
├── tiny-bip39 v0.8.2
├── tiny-keccak v2.0.2
├── twox-hash v1.6.1
├── wasmi v0.9.1
└── zeroize v1.4.3
[dev-dependencies]
├── criterion v0.3.5
├── hex-literal v0.3.4 (proc-macro)
├── rand v0.7.3 (*)
├── serde_json v1.0.71
├── sp-core-hashing-proc-macro v4.0.0-dev (proc-macro)
└── sp-serializer v4.0.0-dev

sp-core-hashing v4.0.0
├── blake2-rfc v0.2.18
├── byteorder v1.3.4
├── sha2 v0.9.8
├── sp-std v4.0.0
├── tiny-keccak v2.0.2
└── twox-hash v1.6.1

sp-core-hashing-proc-macro v4.0.0-dev (proc-macro)
├── proc-macro2 v1.0.32
├── quote v1.0.10
├── sp-core-hashing v4.0.0
└── syn v1.0.82

### Database

- Crate: sp-database.
- Main persistent database trait (Database) and (db)Transaction struct.
- Database implementation over KeyValueDB (kvdb) wrapper and MemDb

primitives/database
├── Cargo.toml
├── README.md
└── src
    ├── error.rs
    ├── kvdb.rs
    ├── lib.rs
    └── mem.rs

**Dependencies**

sp-database v4.0.0-dev
├── kvdb v0.10.0
└── parking_lot v0.11.2

### Debug-Derive

- Crate: sp-debug-derive.
- Macros to derive runtime debug implementation (#derive[RuntimeDebug]).
- With `std` this is equivelent to normal `Debug`. On `no_std` will be empty.

primitives/debug-derive
├── Cargo.toml
├── src
│   ├── impls.rs
│   └── lib.rs
└── tests
    └── tests.rs

**Dependencies**

sp-debug-derive v4.0.0 (proc-macro)
├── proc-macro2 v1.0.32
├── quote v1.0.10
└── syn v1.0.82

### Externalities

- Crate: sp-externalities.
- Provide access to storage and to registered extensions (e.g. keystore or
  offchain externalities).
- These are used to access the node from the runtime via the runtime interfaces.
- Defines Extension, ExtensionStore traits and Extensions struct.
- Defines Externalities and ExternalitiesExt.
- Execution using externalities stored in an "environmental" value to make the
  scope limited.

primitives/externalities
├── Cargo.toml
├── README.md
└── src
    ├── extensions.rs
    ├── lib.rs
    └── scope_limited.rs

**Dependencies**

sp-externalities v0.10.0
├── environmental v1.1.3
├── parity-scale-codec v2.3.1
├── sp-std v4.0.0
└── sp-storage v4.0.0

### Finality-GRANDPA

- Crate: sp-finality-grandpa
- Primitives for GRANDPA integration, suitable for WASM compilation.
- Runtime apis.

primitives/finality-grandpa
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-finality-grandpa v4.0.0-dev
├── finality-grandpa v0.14.4
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-core v4.1.0-dev
├── sp-keystore v0.10.0
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### Inherents

- Crate: sp-inherents.
- Mainly used to pass data (`InherentData`) from the block producer and the
  runtime.  So, inherents require some part running on client side and some part
  on runtime side.
- `InherentData` is passed to `ProvideInherent` pallet implementation.

TODO: breakpoint in timestamp Inherent to check execution path.

primitives/inherents
├── Cargo.toml
├── README.md
└── src
    ├── client_side.rs
    └── lib.rs

**Dependencies**

sp-inherents v4.0.0-dev
├── async-trait v0.1.51 (proc-macro)
├── impl-trait-for-tuples v0.2.1 (proc-macro)
├── parity-scale-codec v2.3.1
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
├── sp-std v4.0.0
└── thiserror v1.0.30
[dev-dependencies]
└── futures v0.3.16

### IO

- Crate: sp-io.
- I/O host interface for substrate runtime.
- Storage, DefaultChildStorage, Trie, traits
- Crypto, Hashing, Offchain, OffchainIndex, Allocator, Logging, Misc (mostly
  print), WasmTracing, Sandbox, RuntimeTasks (spawn tasks with ext) traits.
- Setup for tracing (PassingTracingSubsciber Subscriber)
- Global WASM allocator (WasmAllocator)
- SubstrateHostFunctions type for wasm runtime (defined by the host)

primitives/io
├── Cargo.toml
├── README.md
└── src
    ├── batch_verifier.rs
    └── lib.rs

**Dependencies**

sp-io v4.0.0
├── futures v0.3.16
├── hash-db v0.15.2
├── libsecp256k1 v0.7.0
│   [build-dependencies]
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── sp-core v4.1.0-dev
├── sp-externalities v0.10.0
├── sp-keystore v0.10.0
├── sp-runtime-interface v4.1.0-dev
├── sp-state-machine v0.10.0
├── sp-std v4.0.0
├── sp-tracing v4.0.0
├── sp-trie v4.0.0
├── sp-wasm-interface v4.1.0-dev
├── tracing v0.1.29
└── tracing-core v0.1.21

### Keyring

- Crate: sp-keyring
- Support code for runtime. A set of test accounts.

primitives/keyring
├── Cargo.toml
├── README.md
└── src
    ├── ed25519.rs
    ├── lib.rs
    └── sr25519.rs

**Dependencies**

sp-keyring v4.0.0-dev
├── lazy_static v1.4.0
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
└── strum v0.22.0

### Keystore

- Crate: sp-keystore.
- Keystore traits: CryptoStore (async), SyncCryptoStore.
- VRF-specific data types and helpers (merlin).

primitives/keystore
├── Cargo.toml
└── src
    ├── lib.rs
    ├── testing.rs
    └── vrf.rs

**Dependencies**

sp-keystore v0.10.0
├── async-trait v0.1.51 (proc-macro)
├── derive_more v0.99.16 (proc-macro)
│   [build-dependencies]
├── futures v0.3.16
├── merlin v2.0.1
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── schnorrkel v0.9.1
├── serde v1.0.132
├── sp-core v4.1.0-dev
└── sp-externalities v0.10.0
[dev-dependencies]
├── rand v0.7.3
└── rand_chacha v0.2.2

### Maybe-Compressed-Blob

- Crate: sp-maybe-compresse-blob.
- Handling of blobs that may be compressed (zstd).
- Based on 8-byte magic identifier at the head.

primitives/maybe-compressed-blob
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-maybe-compressed-blob v4.1.0-dev
└── zstd v0.9.0+zstd.1.5.0

### NPoS-Elections

- Crate: sp-npos-elections, sp-npos-elections-solution-type.
- A set of elections algorithms to be used with a substrate runtime, typically
  the staking subsystem.

TODO: THIS IS COMPLEX

primitives/npos-elections
├── Cargo.toml
├── fuzzer
│   ├── Cargo.lock
│   ├── Cargo.toml
│   └── src
│       ├── common.rs
│       ├── compact.rs
│       ├── phragmen_balancing.rs
│       ├── phragmen_pjr.rs
│       ├── phragmms_balancing.rs
│       └── reduce.rs
├── README.md
├── solution-type
│   ├── Cargo.toml
│   ├── src
│   │   ├── codec.rs
│   │   ├── from_assignment_helpers.rs
│   │   ├── index_assignment.rs
│   │   ├── lib.rs
│   │   └── single_page.rs
│   └── tests
│       └── ui
│           └── fail
│               ├── missing_accuracy.rs
│               ├── missing_accuracy.stderr
│               ├── missing_target.rs
│               ├── missing_target.stderr
│               ├── missing_voter.rs
│               ├── missing_voter.stderr
│               ├── no_annotations.rs
│               ├── no_annotations.stderr
│               ├── swap_voter_target.rs
│               ├── swap_voter_target.stderr
│               ├── wrong_attribute.rs
│               └── wrong_attribute.stderr
└── src
    ├── assignments.rs
    ├── balancing.rs
    ├── helpers.rs
    ├── lib.rs
    ├── mock.rs
    ├── node.rs
    ├── phragmen.rs
    ├── phragmms.rs
    ├── pjr.rs
    ├── reduce.rs
    ├── tests.rs
    └── traits.rs

**Dependencies**

sp-npos-elections v4.0.0-dev
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-arithmetic v4.0.0
├── sp-core v4.1.0-dev
├── sp-npos-elections-solution-type v4.0.0-dev (proc-macro)
├── sp-runtime v4.0.0
└── sp-std v4.0.0
[dev-dependencies]
├── rand v0.7.3
└── substrate-test-utils v4.0.0-dev

### Offchain

- Crate: sp-offchain.
- Offchain worker runtime api primitives.

primitives/offchain
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-offchain v4.0.0-dev
├── sp-api v4.0.0-dev
├── sp-core v4.1.0-dev
└── sp-runtime v4.0.0

### Panic-Handler

- Crate: sp-panic-handler.
- Custom panic hook with bug report link.

primitives/panic-handler
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-panic-handler v4.0.0
├── backtrace v0.3.63
│   [build-dependencies]
├── lazy_static v1.4.0
└── regex v1.5.4

### RPC

- Crate: sp-rpc.
- RPC primitives and utilities.

TODO: not clear how the content relates to RPC

primitives/rpc
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── list.rs
    ├── number.rs
    └── tracing.rs

**Dependencies**

sp-rpc v4.0.0-dev
├── rustc-hash v1.1.0
├── serde v1.0.132
└── sp-core v4.1.0-dev
[dev-dependencies]
└── serde_json v1.0.71

### Runtime

- Crate: sp-runtime
- Runtime modules shared primitive types.
- Block finality `Justification` type.
- BuildStorage, BuildModuleGenesisStorage traits.
- MultiSignature, MultiSigned, AnySignature enums.
- DispatchError, DispatchResult, DispatchErrorWithPostInfo. DispatchResultWith...
- ApplyExtrinsicResult (WithInfo)
- MultiAddress

TODO: FINISH ME!!!

primitives/runtime
├── Cargo.toml
├── README.md
└── src
    ├── curve.rs
    ├── generic
    │   ├── block.rs
    │   ├── checked_extrinsic.rs
    │   ├── digest.rs
    │   ├── era.rs
    │   ├── header.rs
    │   ├── mod.rs
    │   ├── tests.rs
    │   └── unchecked_extrinsic.rs
    ├── lib.rs
    ├── multiaddress.rs
    ├── offchain
    │   ├── http.rs
    │   ├── mod.rs
    │   ├── storage_lock.rs
    │   └── storage.rs
    ├── runtime_logger.rs
    ├── runtime_string.rs
    ├── testing.rs
    ├── traits.rs
    └── transaction_validity.rs

**Dependencies**

sp-runtime v4.0.0
├── either v1.6.1
├── hash256-std-hasher v0.15.2
├── impl-trait-for-tuples v0.2.1 (proc-macro)
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parity-util-mem v0.10.2
├── paste v1.0.6 (proc-macro)
├── rand v0.7.3
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-application-crypto v4.0.0
├── sp-arithmetic v4.0.0
├── sp-core v4.1.0-dev
├── sp-io v4.0.0
└── sp-std v4.0.0
[dev-dependencies]
├── rand v0.7.3 (*)
├── serde_json v1.0.71
├── sp-api v4.0.0-dev
├── sp-state-machine v0.10.0
├── sp-tracing v4.0.0
├── substrate-test-runtime-client v2.0.0
└── zstd v0.9.0+zstd.1.5.0

### Runtime-Interface

- Crates: sp-runtime-interface, sp-runtime-interface-proc-macro
- Types, traits and macros around runtime interfaces (fixed interfaces beteween
  runtime and node).
- For native runtime the interface maps directly to a function call of the
  implementation. For wasm runtime the interface maps to an external function
  call.

- TODO: this looks important and has a lot more documented information.

primitives/runtime-interface
├── Cargo.toml
├── proc-macro
│   ├── Cargo.toml
│   └── src
│       ├── lib.rs
│       ├── pass_by
│       │   ├── codec.rs
│       │   ├── enum_.rs
│       │   ├── inner.rs
│       │   └── mod.rs
│       ├── runtime_interface
│       │   ├── bare_function_interface.rs
│       │   ├── host_function_interface.rs
│       │   ├── mod.rs
│       │   └── trait_decl_impl.rs
│       └── utils.rs
├── README.md
├── src
│   ├── host.rs
│   ├── impls.rs
│   ├── lib.rs
│   ├── pass_by.rs
│   ├── util.rs
│   └── wasm.rs
├── test
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── tests
│   ├── ui
│   │   ├── no_duplicate_versions.rs
│   │   ├── no_duplicate_versions.stderr
│   │   ├── no_gaps_in_versions.rs
│   │   ├── no_gaps_in_versions.stderr
│   │   ├── no_generic_parameters_method.rs
│   │   ├── no_generic_parameters_method.stderr
│   │   ├── no_generic_parameters_trait.rs
│   │   ├── no_generic_parameters_trait.stderr
│   │   ├── no_method_implementation.rs
│   │   ├── no_method_implementation.stderr
│   │   ├── pass_by_enum_with_struct.rs
│   │   ├── pass_by_enum_with_struct.stderr
│   │   ├── pass_by_enum_with_value_variant.rs
│   │   ├── pass_by_enum_with_value_variant.stderr
│   │   ├── pass_by_inner_with_two_fields.rs
│   │   ├── pass_by_inner_with_two_fields.stderr
│   │   ├── take_self_by_value.rs
│   │   └── take_self_by_value.stderr
│   └── ui.rs
├── test-wasm
│   ├── build.rs
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
└── test-wasm-deprecated
    ├── build.rs
    ├── Cargo.toml
    └── src
        └── lib.rs

**Dependencies**

sp-runtime-interface v4.1.0-dev
├── impl-trait-for-tuples v0.2.1 (proc-macro)
├── parity-scale-codec v2.3.1
├── primitive-types v0.10.1
├── sp-externalities v0.10.0
├── sp-runtime-interface-proc-macro v4.0.0 (proc-macro)
├── sp-std v4.0.0
├── sp-storage v4.0.0
├── sp-tracing v4.0.0
├── sp-wasm-interface v4.1.0-dev
└── static_assertions v1.1.0
[dev-dependencies]
├── rustversion v1.0.6 (proc-macro)
├── sp-core v4.1.0-dev
├── sp-io v4.0.0
├── sp-runtime-interface-test-wasm v2.0.0
│   [build-dependencies]
├── sp-state-machine v0.10.0
└── trybuild v1.0.53

### Sandbox

- Crate: sp-sandbox
- Means to instantiate and execute wasm modules. Works als when the caller is
  from wasm VM. The same VM is reused.
- Some usages: smart-contracts, executing wasm runtime inside of wasm parachain.

- TODO: this is not used for node runtime execution?

primitives/sandbox
├── Cargo.toml
├── README.md
└── src
    ├── embedded_executor.rs
    ├── host_executor.rs
    └── lib.rs

**Dependencies**

sp-sandbox v0.10.0-dev
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sp-core v4.1.0-dev
├── sp-io v4.0.0
├── sp-std v4.0.0
├── sp-wasm-interface v4.1.0-dev
└── wasmi v0.9.1
[dev-dependencies]
├── assert_matches v1.5.0
└── wat v1.0.40

### Serializer

- Crate: sp-serializer
- Customizable serde serializer. For now... json.

primitives/serializer
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-serializer v4.0.0-dev
├── serde v1.0.132
└── serde_json v1.0.71

### Session

- Crate: sp-session.
- Types around sessions.
- `SessionKeys` runtime apis.

primitives/session
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-session v4.0.0-dev
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-api v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
├── sp-staking v4.0.0-dev
└── sp-std v4.0.0

### Staking

- Crate: sp-staking
- Primitives used for implementation that uses staking approaches in general.
- Definitions related to sessions, slashing, offence, etc.

primitives/staking
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── offence.rs

**Dependencies**

sp-staking v4.0.0-dev
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-runtime v4.0.0
└── sp-std v4.0.0

### State-Machine

- Crate: sp-state-machine.
- State machine implementation.
- `ExecutionStrategy`, `ExecutionManager`, `StateMachine`

primitives/state-machine
├── Cargo.toml
├── README.md
└── src
    ├── backend.rs
    ├── basic.rs
    ├── error.rs
    ├── ext.rs
    ├── in_memory_backend.rs
    ├── lib.rs
    ├── overlayed_changes
    │   ├── changeset.rs
    │   ├── mod.rs
    │   └── offchain.rs
    ├── proving_backend.rs
    ├── read_only.rs
    ├── stats.rs
    ├── testing.rs
    ├── trie_backend_essence.rs
    └── trie_backend.rs

**Dependencies**

sp-state-machine v0.10.0
├── hash-db v0.15.2
├── log v0.4.14
├── num-traits v0.2.14
│   [build-dependencies]
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── rand v0.7.3
├── smallvec v1.7.0
├── sp-core v4.1.0-dev
├── sp-externalities v0.10.0
├── sp-panic-handler v4.0.0
├── sp-std v4.0.0
├── sp-trie v4.0.0
├── thiserror v1.0.30
├── tracing v0.1.29
├── trie-db v0.23.0
└── trie-root v0.17.0
[dev-dependencies]
├── hex-literal v0.3.4 (proc-macro)
├── pretty_assertions v1.0.0
├── rand v0.7.3 (*)
└── sp-runtime v4.0.0


### STD

- Crate: sp-std.
- Re-exports useful primitives from std or core/alloc to be used with any code
  that depends on the runtime.
- Macros: map, vec, if_std.

primitives/std
├── Cargo.toml
├── README.md
├── src
│   └── lib.rs
├── without_std.rs
└── with_std.rs

**Dependencies**

sp-std v4.0.0

### Storage

- Crate: sp-storage.
- Primitives for storage related stuff.
- Well known keys and prefixes in storage.

primitives/storage
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-storage v4.0.0
├── impl-serde v0.3.1
├── parity-scale-codec v2.3.1
├── ref-cast v1.0.6
├── serde v1.0.132
├── sp-debug-derive v4.0.0 (proc-macro)
└── sp-std v4.0.0

### Tasks

- Crate: sp-tasks.
- Runtime-usable functions for spawning parallel purely computational tasks.

primitives/tasks
├── Cargo.toml
├── README.md
└── src
    ├── async_externalities.rs
    └── lib.rs

**Dependencies**

sp-tasks v4.0.0-dev
├── log v0.4.14
├── sp-core v4.1.0-dev
├── sp-externalities v0.10.0
├── sp-io v4.0.0
├── sp-runtime-interface v4.1.0-dev
└── sp-std v4.0.0
[dev-dependencies]
└── parity-scale-codec v2.3.1

### Test-Primitives

- Crate: sp-test-primitives.
- Test primitives to share.

primitives/test-primitives
├── Cargo.toml
└── src
    └── lib.rs

**Dependencies**

sp-test-primitives v2.0.0
├── parity-scale-codec v2.3.1
├── parity-util-mem v0.10.2
├── serde v1.0.132
├── sp-application-crypto v4.0.0
├── sp-core v4.1.0-dev
└── sp-runtime v4.0.0

### Timestamp

- Crate: sp-timestamp.
- Types and inherents for timestamps.

primitives/timestamp
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-timestamp v4.0.0-dev
├── async-trait v0.1.51 (proc-macro)
├── futures-timer v3.0.2
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sp-api v4.0.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-std v4.0.0
└── thiserror v1.0.30

### Tracing

- Crate: sp-tracing
- Tracing primitives and macros.
- Provides: `whithin_span` and `enter_span` macros.

primitives/tracing
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── types.rs

**Dependencies**

sp-tracing v4.0.0
├── parity-scale-codec v2.3.1
├── sp-std v4.0.0
├── tracing v0.1.29
├── tracing-core v0.1.21
└── tracing-subscriber v0.2.25

### Transaction-Pool

- Crate: sp-transaction-pool.
- Runtime api for transaction pool.

primitives/transaction-pool
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── runtime_api.rs

**Dependencies**

sp-transaction-pool v4.0.0-dev
├── sp-api v4.0.0-dev
└── sp-runtime v4.0.0

### Transaction-Storage-Proof

- Crate: sp-transaction-storate-proof
- Types and basic code to extract storage proofs for indexed transactions.

primitives/transaction-storage-proof
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sp-transaction-storage-proof v4.0.0-dev
├── async-trait v0.1.51 (proc-macro)
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-core v4.1.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-std v4.0.0
└── sp-trie v4.0.0

### Trie

- Crate: sp-trie.
- Utilities to interact with base-16 Modified Merkle Patricia tree (trie).

primitives/trie
├── benches
│   └── bench.rs
├── Cargo.toml
├── README.md
├── src
│   ├── error.rs
│   ├── lib.rs
│   ├── node_codec.rs
│   ├── node_header.rs
│   ├── storage_proof.rs
│   ├── trie_codec.rs
│   └── trie_stream.rs
└── test-res
    ├── invalid-delta-order
    ├── proof
    ├── storage_root
    └── valid-delta-order

**Dependencies**

sp-trie v4.0.0
├── hash-db v0.15.2
├── memory-db v0.28.0
├── parity-scale-codec v2.3.1
├── scale-info v1.0.0
├── sp-core v4.1.0-dev
├── sp-std v4.0.0
├── trie-db v0.23.0
└── trie-root v0.17.0
[dev-dependencies]
├── criterion v0.3.5
├── hex-literal v0.3.4 (proc-macro)
├── sp-runtime v4.0.0
├── trie-bench v0.29.0
└── trie-standardmap v0.15.2

### Version

- Crate: sp-version.
- Runtime version (`RuntimeVersion`).
- Functionality to embed runtime version as a custom section in wasm file.

primitives/version
├── Cargo.toml
├── proc-macro
│   ├── Cargo.toml
│   └── src
│       ├── decl_runtime_version.rs
│       └── lib.rs
├── README.md
└── src
    ├── embed.rs
    └── lib.rs

**Dependencies**

sp-version v4.0.0-dev
├── impl-serde v0.3.1
├── parity-scale-codec v2.3.1
├── parity-wasm v0.42.2
├── scale-info v1.0.0
├── serde v1.0.132
├── sp-core-hashing-proc-macro v4.0.0-dev (proc-macro)
├── sp-runtime v4.0.0
├── sp-std v4.0.0
├── sp-version-proc-macro v4.0.0-dev (proc-macro)
└── thiserror v1.0.30

### Wasm-Interface

- Crate: sp-wasm-interface.
- Types and traits for interfacing between the host and wasm runtime.
- Implementation of conversions between Substrate and wasmi types.

primitives/wasm-interface
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── wasmi_impl.rs

**Dependencies**

sp-wasm-interface v4.1.0-dev
├── impl-trait-for-tuples v0.2.1 (proc-macro)
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sp-std v4.0.0
└── wasmi v0.9.1
