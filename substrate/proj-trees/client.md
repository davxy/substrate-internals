CLIENT
------

### Allocator

client/allocator
├── Cargo.toml
├── README.md
└── src
    ├── error.rs
    ├── freeing_bump.rs
    └── lib.rs

**Dependencies**

sc-allocator v4.1.0-dev
├── log v0.4.14
├── sp-core v4.1.0-dev
├── sp-wasm-interface v4.1.0-dev
└── thiserror v1.0.30

### API

- Crate: sc-client-api
- Client interfaces.

client/api
├── Cargo.toml
├── README.md
└── src
    ├── backend.rs
    ├── call_executor.rs
    ├── client.rs
    ├── execution_extensions.rs
    ├── in_mem.rs
    ├── leaves.rs
    ├── lib.rs
    ├── notifications.rs
    └── proof_provider.rs

**Dependencies**

sc-client-api v4.0.0-dev
├── fnv v1.0.7
├── futures v0.3.16
├── hash-db v0.15.2
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── sc-executor v0.10.0-dev
├── sc-transaction-pool-api v4.0.0-dev
├── sc-utils v4.0.0-dev
├── sp-api v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-core v4.1.0-dev
├── sp-database v4.0.0-dev
├── sp-externalities v0.10.0
├── sp-keystore v0.10.0
├── sp-runtime v4.0.0
├── sp-state-machine v0.10.0
├── sp-storage v4.0.0
├── sp-trie v4.0.0
└── substrate-prometheus-endpoint v0.10.0-dev
[dev-dependencies]
├── sp-test-primitives v2.0.0
├── substrate-test-runtime v2.0.0
│   [build-dependencies]
└── thiserror v1.0.30

### Authority-Discovery

client/authority-discovery
├── build.rs
├── Cargo.toml
├── README.md
└── src
    ├── error.rs
    ├── interval.rs
    ├── lib.rs
    ├── service.rs
    ├── tests.rs
    ├── worker
    │   ├── addr_cache.rs
    │   ├── schema
    │   │   ├── dht-v1.proto
    │   │   ├── dht-v2.proto
    │   │   └── tests.rs
    │   └── tests.rs
    └── worker.rs

**Dependencies**

sc-authority-discovery v0.10.0-dev
├── async-trait v0.1.51 (proc-macro)
├── derive_more v0.99.16 (proc-macro)
│   [build-dependencies]
├── futures v0.3.16
├── futures-timer v3.0.2
├── ip_network v0.4.0
├── libp2p v0.40.0
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── prost v0.9.0
├── rand v0.7.3
├── sc-client-api v4.0.0-dev
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sp-api v4.0.0-dev
├── sp-authority-discovery v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-keystore v0.10.0
├── sp-runtime v4.0.0
└── substrate-prometheus-endpoint v0.10.0-dev
[build-dependencies]
└── prost-build v0.9.0
    [build-dependencies]
[dev-dependencies]
├── quickcheck v1.0.3
├── sp-tracing v4.0.0
└── substrate-test-runtime-client v2.0.0

### Basic-Authorship

- Crate: sc-basic-authorship
- Basic implementation of block-authoring logic.

client/basic-authorship
├── Cargo.toml
├── README.md
└── src
    ├── basic_authorship.rs
    └── lib.rs

**Dependencies**

sc-basic-authorship v0.10.0-dev
├── futures v0.3.16
├── futures-timer v3.0.2
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sc-block-builder v0.10.0-dev
├── sc-client-api v4.0.0-dev
├── sc-proposer-metrics v0.10.0-dev
├── sc-telemetry v4.0.0-dev
├── sc-transaction-pool-api v4.0.0-dev
├── sp-api v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-core v4.1.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
└── substrate-prometheus-endpoint v0.10.0-dev
[dev-dependencies]
├── parking_lot v0.11.2
├── sc-transaction-pool v4.0.0-dev
└── substrate-test-runtime-client v2.0.0

### Beefy

client/beefy
├── Cargo.toml
├── rpc
│   ├── Cargo.toml
│   └── src
│       ├── lib.rs
│       └── notification.rs
└── src
    ├── error.rs
    ├── gossip.rs
    ├── keystore.rs
    ├── lib.rs
    ├── metrics.rs
    ├── notification.rs
    ├── round.rs
    └── worker.rs

**Dependencies**

beefy-gadget v4.0.0-dev
├── beefy-primitives v4.0.0-dev
├── fnv v1.0.7
├── futures v0.3.16
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── sc-client-api v4.0.0-dev
├── sc-keystore v4.0.0-dev
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sc-network-gossip v0.10.0-dev
├── sc-utils v4.0.0-dev
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-arithmetic v4.0.0
├── sp-blockchain v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-keystore v0.10.0
├── sp-runtime v4.0.0
├── substrate-prometheus-endpoint v0.10.0-dev
├── thiserror v1.0.30
└── wasm-timer v0.2.5
[dev-dependencies]
├── sc-network-test v0.8.0
├── sp-tracing v4.0.0
└── strum v0.22.0

### Block-Builder

- Crate: sc-block-builder
- Provides the `BlockBuilder` utility and the corresponding runtime api
  (re-exports the `sp_block_builder::BlockBuilder` runtime api as
  BlockBuilderApi)

client/block-builder
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sc-block-builder v0.10.0-dev
├── parity-scale-codec v2.3.1
├── sc-client-api v4.0.0-dev
├── sp-api v4.0.0-dev
├── sp-block-builder v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-inherents v4.0.0-dev
├── sp-runtime v4.0.0
└── sp-state-machine v0.10.0
[dev-dependencies]
└── substrate-test-runtime-client v2.0.0

### Chain-Spec

client/chain-spec
├── Cargo.toml
├── derive
│   ├── Cargo.toml
│   └── src
│       ├── impls.rs
│       └── lib.rs
├── README.md
├── res
│   ├── chain_spec2.json
│   └── chain_spec.json
└── src
    ├── chain_spec.rs
    ├── extension.rs
    └── lib.rs

**Dependencies**

sc-chain-spec v4.0.0-dev
├── impl-trait-for-tuples v0.2.1 (proc-macro)
├── memmap2 v0.5.0
├── parity-scale-codec v2.3.1
├── sc-chain-spec-derive v4.0.0-dev (proc-macro)
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sc-telemetry v4.0.0-dev
├── serde v1.0.132
├── serde_json v1.0.71
├── sp-core v4.1.0-dev
└── sp-runtime v4.0.0

### CLI

client/cli
├── Cargo.toml
├── README.adoc
├── README.md
└── src
    ├── arg_enums.rs
    ├── commands
    │   ├── build_spec_cmd.rs
    │   ├── check_block_cmd.rs
    │   ├── export_blocks_cmd.rs
    │   ├── export_state_cmd.rs
    │   ├── generate_node_key.rs
    │   ├── generate.rs
    │   ├── import_blocks_cmd.rs
    │   ├── insert_key.rs
    │   ├── inspect_key.rs
    │   ├── inspect_node_key.rs
    │   ├── key.rs
    │   ├── mod.rs
    │   ├── purge_chain_cmd.rs
    │   ├── revert_cmd.rs
    │   ├── run_cmd.rs
    │   ├── sign.rs
    │   ├── utils.rs
    │   ├── vanity.rs
    │   └── verify.rs
    ├── config.rs
    ├── error.rs
    ├── lib.rs
    ├── params
    │   ├── database_params.rs
    │   ├── import_params.rs
    │   ├── keystore_params.rs
    │   ├── mod.rs
    │   ├── network_params.rs
    │   ├── node_key_params.rs
    │   ├── offchain_worker_params.rs
    │   ├── pruning_params.rs
    │   ├── shared_params.rs
    │   └── transaction_pool_params.rs
    └── runner.rs

### Consensus Common

- Crate: sc-consensus.
- Common consensus utilities implementations.
- Block import utilities (`BlockImportParams`, `BlockImport` trait)

client/consensus/common
├── Cargo.toml
├── README.md
└── src
    ├── block_import.rs
    ├── import_queue
    │   ├── basic_queue.rs
    │   └── buffered_link.rs
    ├── import_queue.rs
    ├── lib.rs
    ├── longest_chain.rs
    ├── metrics.rs
    └── shared_data.rs

### Consensus Aura

client/consensus/aura
├── Cargo.toml
├── README.md
└── src
    ├── import_queue.rs
    └── lib.rs

### Consensus BABE

client/consensus/babe
├── Cargo.toml
├── README.md
├── rpc
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       └── lib.rs
└── src
    ├── authorship.rs
    ├── aux_schema.rs
    ├── lib.rs
    ├── migration.rs
    ├── tests.rs
    └── verification.rs

### Consensus Epochs

client/consensus/epochs
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── migration.rs

### Consensus Manual-Seal

client/consensus/manual-seal
├── Cargo.toml
├── README.md
└── src
    ├── consensus
    │   └── babe.rs
    ├── consensus.rs
    ├── error.rs
    ├── finalize_block.rs
    ├── lib.rs
    ├── rpc.rs
    └── seal_block.rs

### Consensus PoW

client/consensus/pow
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── worker.rs

### Consensus Slots

client/consensus/slots
├── build.rs
├── Cargo.toml
├── README.md
└── src
    ├── aux_schema.rs
    ├── lib.rs
    └── slots.rs

### Consensus Uncles

client/consensus/uncles
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

### DB

- Crate: sc-client-db
- Client backend that is backed by a database.

client/db
├── Cargo.toml
├── README.md
└── src
    ├── bench.rs
    ├── children.rs
    ├── lib.rs
    ├── offchain.rs
    ├── parity_db.rs
    ├── stats.rs
    ├── storage_cache.rs
    ├── upgrade.rs
    └── utils.rs

### Executor

client/executor
├── Cargo.toml
├── common
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── error.rs
│       ├── lib.rs
│       ├── runtime_blob
│       │   ├── data_segments_snapshot.rs
│       │   ├── globals_snapshot.rs
│       │   ├── mod.rs
│       │   └── runtime_blob.rs
│       ├── sandbox.rs
│       ├── util.rs
│       └── wasm_runtime.rs
├── README.md
├── runtime-test
│   ├── build.rs
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── src
│   ├── integration_tests
│   │   ├── linux
│   │   │   └── smaps.rs
│   │   ├── linux.rs
│   │   ├── mod.rs
│   │   └── sandbox.rs
│   ├── lib.rs
│   ├── native_executor.rs
│   └── wasm_runtime.rs
├── wasmi
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       └── lib.rs
└── wasmtime
    ├── build.rs
    ├── Cargo.toml
    ├── README.md
    └── src
        ├── host.rs
        ├── imports.rs
        ├── instance_wrapper.rs
        ├── lib.rs
        ├── runtime.rs
        ├── test-guard-page-skip.wat
        ├── tests.rs
        └── util.rs

sc-executor-common v0.10.0-dev
├── derive_more v0.99.16 (proc-macro)
│   [build-dependencies]
├── environmental v1.1.3
├── parity-scale-codec v2.3.1
├── pwasm-utils v0.18.2
├── sc-allocator v4.1.0-dev
├── sp-core v4.1.0-dev
├── sp-maybe-compressed-blob v4.1.0-dev
├── sp-serializer v4.0.0-dev
├── sp-wasm-interface v4.1.0-dev
├── thiserror v1.0.30
└── wasmi v0.9.1

sc-executor-wasmi v0.10.0-dev
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── sc-allocator v4.1.0-dev
├── sc-executor-common v0.10.0-dev
├── scoped-tls v1.0.0
├── sp-core v4.1.0-dev
├── sp-runtime-interface v4.1.0-dev
├── sp-wasm-interface v4.1.0-dev
└── wasmi v0.9.1

sc-executor-wasmtime v0.10.0-dev
├── cfg-if v1.0.0
├── libc v0.2.112
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parity-wasm v0.42.2
├── sc-allocator v4.1.0-dev
├── sc-executor-common v0.10.0-dev
├── sp-core v4.1.0-dev
├── sp-runtime-interface v4.1.0-dev
├── sp-wasm-interface v4.1.0-dev
└── wasmtime v0.31.0
[dev-dependencies]
├── sc-runtime-test v2.0.0
│   [build-dependencies]
├── sp-io v4.0.0
└── wat v1.0.40

### Finality-GRANDPA

client/finality-grandpa
├── Cargo.toml
├── README.md
├── rpc
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── error.rs
│       ├── finality.rs
│       ├── lib.rs
│       ├── notification.rs
│       └── report.rs
└── src
    ├── authorities.rs
    ├── aux_schema.rs
    ├── communication
    │   ├── gossip.rs
    │   ├── mod.rs
    │   ├── periodic.rs
    │   └── tests.rs
    ├── environment.rs
    ├── finality_proof.rs
    ├── import.rs
    ├── justification.rs
    ├── lib.rs
    ├── notification.rs
    ├── observer.rs
    ├── tests.rs
    ├── until_imported.rs
    ├── voting_rule.rs
    └── warp_proof.rs

**Dependencies**

sc-finality-grandpa v0.10.0-dev
├── async-trait v0.1.51 (proc-macro)
├── derive_more v0.99.16 (proc-macro)
│   [build-dependencies]
├── dyn-clone v1.0.4
├── finality-grandpa v0.14.4
├── fork-tree v3.0.0
├── futures v0.3.16
├── futures-timer v3.0.2
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── rand v0.8.4
├── sc-block-builder v0.10.0-dev
├── sc-client-api v4.0.0-dev
├── sc-consensus v0.10.0-dev
├── sc-keystore v4.0.0-dev
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sc-network-gossip v0.10.0-dev
├── sc-telemetry v4.0.0-dev
├── sc-utils v4.0.0-dev
├── serde_json v1.0.71
├── sp-api v4.0.0-dev
├── sp-application-crypto v4.0.0
├── sp-arithmetic v4.0.0
├── sp-blockchain v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-core v4.1.0-dev
├── sp-finality-grandpa v4.0.0-dev
├── sp-keystore v0.10.0
├── sp-runtime v4.0.0
└── substrate-prometheus-endpoint v0.10.0-dev
[dev-dependencies]
├── assert_matches v1.5.0
├── finality-grandpa v0.14.4 (*)
├── sc-network v0.10.0-dev
├── sc-network-test v0.8.0
├── sp-keyring v4.0.0-dev
├── sp-tracing v4.0.0
├── substrate-test-runtime-client v2.0.0
├── tempfile v3.2.0
└── tokio v1.15.0


### Informant

client/informant
├── Cargo.toml
├── README.md
└── src
    ├── display.rs
    └── lib.rs

**Dependencies**

sc-informant v0.10.0-dev
├── ansi_term v0.12.1
├── futures v0.3.16
├── futures-timer v3.0.2
├── log v0.4.14
├── parity-util-mem v0.10.2
├── sc-client-api v4.0.0-dev
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sc-transaction-pool-api v4.0.0-dev
├── sp-blockchain v4.0.0-dev
└── sp-runtime v4.0.0

### Keystore

client/keystore
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── local.rs

**Dependencies**

sc-keystore v4.0.0-dev
├── async-trait v0.1.51 (proc-macro)
├── derive_more v0.99.16 (proc-macro)
│   [build-dependencies]
├── hex v0.4.3
├── parking_lot v0.11.2
├── serde_json v1.0.71
├── sp-application-crypto v4.0.0
├── sp-core v4.1.0-dev
└── sp-keystore v0.10.0
[dev-dependencies]
└── tempfile v3.2.0

### Network

client/network
├── build.rs
├── Cargo.toml
├── README.md
├── src
│   ├── behaviour.rs
│   ├── bitswap.rs
│   ├── block_request_handler.rs
│   ├── chain.rs
│   ├── config.rs
│   ├── discovery.rs
│   ├── error.rs
│   ├── lib.rs
│   ├── light_client_requests
│   │   └── handler.rs
│   ├── light_client_requests.rs
│   ├── network_state.rs
│   ├── peer_info.rs
│   ├── protocol
│   │   ├── event.rs
│   │   ├── message.rs
│   │   ├── notifications
│   │   │   ├── behaviour.rs
│   │   │   ├── handler.rs
│   │   │   ├── tests.rs
│   │   │   ├── upgrade
│   │   │   │   ├── collec.rs
│   │   │   │   └── notifications.rs
│   │   │   └── upgrade.rs
│   │   ├── notifications.rs
│   │   ├── sync
│   │   │   ├── blocks.rs
│   │   │   ├── extra_requests.rs
│   │   │   ├── state.rs
│   │   │   └── warp.rs
│   │   └── sync.rs
│   ├── protocol.rs
│   ├── request_responses.rs
│   ├── schema
│   │   ├── api.v1.proto
│   │   ├── bitswap.v1.2.0.proto
│   │   └── light.v1.proto
│   ├── schema.rs
│   ├── service
│   │   ├── metrics.rs
│   │   ├── out_events.rs
│   │   ├── signature.rs
│   │   └── tests.rs
│   ├── service.rs
│   ├── state_request_handler.rs
│   ├── transactions.rs
│   ├── transport.rs
│   ├── utils.rs
│   └── warp_request_handler.rs
└── test
    ├── Cargo.toml
    └── src
        ├── block_import.rs
        ├── lib.rs
        └── sync.rs

**Dependencies**

sc-network v0.10.0-dev
├── async-std v1.10.0
├── async-trait v0.1.51 (proc-macro)
├── asynchronous-codec v0.5.0
├── bitflags v1.3.2
├── bytes v1.1.0
├── cid v0.6.1
├── derive_more v0.99.16 (proc-macro)
│   [build-dependencies]
├── either v1.6.1
├── fnv v1.0.7
├── fork-tree v3.0.0
├── futures v0.3.16
├── futures-timer v3.0.2
├── hex v0.4.3
├── ip_network v0.4.0
├── libp2p v0.40.0
├── linked-hash-map v0.5.4
├── linked_hash_set v0.1.4
├── log v0.4.14
├── lru v0.7.0
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── pin-project v1.0.8
├── prost v0.9.0
├── rand v0.7.3
├── sc-block-builder v0.10.0-dev
├── sc-client-api v4.0.0-dev
├── sc-consensus v0.10.0-dev
├── sc-peerset v4.0.0-dev
├── sc-utils v4.0.0-dev
├── serde v1.0.132
├── serde_json v1.0.71
├── smallvec v1.7.0
├── sp-arithmetic v4.0.0
├── sp-blockchain v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-core v4.1.0-dev
├── sp-finality-grandpa v4.0.0-dev
├── sp-runtime v4.0.0
├── substrate-prometheus-endpoint v0.10.0-dev
├── thiserror v1.0.30
├── unsigned-varint v0.6.0
├── void v1.0.2
└── zeroize v1.4.3
[build-dependencies]
└── prost-build v0.9.0
    [build-dependencies]
[dev-dependencies]
├── assert_matches v1.5.0
├── libp2p v0.40.0 (*)
├── quickcheck v1.0.3
├── rand v0.7.3 (*)
├── sp-test-primitives v2.0.0
├── sp-tracing v4.0.0
├── substrate-test-runtime v2.0.0
│   [build-dependencies]
├── substrate-test-runtime-client v2.0.0
└── tempfile v3.2.0

### Network-Gossip

client/network-gossip
├── Cargo.toml
├── README.md
└── src
    ├── bridge.rs
    ├── lib.rs
    ├── state_machine.rs
    └── validator.rs

**Dependencies**

sc-network-gossip v0.10.0-dev
├── futures v0.3.16
├── futures-timer v3.0.2
├── libp2p v0.40.0
├── log v0.4.14
├── lru v0.7.0
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sp-runtime v4.0.0
├── substrate-prometheus-endpoint v0.10.0-dev
└── tracing v0.1.29
[dev-dependencies]
├── async-std v1.10.0
├── quickcheck v1.0.3
└── substrate-test-runtime-client v2.0.0

### Offchain

client/offchain
├── Cargo.toml
├── README.md
└── src
    ├── api
    │   ├── http.rs
    │   └── timestamp.rs
    ├── api.rs
    └── lib.rs

**Dependencies**

sc-offchain v4.0.0-dev
├── bytes v1.1.0
├── fnv v1.0.7
├── futures v0.3.16
├── futures-timer v3.0.2
├── hex v0.4.3
├── hyper v0.14.16
├── hyper-rustls v0.22.1
├── num_cpus v1.13.1
├── once_cell v1.8.0
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── rand v0.7.3
├── sc-client-api v4.0.0-dev
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sc-utils v4.0.0-dev
├── sp-api v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-offchain v4.0.0-dev
├── sp-runtime v4.0.0
├── threadpool v1.8.1
└── tracing v0.1.29
[dev-dependencies]
├── lazy_static v1.4.0
├── sc-block-builder v0.10.0-dev
├── sc-client-db v0.10.0-dev
├── sc-transaction-pool v4.0.0-dev
├── sc-transaction-pool-api v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-tracing v4.0.0
├── substrate-test-runtime-client v2.0.0
└── tokio v1.15.0

### Peerset

client/peerset
├── Cargo.toml
├── README.md
├── src
│   ├── lib.rs
│   └── peersstate.rs
└── tests
    └── fuzz.rs

**Dependencies**

sc-peerset v4.0.0-dev
├── futures v0.3.16
├── libp2p v0.40.0
├── log v0.4.14
├── sc-utils v4.0.0-dev
├── serde_json v1.0.71
└── wasm-timer v0.2.5
[dev-dependencies]
└── rand v0.7.3

### Proposer-Metrics

client/proposer-metrics
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

**Dependencies**

sc-proposer-metrics v0.10.0-dev
├── log v0.4.14
└── substrate-prometheus-endpoint v0.10.0-dev

### RPC

client/rpc
├── Cargo.toml
├── README.md
└── src
    ├── author
    │   ├── mod.rs
    │   └── tests.rs
    ├── chain
    │   ├── chain_full.rs
    │   ├── mod.rs
    │   └── tests.rs
    ├── lib.rs
    ├── offchain
    │   ├── mod.rs
    │   └── tests.rs
    ├── state
    │   ├── mod.rs
    │   ├── state_full.rs
    │   └── tests.rs
    ├── system
    │   ├── mod.rs
    │   └── tests.rs
    └── testing.rs

**Dependencies**

sc-rpc v4.0.0-dev
├── futures v0.3.16
├── hash-db v0.15.2
├── jsonrpc-core v18.0.0
├── jsonrpc-pubsub v18.0.0
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── sc-block-builder v0.10.0-dev
├── sc-chain-spec v4.0.0-dev
├── sc-client-api v4.0.0-dev
├── sc-rpc-api v0.10.0-dev
├── sc-tracing v4.0.0-dev
├── sc-transaction-pool-api v4.0.0-dev
├── sc-utils v4.0.0-dev
├── serde_json v1.0.71
├── sp-api v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-keystore v0.10.0
├── sp-offchain v4.0.0-dev
├── sp-rpc v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-session v4.0.0-dev
└── sp-version v4.0.0-dev
[dev-dependencies]
├── assert_matches v1.5.0
├── lazy_static v1.4.0
├── sc-network v0.10.0-dev
│   [build-dependencies]
├── sc-transaction-pool v4.0.0-dev
├── sp-consensus v0.10.0-dev
├── sp-io v4.0.0
└── substrate-test-runtime-client v2.0.0

### RPC-API

client/rpc-api
├── Cargo.toml
├── README.md
└── src
    ├── author
    │   ├── error.rs
    │   ├── hash.rs
    │   └── mod.rs
    ├── chain
    │   ├── error.rs
    │   └── mod.rs
    ├── child_state
    │   └── mod.rs
    ├── errors.rs
    ├── helpers.rs
    ├── lib.rs
    ├── metadata.rs
    ├── offchain
    │   ├── error.rs
    │   └── mod.rs
    ├── policy.rs
    ├── state
    │   ├── error.rs
    │   ├── helpers.rs
    │   └── mod.rs
    └── system
        ├── error.rs
        ├── helpers.rs
        └── mod.rs

**Dependencies**

sc-rpc-api v0.10.0-dev
├── futures v0.3.16
├── jsonrpc-core v18.0.0
├── jsonrpc-core-client v18.0.0
├── jsonrpc-derive v18.0.0 (proc-macro)
├── jsonrpc-pubsub v18.0.0
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parking_lot v0.11.2
├── sc-chain-spec v4.0.0-dev
├── sc-transaction-pool-api v4.0.0-dev
├── serde v1.0.132
├── serde_json v1.0.71
├── sp-core v4.1.0-dev
├── sp-rpc v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-tracing v4.0.0
├── sp-version v4.0.0-dev
└── thiserror v1.0.30


### RPC-Servers

client/rpc-servers
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── middleware.rs

**Dependencies**

sc-rpc-server v4.0.0-dev
├── futures v0.3.16
├── jsonrpc-core v18.0.0
├── jsonrpc-http-server v18.0.0
├── jsonrpc-ipc-server v18.0.0
├── jsonrpc-pubsub v18.0.0
├── jsonrpc-ws-server v18.0.0
├── log v0.4.14
├── serde_json v1.0.71
├── substrate-prometheus-endpoint v0.10.0-dev
└── tokio v1.15.0

### Service

client/service
├── Cargo.toml
├── README.md
├── src
│   ├── builder.rs
│   ├── chain_ops
│   │   ├── check_block.rs
│   │   ├── export_blocks.rs
│   │   ├── export_raw_state.rs
│   │   ├── import_blocks.rs
│   │   ├── mod.rs
│   │   └── revert_chain.rs
│   ├── client
│   │   ├── block_rules.rs
│   │   ├── call_executor.rs
│   │   ├── client.rs
│   │   ├── genesis.rs
│   │   ├── mod.rs
│   │   ├── wasm_override.rs
│   │   └── wasm_substitutes.rs
│   ├── config.rs
│   ├── error.rs
│   ├── lib.rs
│   ├── metrics.rs
│   └── task_manager
│       ├── mod.rs
│       ├── prometheus_future.rs
│       └── tests.rs
└── test
    ├── Cargo.toml
    └── src
        ├── client
        │   ├── db.rs
        │   └── mod.rs
        └── lib.rs

### State-DB

client/state-db
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── noncanonical.rs
    ├── pruning.rs
    └── test.rs

**Dependencies**

sc-state-db v0.10.0-dev
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parity-util-mem v0.10.2
├── parity-util-mem-derive v0.1.0 (proc-macro)
├── parking_lot v0.11.2
├── sc-client-api v4.0.0-dev
└── sp-core v4.1.0-dev

### Sync-State-RPC

client/sync-state-rpc
├── Cargo.toml
└── src
    └── lib.rs

**Dependencies**

sc-sync-state-rpc v0.10.0-dev
├── jsonrpc-core v18.0.0
├── jsonrpc-core-client v18.0.0
├── jsonrpc-derive v18.0.0 (proc-macro)
├── parity-scale-codec v2.3.1
├── sc-chain-spec v4.0.0-dev
├── sc-client-api v4.0.0-dev
├── sc-consensus-babe v0.10.0-dev
├── sc-consensus-epochs v0.10.0-dev
├── sc-finality-grandpa v0.10.0-dev
├── sc-rpc-api v0.10.0-dev
├── serde v1.0.132
├── serde_json v1.0.71
├── sp-blockchain v4.0.0-dev
├── sp-runtime v4.0.0
└── thiserror v1.0.30

### Telemetry

client/telemetry
├── Cargo.toml
├── README.md
└── src
    ├── endpoints.rs
    ├── error.rs
    ├── lib.rs
    ├── node.rs
    └── transport.rs

**Dependencies**

sc-telemetry v4.0.0-dev
├── chrono v0.4.19
├── futures v0.3.16
├── libp2p v0.40.0
├── log v0.4.14
├── parking_lot v0.11.2
├── pin-project v1.0.8
├── rand v0.7.3
├── serde v1.0.132
├── serde_json v1.0.71
├── thiserror v1.0.30
└── wasm-timer v0.2.5

### Tracing

client/tracing
├── benches
│   └── bench.rs
├── Cargo.toml
├── proc-macro
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── README.md
└── src
    ├── block
    │   └── mod.rs
    ├── lib.rs
    └── logging
        ├── directives.rs
        ├── event_format.rs
        ├── fast_local_time.rs
        ├── layers
        │   ├── mod.rs
        │   └── prefix_layer.rs
        ├── mod.rs
        └── stderr_writer.rs

**Dependencies**

sc-tracing v4.0.0-dev
├── ansi_term v0.12.1
├── atty v0.2.14
├── chrono v0.4.19
├── lazy_static v1.4.0
├── libc v0.2.112
├── log v0.4.14
├── once_cell v1.8.0
├── parking_lot v0.11.2
├── regex v1.5.4
├── rustc-hash v1.1.0
├── sc-client-api v4.0.0-dev
├── sc-rpc-server v4.0.0-dev
├── sc-tracing-proc-macro v4.0.0-dev (proc-macro)
├── serde v1.0.132
├── sp-api v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-rpc v4.0.0-dev
├── sp-runtime v4.0.0
├── sp-tracing v4.0.0
├── thiserror v1.0.30
├── tracing v0.1.29
├── tracing-log v0.1.2
└── tracing-subscriber v0.2.25
[dev-dependencies]
└── criterion v0.3.5

### Transaction-Pool

client/transaction-pool
├── api
│   ├── Cargo.toml
│   └── src
│       ├── error.rs
│       └── lib.rs
├── benches
│   └── basics.rs
├── Cargo.toml
├── README.md
├── src
│   ├── api.rs
│   ├── error.rs
│   ├── graph
│   │   ├── base_pool.rs
│   │   ├── future.rs
│   │   ├── listener.rs
│   │   ├── mod.rs
│   │   ├── pool.rs
│   │   ├── ready.rs
│   │   ├── rotator.rs
│   │   ├── tracked_map.rs
│   │   ├── validated_pool.rs
│   │   └── watcher.rs
│   ├── lib.rs
│   ├── metrics.rs
│   └── revalidation.rs
└── tests
    ├── pool.rs
    └── revalidation.rs

**Dependencies**

sc-transaction-pool v4.0.0-dev
├── futures v0.3.16
├── intervalier v0.4.0
├── linked-hash-map v0.5.4
├── log v0.4.14
├── parity-scale-codec v2.3.1
├── parity-util-mem v0.10.2
├── parking_lot v0.11.2
├── retain_mut v0.1.4
├── sc-client-api v4.0.0-dev
├── sc-transaction-pool-api v4.0.0-dev
├── sc-utils v4.0.0-dev
├── serde v1.0.132
├── sp-api v4.0.0-dev
├── sp-blockchain v4.0.0-dev
├── sp-core v4.1.0-dev
├── sp-runtime v4.0.0
├── sp-tracing v4.0.0
├── sp-transaction-pool v4.0.0-dev
├── substrate-prometheus-endpoint v0.10.0-dev
└── thiserror v1.0.30
[dev-dependencies]
├── assert_matches v1.5.0
├── criterion v0.3.5
├── hex v0.4.3
├── parity-scale-codec v2.3.1 (*)
├── sc-block-builder v0.10.0-dev
├── sp-consensus v0.10.0-dev
├── substrate-test-runtime v2.0.0
│   [build-dependencies]
├── substrate-test-runtime-client v2.0.0
└── substrate-test-runtime-transaction-pool v2.0.0

### Utils

client/utils
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── metrics.rs
    ├── mpsc.rs
    └── status_sinks.rs

**Dependencies**

sc-utils v4.0.0-dev
├── futures v0.3.16
├── futures-timer v3.0.2
├── lazy_static v1.4.0
└── prometheus v0.13.0
