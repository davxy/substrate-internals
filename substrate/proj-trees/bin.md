BINARIES
--------

### Node Template

bin/node-template
├── docker-compose.yml
├── docs
│   └── rust-setup.md
├── LICENSE
├── node
│   ├── build.rs
│   ├── Cargo.toml
│   └── src
│       ├── chain_spec.rs
│       ├── cli.rs
│       ├── command.rs
│       ├── main.rs
│       ├── rpc.rs
│       └── service.rs
├── pallets
│   └── template
│       ├── Cargo.toml
│       ├── README.md
│       └── src
│           ├── benchmarking.rs
│           ├── lib.rs
│           ├── mock.rs
│           └── tests.rs
├── README.md
├── runtime
│   ├── build.rs
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── scripts
│   ├── docker_run.sh
│   └── init.sh
└── shell.nix

### Node

bin/node
├── bench
│   ├── Cargo.toml
│   └── src
│       ├── common.rs
│       ├── construct.rs
│       ├── core.rs
│       ├── generator.rs
│       ├── import.rs
│       ├── main.rs
│       ├── simple_trie.rs
│       ├── state_sizes.rs
│       ├── tempdb.rs
│       ├── trie.rs
│       └── txpool.rs
├── cli
│   ├── benches
│   │   ├── block_production.rs
│   │   └── transaction_pool.rs
│   ├── bin
│   │   └── main.rs
│   ├── build.rs
│   ├── Cargo.toml
│   ├── doc
│   │   └── shell-completion.adoc
│   ├── res
│   │   └── flaming-fir.json
│   ├── src
│   │   ├── chain_spec.rs
│   │   ├── cli.rs
│   │   ├── command.rs
│   │   ├── lib.rs
│   │   └── service.rs
│   └── tests
│       ├── build_spec_works.rs
│       ├── check_block_works.rs
│       ├── common.rs
│       ├── export_import_flow.rs
│       ├── inspect_works.rs
│       ├── purge_chain_works.rs
│       ├── running_the_node_and_interrupt.rs
│       ├── telemetry.rs
│       ├── temp_base_path_works.rs
│       ├── version.rs
│       └── websocket_server.rs
├── executor
│   ├── benches
│   │   └── bench.rs
│   ├── Cargo.toml
│   ├── src
│   │   └── lib.rs
│   └── tests
│       ├── basic.rs
│       ├── common.rs
│       ├── fees.rs
│       └── submit_transaction.rs
├── inspect
│   ├── Cargo.toml
│   └── src
│       ├── cli.rs
│       ├── command.rs
│       └── lib.rs
├── primitives
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── rpc
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── runtime
│   ├── build.rs
│   ├── Cargo.toml
│   └── src
│       ├── constants.rs
│       ├── impls.rs
│       ├── lib.rs
│       └── voter_bags.rs
├── testing
│   ├── Cargo.toml
│   └── src
│       ├── bench.rs
│       ├── client.rs
│       ├── genesis.rs
│       ├── keyring.rs
│       └── lib.rs
└── test-runner-example
    ├── Cargo.toml
    └── src
        └── lib.rs

### Utils

bin/utils
├── chain-spec-builder
│   ├── build.rs
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       └── main.rs
└── subkey
    ├── Cargo.toml
    ├── README.md
    ├── SECURITY.md
    └── src
        ├── lib.rs
        └── main.rs