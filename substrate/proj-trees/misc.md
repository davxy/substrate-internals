Miscellanea
===========

### Root files

.
├── bin/
├── .cargo/
├── Cargo.lock
├── Cargo.toml
├── client/
├── docker/
├── .dockerignore
├── docs/
├── .editorconfig
├── frame/
├── .git/
├── .gitattributes
├── .github/
├── .gitignore
├── .gitlab-ci.yml
├── HEADER-APACHE2
├── HEADER-GPL3
├── LICENSE-APACHE2
├── LICENSE-GPL3
├── .maintain/
├── primitives/
├── Process.json
├── README.md
├── rustfmt.toml
├── shell.nix
├── substrate-tree.md
├── test-utils/
├── utils/
├── .vscode/
└── workspace.code-workspace


### Docker

docker
├── build.sh
├── README.md
└── substrate_builder.Dockerfile

### Docs

docs
├── CHANGELOG.md
├── CODE_OF_CONDUCT.md
├── CODEOWNERS
├── CONTRIBUTING.adoc
├── media
│   └── sub.gif
├── node-template-release.md
├── PULL_REQUEST_TEMPLATE.md
├── README.adoc
├── SECURITY.md
├── Structure.adoc
├── STYLE_GUIDE.md
├── Upgrade.md
└── Upgrading-2.0-to-3.0.md


### Github

.github/
├── allowed-actions.js
├── dependabot.yml
├── stale.yml
└── workflows
    ├── auto-label-issues.yml
    ├── auto-label-prs.yml
    ├── bench_gh_gl.yaml
    ├── burnin-label-notification.yml
    ├── check-labels.yml
    ├── md-link-check.yml
    ├── mlc_config.json
    ├── monthly-tag.yml
    ├── polkadot-companion-labels.yml
    ├── release-bot.yml
    ├── release-tagging.yml
    └── trigger-review-pipeline.yml


### Maintain

.maintain/
├── build-only-wasm.sh
├── common
│   └── lib.sh
├── deny.toml
├── docker
│   ├── subkey.Dockerfile
│   └── substrate.Dockerfile
├── docs-index-tpl.ejs
├── ensure-deps.sh
├── frame-weight-template.hbs
├── getgoing.sh
├── github
│   └── check_labels.sh
├── gitlab
│   ├── check_runtime.sh
│   ├── check_signed.sh
│   ├── generate_changelog.sh
│   ├── publish_draft_release.sh
│   └── skip_if_draft.sh
├── init.sh
├── local-docker-test-network
│   ├── docker-compose.yml
│   ├── grafana
│   │   └── provisioning
│   │       ├── dashboards
│   │       │   └── dashboards.yml
│   │       └── datasources
│   │           └── datasource.yml
│   └── prometheus
│       └── prometheus.yml
├── monitoring
│   ├── alerting-rules
│   │   ├── alerting-rules.yaml
│   │   └── alerting-rule-tests.yaml
│   └── grafana-dashboards
│       ├── README_dashboard.md
│       ├── substrate-networking.json
│       └── substrate-service-tasks.json
├── node-template-release
│   ├── Cargo.toml
│   └── src
│       └── main.rs
├── node-template-release.sh
├── rename-crates-for-2.0.sh
├── runtime-dep.py
├── update-copyright.sh
└── update-deps.sh
