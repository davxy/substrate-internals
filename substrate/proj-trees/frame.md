FRAME
=====

### Support

frame/support
├── Cargo.toml
├── procedural
│   ├── Cargo.toml
│   ├── src
│   │   ├── clone_no_bound.rs
│   │   ├── construct_runtime
│   │   │   ├── expand
│   │   │   │   ├── call.rs
│   │   │   │   ├── config.rs
│   │   │   │   ├── event.rs
│   │   │   │   ├── inherent.rs
│   │   │   │   ├── metadata.rs
│   │   │   │   ├── mod.rs
│   │   │   │   ├── origin.rs
│   │   │   │   └── unsigned.rs
│   │   │   ├── mod.rs
│   │   │   └── parse.rs
│   │   ├── crate_version.rs
│   │   ├── debug_no_bound.rs
│   │   ├── default_no_bound.rs
│   │   ├── dummy_part_checker.rs
│   │   ├── key_prefix.rs
│   │   ├── lib.rs
│   │   ├── match_and_insert.rs
│   │   ├── pallet
│   │   │   ├── expand
│   │   │   │   ├── call.rs
│   │   │   │   ├── config.rs
│   │   │   │   ├── constants.rs
│   │   │   │   ├── error.rs
│   │   │   │   ├── event.rs
│   │   │   │   ├── genesis_build.rs
│   │   │   │   ├── genesis_config.rs
│   │   │   │   ├── hooks.rs
│   │   │   │   ├── inherent.rs
│   │   │   │   ├── instances.rs
│   │   │   │   ├── mod.rs
│   │   │   │   ├── origin.rs
│   │   │   │   ├── pallet_struct.rs
│   │   │   │   ├── storage.rs
│   │   │   │   ├── store_trait.rs
│   │   │   │   ├── tt_default_parts.rs
│   │   │   │   ├── type_value.rs
│   │   │   │   └── validate_unsigned.rs
│   │   │   ├── mod.rs
│   │   │   └── parse
│   │   │       ├── call.rs
│   │   │       ├── config.rs
│   │   │       ├── error.rs
│   │   │       ├── event.rs
│   │   │       ├── extra_constants.rs
│   │   │       ├── genesis_build.rs
│   │   │       ├── genesis_config.rs
│   │   │       ├── helper.rs
│   │   │       ├── hooks.rs
│   │   │       ├── inherent.rs
│   │   │       ├── mod.rs
│   │   │       ├── origin.rs
│   │   │       ├── pallet_struct.rs
│   │   │       ├── storage.rs
│   │   │       ├── type_value.rs
│   │   │       └── validate_unsigned.rs
│   │   ├── partial_eq_no_bound.rs
│   │   ├── storage
│   │   │   ├── genesis_config
│   │   │   │   ├── builder_def.rs
│   │   │   │   ├── genesis_config_def.rs
│   │   │   │   └── mod.rs
│   │   │   ├── getters.rs
│   │   │   ├── instance_trait.rs
│   │   │   ├── metadata.rs
│   │   │   ├── mod.rs
│   │   │   ├── parse.rs
│   │   │   ├── print_pallet_upgrade.rs
│   │   │   ├── storage_info.rs
│   │   │   ├── storage_struct.rs
│   │   │   └── store_trait.rs
│   │   └── transactional.rs
│   └── tools
│       ├── Cargo.toml
│       ├── derive
│       │   ├── Cargo.toml
│       │   └── src
│       │       └── lib.rs
│       └── src
│           ├── lib.rs
│           └── syn_ext.rs
├── README.md
├── src
│   ├── dispatch.rs
│   ├── error.rs
│   ├── event.rs
│   ├── hash.rs
│   ├── inherent.rs
│   ├── instances.rs
│   ├── lib.rs
│   ├── migrations.rs
│   ├── storage
│   │   ├── bounded_btree_map.rs
│   │   ├── bounded_btree_set.rs
│   │   ├── bounded_vec.rs
│   │   ├── child.rs
│   │   ├── generator
│   │   │   ├── double_map.rs
│   │   │   ├── map.rs
│   │   │   ├── mod.rs
│   │   │   ├── nmap.rs
│   │   │   └── value.rs
│   │   ├── hashed.rs
│   │   ├── migration.rs
│   │   ├── mod.rs
│   │   ├── types
│   │   │   ├── counted_map.rs
│   │   │   ├── double_map.rs
│   │   │   ├── key.rs
│   │   │   ├── map.rs
│   │   │   ├── mod.rs
│   │   │   ├── nmap.rs
│   │   │   └── value.rs
│   │   ├── unhashed.rs
│   │   └── weak_bounded_vec.rs
│   ├── traits
│   │   ├── dispatch.rs
│   │   ├── filter.rs
│   │   ├── hooks.rs
│   │   ├── members.rs
│   │   ├── metadata.rs
│   │   ├── misc.rs
│   │   ├── randomness.rs
│   │   ├── schedule.rs
│   │   ├── storage.rs
│   │   ├── stored_map.rs
│   │   ├── tokens
│   │   │   ├── currency
│   │   │   │   ├── lockable.rs
│   │   │   │   └── reservable.rs
│   │   │   ├── currency.rs
│   │   │   ├── fungible
│   │   │   │   ├── balanced.rs
│   │   │   │   └── imbalance.rs
│   │   │   ├── fungible.rs
│   │   │   ├── fungibles
│   │   │   │   ├── approvals.rs
│   │   │   │   ├── balanced.rs
│   │   │   │   ├── imbalance.rs
│   │   │   │   └── metadata.rs
│   │   │   ├── fungibles.rs
│   │   │   ├── imbalance
│   │   │   │   ├── on_unbalanced.rs
│   │   │   │   ├── signed_imbalance.rs
│   │   │   │   └── split_two_ways.rs
│   │   │   ├── imbalance.rs
│   │   │   ├── misc.rs
│   │   │   ├── nonfungible.rs
│   │   │   └── nonfungibles.rs
│   │   ├── tokens.rs
│   │   ├── validation.rs
│   │   └── voting.rs
│   ├── traits.rs
│   └── weights.rs
└── test
    ├── Cargo.toml
    ├── compile_pass
    │   ├── Cargo.toml
    │   └── src
    │       └── lib.rs
    ├── pallet
    │   ├── Cargo.toml
    │   └── src
    │       └── lib.rs
    ├── src
    │   ├── lib.rs
    │   └── pallet_version.rs
    └── tests
        ├── construct_runtime.rs
        ├── construct_runtime_ui
        │   ├── abundant_where_param.rs
        │   ├── abundant_where_param.stderr
        │   ├── both_use_and_excluded_parts.rs
        │   ├── both_use_and_excluded_parts.stderr
        │   ├── conflicting_index_2.rs
        │   ├── conflicting_index_2.stderr
        │   ├── conflicting_index.rs
        │   ├── conflicting_index.stderr
        │   ├── conflicting_module_name.rs
        │   ├── conflicting_module_name.stderr
        │   ├── double_module_parts.rs
        │   ├── double_module_parts.stderr
        │   ├── duplicate_exclude.rs
        │   ├── duplicate_exclude.stderr
        │   ├── empty_pallet_path.rs
        │   ├── empty_pallet_path.stderr
        │   ├── exclude_missspell.rs
        │   ├── exclude_missspell.stderr
        │   ├── exclude_undefined_part.rs
        │   ├── exclude_undefined_part.stderr
        │   ├── generics_in_invalid_module.rs
        │   ├── generics_in_invalid_module.stderr
        │   ├── invalid_module_details_keyword.rs
        │   ├── invalid_module_details_keyword.stderr
        │   ├── invalid_module_details.rs
        │   ├── invalid_module_details.stderr
        │   ├── invalid_module_entry.rs
        │   ├── invalid_module_entry.stderr
        │   ├── invalid_token_after_module.rs
        │   ├── invalid_token_after_module.stderr
        │   ├── invalid_token_after_name.rs
        │   ├── invalid_token_after_name.stderr
        │   ├── invalid_where_param.rs
        │   ├── invalid_where_param.stderr
        │   ├── missing_event_generic_on_module_with_instance.rs
        │   ├── missing_event_generic_on_module_with_instance.stderr
        │   ├── missing_module_instance.rs
        │   ├── missing_module_instance.stderr
        │   ├── missing_origin_generic_on_module_with_instance.rs
        │   ├── missing_origin_generic_on_module_with_instance.stderr
        │   ├── missing_system_module.rs
        │   ├── missing_system_module.stderr
        │   ├── missing_where_block.rs
        │   ├── missing_where_block.stderr
        │   ├── missing_where_param.rs
        │   ├── missing_where_param.stderr
        │   ├── more_than_256_modules.rs
        │   ├── more_than_256_modules.stderr
        │   ├── no_comma_after_where.rs
        │   ├── no_comma_after_where.stderr
        │   ├── no_std_genesis_config.rs
        │   ├── no_std_genesis_config.stderr
        │   ├── old_unsupported_pallet_decl.rs
        │   ├── old_unsupported_pallet_decl.stderr
        │   ├── undefined_call_part.rs
        │   ├── undefined_call_part.stderr
        │   ├── undefined_event_part.rs
        │   ├── undefined_event_part.stderr
        │   ├── undefined_genesis_config_part.rs
        │   ├── undefined_genesis_config_part.stderr
        │   ├── undefined_inherent_part.rs
        │   ├── undefined_inherent_part.stderr
        │   ├── undefined_origin_part.rs
        │   ├── undefined_origin_part.stderr
        │   ├── undefined_validate_unsigned_part.rs
        │   ├── undefined_validate_unsigned_part.stderr
        │   ├── use_undefined_part.rs
        │   └── use_undefined_part.stderr
        ├── construct_runtime_ui.rs
        ├── decl_module_ui
        │   ├── reserved_keyword_two_times_integrity_test.rs
        │   ├── reserved_keyword_two_times_integrity_test.stderr
        │   ├── reserved_keyword_two_times_on_initialize.rs
        │   └── reserved_keyword_two_times_on_initialize.stderr
        ├── decl_module_ui.rs
        ├── decl_storage.rs
        ├── decl_storage_ui
        │   ├── config_duplicate.rs
        │   ├── config_duplicate.stderr
        │   ├── config_get_duplicate.rs
        │   ├── config_get_duplicate.stderr
        │   ├── get_duplicate.rs
        │   └── get_duplicate.stderr
        ├── decl_storage_ui.rs
        ├── derive_no_bound.rs
        ├── derive_no_bound_ui
        │   ├── clone.rs
        │   ├── clone.stderr
        │   ├── debug.rs
        │   ├── debug.stderr
        │   ├── default.rs
        │   ├── default.stderr
        │   ├── eq.rs
        │   ├── eq.stderr
        │   ├── partial_eq.rs
        │   └── partial_eq.stderr
        ├── derive_no_bound_ui.rs
        ├── final_keys.rs
        ├── genesisconfig.rs
        ├── instance.rs
        ├── issue2219.rs
        ├── pallet_compatibility_instance.rs
        ├── pallet_compatibility.rs
        ├── pallet_instance.rs
        ├── pallet.rs
        ├── pallet_ui
        │   ├── attr_non_empty.rs
        │   ├── attr_non_empty.stderr
        │   ├── call_argument_invalid_bound_2.rs
        │   ├── call_argument_invalid_bound_2.stderr
        │   ├── call_argument_invalid_bound_3.rs
        │   ├── call_argument_invalid_bound_3.stderr
        │   ├── call_argument_invalid_bound.rs
        │   ├── call_argument_invalid_bound.stderr
        │   ├── call_invalid_const.rs
        │   ├── call_invalid_const.stderr
        │   ├── call_invalid_origin_type.rs
        │   ├── call_invalid_origin_type.stderr
        │   ├── call_invalid_return.rs
        │   ├── call_invalid_return.stderr
        │   ├── call_invalid_vis_2.rs
        │   ├── call_invalid_vis_2.stderr
        │   ├── call_invalid_vis.rs
        │   ├── call_invalid_vis.stderr
        │   ├── call_missing_weight.rs
        │   ├── call_missing_weight.stderr
        │   ├── call_no_origin.rs
        │   ├── call_no_origin.stderr
        │   ├── call_no_return.rs
        │   ├── call_no_return.stderr
        │   ├── duplicate_call_attr.rs
        │   ├── duplicate_call_attr.stderr
        │   ├── duplicate_storage_prefix.rs
        │   ├── duplicate_storage_prefix.stderr
        │   ├── duplicate_store_attr.rs
        │   ├── duplicate_store_attr.stderr
        │   ├── error_no_fieldless.rs
        │   ├── error_no_fieldless.stderr
        │   ├── error_where_clause.rs
        │   ├── error_where_clause.stderr
        │   ├── error_wrong_item_name.rs
        │   ├── error_wrong_item_name.stderr
        │   ├── error_wrong_item.rs
        │   ├── error_wrong_item.stderr
        │   ├── event_field_not_member.rs
        │   ├── event_field_not_member.stderr
        │   ├── event_not_in_trait.rs
        │   ├── event_not_in_trait.stderr
        │   ├── event_type_invalid_bound_2.rs
        │   ├── event_type_invalid_bound_2.stderr
        │   ├── event_type_invalid_bound.rs
        │   ├── event_type_invalid_bound.stderr
        │   ├── event_wrong_item_name.rs
        │   ├── event_wrong_item_name.stderr
        │   ├── event_wrong_item.rs
        │   ├── event_wrong_item.stderr
        │   ├── genesis_default_not_satisfied.rs
        │   ├── genesis_default_not_satisfied.stderr
        │   ├── genesis_inconsistent_build_config.rs
        │   ├── genesis_inconsistent_build_config.stderr
        │   ├── genesis_invalid_generic.rs
        │   ├── genesis_invalid_generic.stderr
        │   ├── genesis_wrong_name.rs
        │   ├── genesis_wrong_name.stderr
        │   ├── hooks_invalid_item.rs
        │   ├── hooks_invalid_item.stderr
        │   ├── inconsistent_instance_1.rs
        │   ├── inconsistent_instance_1.stderr
        │   ├── inconsistent_instance_2.rs
        │   ├── inconsistent_instance_2.stderr
        │   ├── inherent_check_inner_span.rs
        │   ├── inherent_check_inner_span.stderr
        │   ├── inherent_invalid_item.rs
        │   ├── inherent_invalid_item.stderr
        │   ├── mod_not_inlined.rs
        │   ├── mod_not_inlined.stderr
        │   ├── pass
        │   │   └── trait_constant_valid_bounds.rs
        │   ├── storage_ensure_span_are_ok_on_wrong_gen.rs
        │   ├── storage_ensure_span_are_ok_on_wrong_gen.stderr
        │   ├── storage_ensure_span_are_ok_on_wrong_gen_unnamed.rs
        │   ├── storage_ensure_span_are_ok_on_wrong_gen_unnamed.stderr
        │   ├── storage_incomplete_item.rs
        │   ├── storage_incomplete_item.stderr
        │   ├── storage_info_unsatisfied_nmap.rs
        │   ├── storage_info_unsatisfied_nmap.stderr
        │   ├── storage_info_unsatisfied.rs
        │   ├── storage_info_unsatisfied.stderr
        │   ├── storage_invalid_attribute.rs
        │   ├── storage_invalid_attribute.stderr
        │   ├── storage_invalid_first_generic.rs
        │   ├── storage_invalid_first_generic.stderr
        │   ├── storage_invalid_rename_value.rs
        │   ├── storage_invalid_rename_value.stderr
        │   ├── storage_multiple_getters.rs
        │   ├── storage_multiple_getters.stderr
        │   ├── storage_multiple_renames.rs
        │   ├── storage_multiple_renames.stderr
        │   ├── storage_not_storage_type.rs
        │   ├── storage_not_storage_type.stderr
        │   ├── storage_value_duplicate_named_generic.rs
        │   ├── storage_value_duplicate_named_generic.stderr
        │   ├── storage_value_generic_named_and_unnamed.rs
        │   ├── storage_value_generic_named_and_unnamed.stderr
        │   ├── storage_value_no_generic.rs
        │   ├── storage_value_no_generic.stderr
        │   ├── storage_value_unexpected_named_generic.rs
        │   ├── storage_value_unexpected_named_generic.stderr
        │   ├── storage_wrong_item.rs
        │   ├── storage_wrong_item.stderr
        │   ├── store_trait_leak_private.rs
        │   ├── store_trait_leak_private.stderr
        │   ├── trait_constant_invalid_bound_lifetime.rs
        │   ├── trait_constant_invalid_bound_lifetime.stderr
        │   ├── trait_constant_invalid_bound.rs
        │   ├── trait_constant_invalid_bound.stderr
        │   ├── trait_invalid_item.rs
        │   ├── trait_invalid_item.stderr
        │   ├── trait_no_supertrait.rs
        │   ├── trait_no_supertrait.stderr
        │   ├── type_value_error_in_block.rs
        │   ├── type_value_error_in_block.stderr
        │   ├── type_value_forgotten_where_clause.rs
        │   ├── type_value_forgotten_where_clause.stderr
        │   ├── type_value_invalid_item.rs
        │   ├── type_value_invalid_item.stderr
        │   ├── type_value_no_return.rs
        │   └── type_value_no_return.stderr
        ├── pallet_ui.rs
        ├── pallet_with_name_trait_is_valid.rs
        ├── storage_transaction.rs
        └── system.rs


### Pallets

frame/assets
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── extra_mutator.rs
    ├── functions.rs
    ├── impl_fungibles.rs
    ├── impl_stored_map.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    ├── types.rs
    └── weights.rs

frame/atomic-swap
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    └── tests.rs

frame/aura
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── migrations.rs
    ├── mock.rs
    └── tests.rs

frame/authority-discovery
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

frame/authorship
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

frame/babe
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── default_weights.rs
    ├── equivocation.rs
    ├── lib.rs
    ├── mock.rs
    ├── randomness.rs
    └── tests.rs

frame/bags-list
├── Cargo.toml
├── fuzzer
│   ├── Cargo.toml
│   └── src
│       └── main.rs
├── remote-tests
│   ├── Cargo.toml
│   └── src
│       ├── lib.rs
│       ├── migration.rs
│       ├── sanity_check.rs
│       └── snapshot.rs
└── src
    ├── benchmarks.rs
    ├── lib.rs
    ├── list
    │   ├── mod.rs
    │   └── tests.rs
    ├── migrations.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/balances
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests_composite.rs
    ├── tests_local.rs
    ├── tests_reentrancy.rs
    ├── tests.rs
    └── weights.rs

frame/beefy
├── Cargo.toml
└── src
    ├── lib.rs
    ├── mock.rs
    └── tests.rs

frame/beefy-mmr
├── Cargo.toml
├── primitives
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
└── src
    ├── lib.rs
    ├── mock.rs
    └── tests.rs

frame/benchmarking
├── Cargo.toml
├── README.md
└── src
    ├── analysis.rs
    ├── baseline.rs
    ├── lib.rs
    ├── tests_instance.rs
    ├── tests.rs
    ├── utils.rs
    └── weights.rs

frame/bounties
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── migrations
    │   ├── mod.rs
    │   └── v4.rs
    ├── tests.rs
    └── weights.rs

frame/child-bounties
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests.rs
    └── weights.rs

frame/collective
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── migrations
    │   ├── mod.rs
    │   └── v4.rs
    ├── tests.rs
    └── weights.rs

frame/contracts
├── benchmarks
│   ├── ink_erc20.json
│   ├── ink_erc20_test.wasm
│   ├── ink_erc20.wasm
│   ├── README.md
│   ├── solang_erc20.json
│   └── solang_erc20.wasm
├── Cargo.toml
├── CHANGELOG.md
├── common
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       └── lib.rs
├── fixtures
│   ├── caller_contract.wat
│   ├── call_return_code.wat
│   ├── call_runtime.wat
│   ├── call_with_limit.wat
│   ├── chain_extension.wat
│   ├── crypto_hashes.wat
│   ├── debug_message_invalid_utf8.wat
│   ├── debug_message_logging_disabled.wat
│   ├── debug_message_works.wat
│   ├── destroy_and_transfer.wat
│   ├── drain.wat
│   ├── dummy.wat
│   ├── ecdsa_recover.wat
│   ├── event_size.wat
│   ├── instantiate_return_code.wat
│   ├── multi_store.wat
│   ├── ok_trap_revert.wat
│   ├── return_from_start_fn.wat
│   ├── return_with_data.wat
│   ├── run_out_of_gas.wat
│   ├── self_destructing_constructor.wat
│   ├── self_destruct.wat
│   ├── set_empty_storage.wat
│   ├── storage_size.wat
│   ├── store.wat
│   └── transfer_return_code.wat
├── proc-macro
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── README.md
├── rpc
│   ├── Cargo.toml
│   ├── README.md
│   ├── runtime-api
│   │   ├── Cargo.toml
│   │   ├── README.md
│   │   └── src
│   │       └── lib.rs
│   └── src
│       └── lib.rs
└── src
    ├── benchmarking
    │   ├── code.rs
    │   ├── mod.rs
    │   └── sandbox.rs
    ├── chain_extension.rs
    ├── exec.rs
    ├── gas.rs
    ├── lib.rs
    ├── migration.rs
    ├── schedule.rs
    ├── storage
    │   └── meter.rs
    ├── storage.rs
    ├── tests.rs
    ├── wasm
    │   ├── code_cache.rs
    │   ├── env_def
    │   │   ├── macros.rs
    │   │   └── mod.rs
    │   ├── mod.rs
    │   ├── prepare.rs
    │   └── runtime.rs
    └── weights.rs

frame/democracy
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── conviction.rs
    ├── lib.rs
    ├── tests
    │   ├── cancellation.rs
    │   ├── decoders.rs
    │   ├── delegation.rs
    │   ├── external_proposing.rs
    │   ├── fast_tracking.rs
    │   ├── lock_voting.rs
    │   ├── preimage.rs
    │   ├── public_proposals.rs
    │   ├── scheduling.rs
    │   └── voting.rs
    ├── tests.rs
    ├── types.rs
    ├── vote.rs
    ├── vote_threshold.rs
    └── weights.rs

frame/election-provider-multi-phase
├── Cargo.toml
└── src
    ├── benchmarking.rs
    ├── helpers.rs
    ├── lib.rs
    ├── mock.rs
    ├── signed.rs
    ├── unsigned.rs
    └── weights.rs

frame/election-provider-support
├── Cargo.toml
└── src
    ├── lib.rs
    └── onchain.rs

frame/elections-phragmen
├── Cargo.toml
├── CHANGELOG.md
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── migrations
    │   ├── mod.rs
    │   ├── v3.rs
    │   └── v4.rs
    └── weights.rs

frame/examples
├── basic
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── benchmarking.rs
│       ├── lib.rs
│       ├── tests.rs
│       └── weights.rs
├── offchain-worker
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── lib.rs
│       └── tests.rs
└── parallel
    ├── Cargo.toml
    ├── README.md
    └── src
        ├── lib.rs
        └── tests.rs

frame/executive
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

frame/gilt
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/grandpa
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── default_weights.rs
    ├── equivocation.rs
    ├── lib.rs
    ├── migrations
    │   └── v4.rs
    ├── migrations.rs
    ├── mock.rs
    └── tests.rs

frame/identity
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests.rs
    ├── types.rs
    └── weights.rs

frame/im-online
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/indices
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/lottery
├── Cargo.toml
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/membership
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── migrations
    │   ├── mod.rs
    │   └── v4.rs
    └── weights.rs

frame/merkle-mountain-range
├── Cargo.toml
├── primitives
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── rpc
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
└── src
    ├── benchmarking.rs
    ├── default_weights.rs
    ├── lib.rs
    ├── mmr
    │   ├── mmr.rs
    │   ├── mod.rs
    │   ├── storage.rs
    │   └── utils.rs
    ├── mock.rs
    └── tests.rs

frame/multisig
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests.rs
    └── weights.rs

frame/nicks
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

frame/node-authorization
├── Cargo.toml
└── src
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/offences
├── benchmarking
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── lib.rs
│       └── mock.rs
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── migration.rs
    ├── mock.rs
    └── tests.rs

frame/preimage
├── Cargo.toml
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/proxy
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests.rs
    └── weights.rs

frame/randomness-collective-flip
├── Cargo.toml
├── README.md
└── src
    └── lib.rs

frame/recovery
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── mock.rs
    └── tests.rs

frame/scheduler
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/scored-pool
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── mock.rs
    └── tests.rs

frame/session
├── benchmarking
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── lib.rs
│       └── mock.rs
├── Cargo.toml
├── README.md
└── src
    ├── historical
    │   ├── mod.rs
    │   ├── offchain.rs
    │   ├── onchain.rs
    │   └── shared.rs
    ├── lib.rs
    ├── migrations
    │   ├── mod.rs
    │   └── v1.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/society
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── mock.rs
    └── tests.rs

frame/staking
├── Cargo.toml
├── README.md
├── reward-curve
│   ├── Cargo.toml
│   ├── src
│   │   ├── lib.rs
│   │   └── log.rs
│   └── tests
│       └── test.rs
├── reward-fn
│   ├── Cargo.toml
│   ├── src
│   │   └── lib.rs
│   └── tests
│       └── test.rs
└── src
    ├── benchmarking.rs
    ├── inflation.rs
    ├── lib.rs
    ├── migrations.rs
    ├── mock.rs
    ├── pallet
    │   ├── impls.rs
    │   └── mod.rs
    ├── slashing.rs
    ├── testing_utils.rs
    ├── tests.rs
    └── weights.rs

frame/sudo
├── Cargo.toml
├── README.md
└── src
    ├── lib.rs
    ├── mock.rs
    └── tests.rs

frame/system
├── benches
│   └── bench.rs
├── benchmarking
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── lib.rs
│       └── mock.rs
├── Cargo.toml
├── README.md
├── rpc
│   └── runtime-api
│       ├── Cargo.toml
│       ├── README.md
│       └── src
│           └── lib.rs
└── src
    ├── extensions
    │   ├── check_genesis.rs
    │   ├── check_mortality.rs
    │   ├── check_nonce.rs
    │   ├── check_non_zero_sender.rs
    │   ├── check_spec_version.rs
    │   ├── check_tx_version.rs
    │   ├── check_weight.rs
    │   └── mod.rs
    ├── lib.rs
    ├── limits.rs
    ├── migrations
    │   └── mod.rs
    ├── mocking.rs
    ├── mock.rs
    ├── offchain.rs
    ├── tests.rs
    └── weights.rs

frame/timestamp
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    └── weights.rs

frame/tips
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── migrations
    │   ├── mod.rs
    │   └── v4.rs
    ├── tests.rs
    └── weights.rs

frame/transaction-payment
├── asset-tx-payment
│   ├── Cargo.toml
│   ├── README.md
│   └── src
│       ├── lib.rs
│       ├── payment.rs
│       └── tests.rs
├── Cargo.toml
├── README.md
├── rpc
│   ├── Cargo.toml
│   ├── README.md
│   ├── runtime-api
│   │   ├── Cargo.toml
│   │   ├── README.md
│   │   └── src
│   │       └── lib.rs
│   └── src
│       └── lib.rs
└── src
    ├── lib.rs
    ├── payment.rs
    └── types.rs

frame/transaction-storage
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── mock.rs
    ├── tests.rs
    └── weights.rs

frame/treasury
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests.rs
    └── weights.rs

frame/try-runtime
├── Cargo.toml
└── src
    └── lib.rs

frame/uniques
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── functions.rs
    ├── impl_nonfungibles.rs
    ├── lib.rs
    ├── migration.rs
    ├── mock.rs
    ├── tests.rs
    ├── types.rs
    └── weights.rs

frame/utility
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── tests.rs
    └── weights.rs

frame/vesting
├── Cargo.toml
├── README.md
└── src
    ├── benchmarking.rs
    ├── lib.rs
    ├── migrations.rs
    ├── mock.rs
    ├── tests.rs
    ├── vesting_info.rs
    └── weights.rs
