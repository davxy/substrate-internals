Node Cryptographic Keys
=======================

Network Key
-----------

Ed25519 key mainly used by libp2p to authenticate traffic between nodes.

The CLI allows to specify the following params:
- `--node-key-type`: currently only supports ed25519.
- `--node-key-file`: file containing raw or hex encoded ed25519 secret key.
- `--node-key`: directly pass the node key via command line (has precedence).

If not explicitly specified or a key file is not given, then the key is randomly
generated during node startup and stored in the `<base-path>/network` folder.

A node key can be generated and parsed using `subkey` tool

    $ subkey generate-node-key --file <FILE>
    <PeerId output>

    $ subey inspect-node-key --file <FILE>
    <PeerId output>

Validator Keys
--------------

Validators encryption keys are maintained within the node keystore.

The keystore is in the `keystore` folder, in the same root as the database and
specified with the `--base-path` CLI option.

### Key generation

Each key has its own schema, so pay attention when generating a key before
adding it to the keystore.

The key types used by the provided node template are:
- aura: sr25519
- grandpa: ed25519

For example to generate an sr25519 key to be used for Aura:

    $ subkey generate --scheme sr25519

If we know the secret URI of a key and we wish to fetch its public key in order
to insert it in the validator keystore then the command is:

    $ subkey inspect --scheme sr25519 "//Alice"

### Adding a key

#### Via RPC

Once we have a key secret secret URI and its public key component we can add it
to the validator keystore via RPC using the method:

    author.insertKey(keyType, suri, publicKey)

With:
- keyType: the 4 characters key type that has been used to declare the key via
  the `app_crypto!` macro (e.g. "babe")
- suri: the secret URI: (e.g. "//Alice")
- publicKey: hex encoded public key (e.g. 0xd43593...6da27d)

The method can be easily invoked via `polkadot.js` or via command line
(e.g. via `curl`).

#### Via Node CLI

Keys within the keystore can be added using the `substrate` node CLI as well.
For example to add a `granpa` key to the "Bob" node keystore:

    $ ./substrate key insert --scheme ed25519 --key-type gran \
        --chain local --suri "//Bob" --base-path <DIR>

The keys will be added to the keystore folder `<base-path>/keystore` and can
thus be found by the validator when it requires it, automatically.

A validator node can contain more than one key of the same type.

The filename is a string representing the hex encoding of

    <key-type><public-key>

With key-type a 4 characters shortcut of the key type.

Example

    |62616265|d43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d|
    | "babe" |              public-key (32 bytes)                             |

The public key is as returned by subkey with URI = `//Alice`.

The file content contains the secret URI (aka `suri`) used to generate the key.

**BEAWARE**

After inserting the grandpa key the node needs to be restarted!!!

TODO: WHY?!?

### Key rotation

The node has an RPC method to generate a new set of session keys:

    author.rotateKeys()

The public keys are returned to the caller as a unique long hex encoded string.
For example:

    e49c0bfef13379446b098109322eea114576a630a9c4b776f9eb354f2036230c
    288aa2ca9b2babbd79207e0342da62613516bc3ae7f91530cc81f83620f3eb7a
    66529111e4692fbadfa58e5ee0b53ea8264de96c1a32fc1cf16b6df03e05fe32
    446eea16fd6ba42b269b298316f7a2409ca17f8dfc024196f9429d813c582d00

The corresponding secret keys are be stored within the keystore with file names
using the same pattern already described. For example

    |6772616e|e49c0bfef13379446b098109322eea114576a630a9c4b776f9eb354f2036230c|
    | gran   |                      public key                                |

This RPC call only takes care of creating a set of random keys within the
validator. If you want to use the keys in place of the current ones then these
must be added to the blockchain state using a transaction (extrinsic) to the
pallet that handles the session keys (e.g. session or authority-set pallets).

### Development Accounts

A common seed used for all development accounts is:

    "bottom drive obey lake curtain smoke basket hold race lonely fit walk"

Various development accounts are derived from this seed.

For example:

    subkey inspect //Alice

Implicitly prepends the common seed to the "//Alice" hard derivation path.

When starting a node we can specify that we want to use one development account
for all the required `SessionKeys` by explicitly passing the name of the
development account.

For example `--alice` option is a shortcut for `--name Alice --validator`, i.e.
session keys are derived for the well known URI "//Alice".

### Onchain keys

Keys used by validators should be registered onchain.

This happens via various ways, but commonly during genesis or via Governance or
Sudo.
