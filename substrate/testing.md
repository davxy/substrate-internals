Client Testing
==============

### Test Client creation

- `client/service/test/src/client/mod.rs`
- `test-utils/client/src/lib.rs`

- `TestClientBuilder`: builds a `Client` suitable for testing.
- The default `Backend` is constructed by the `with_default_backend` function.
  It is a `kvdb_memorydb`

- `PruningMode` in the Backend test is set to `Constrained` (maintains a pruning
  window).
  (This may be the pruning causing problems)

- The `build_with_longest_chain` returns both the `Client` and the
  `sc_consensus::LongestChain`
    -  calls `build_with_native_executor` that constructs a
       `NativeElseWasmExecutor` this is used to create a `LocalCallExecutor`.
    - as async executor sp-core::testing::TaskExecutor is used.
        - Implements `SpawnNamed`.
        - It uses `futures::executor::ThreadPool`

- Finally `build_with_executor` is called
    - Constructs genesis block (`genesis_storage() -> construct_genesis_block`)
    - Creates the `Client` structure (Client::new()).
        - Fetch blockchain info from backend and builds genesis only if no
          finalized state exists.
    - `LongestChain` chain select implementation is created with the current
      backend.

### Import blocks

- The block is build using the `BlockBuilder`
    - `builder = client.new_block_at(parent, digests, record_proofs)`
    - transfer extrinsics are pushed: `builder.push_transfer(Transfer)`
    - Block is built: `builder.build()`
        - block finalization is performed (calls runtime finalize api)

### NOT CLEAR

    struct Storage {
        top: StorageMap,
        children_default: HashMap<Vec<u8>, StorageChild>
    }

What is a StorageChild??? (Child trie storage data...)

### Important Notes

- A chain becomes the best one as soon as it contains a finalized block.

### Domande

- What is a block `Justification?`
  From docs: essentially a finality proof. The exact formulation vary between
  consensus algorithms (indeed it is a byte array). It is included withing a
  block and there can (potentially) be multiple justifications


THE ISSUE
=========

The place to get stale branches looks like is:

    `client/service/src/client/client.rs: Client::apply_finality_with_block_hash`


Hi guys. I managed to understand how the client unit tests works and where to
apply the modifications in order to - hopefully - fix the forsaken `BlockAux`
issue.

To proceed I need a missing info:

Given the following situation:

```
           /-> A3
          /
	G -> A1 -> A2
	  \
	   \-> B1 -> B2
            \
             \ -> B3
```

Where:
- Current best block is A2
- Last finalized block is G

Lets assume that B1 gets finalized, this result in the following situation:
- B1 becomes last finalized
- B1 also becomes new BEST (looks like B2 nor B3 are not immediatelly set as the
  new best... but this is not relevant for my question)

In the `Client::apply_finality_with_block_hash` I now need to find out the leafs
that are not descendents of the current new final blocks (i.e. A2 and A3).

These will be used in the finalized handler of BABE to reconstruct the path from
the prev finalized block (A3->A1->G, A2->A1->G) and cleanup the AuxData.

(Probably during cleanup I will pass through some node twice. I will see later
if there is some more clever method to do that).

>> Question <<
I see there is in the Client::Blockchain::leaves() method.
Looks like this returns all the leaves of the block tree.
To catch the stale ones I have to take all the leaves 'X' such that the path
from the finalized one B1 to X has some "retracted" nodes.
This should work... is there a better method?
