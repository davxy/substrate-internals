Inherents
=========

- Extrinsic type added to the block by the author.
- Not from transaction pool and not gossipped.

### Creation

- Client authorship subsystem triggers the inherents creation.
    - E.g. via `babe::BabeParams::create_inherent_data_providers` closure.
    - The `create_inherent_data_providers` return a tuple with each element
      implementing the `InherentDataProvider` trait.

- Inherents are created by components implementing the
  `sp_inherents::InherentDataProvider` trait.
    - Examples of implementors: `ParachainsInherentDataProvider` (for polkadot),
      aura/babe/timestamp `InherentDataProvider`.

- On block authoring the `InherentDataProvider::provide_inherent_data` is called
  for each member.
    - The `provide_inherent_data(&self, &mut InherentData)` pushes into the
      `InherentData` the data specific to that subsystem keyed by an
      `InherentIdentifier` (8 bytes array newtype) and the value an `Encode`able
      value.

- The inherent data is returned by the `slot_next` stream withing the `SlotInfo`
  structure.

- The block is authoried via the `Proposer::propose` method of `basic_authorship`
    - here the `propose_with` is called and this is where the inherent data is
      used to create inherent extrinsic using the runtime:
      `block_builder.create_inherents(inherent_data)`
    - In `create_inherents` the `inherent_extrinsics` of the runtime api is
      called.

- In runtime the `InherentData` is passed to each pallet implementing the
  `ProvideInherent` trait (e.g. timestamp pallet)


### Check

- When a block is received inherents are checked.

- For example BABE `verify` method:
    - invokes the local node `create_inherent_data_providers` to get the local
      node values.
    - invokes the its `check_inherents` method
        - this calls the runtime by passing the local inherent data and the
          block. In this way the runtime can, for example, compare the values.
