Runtime API Call
================

Execution path from native client to runtime api.

- `Client` implements the following traits related to runtime access:
    - `ProvideRuntimeApi`: provides a runtime api;
    - `CallApiAt`: can call into the api at a given block;
    - `BlockBuilderProvider`: see `block-builder.md`

    trait ProvideRuntimeApi {
        // Concrete type providing the API
        type Api: ApiExt;

        // Returns the runtime api.
        fn runtime_api<'a>(&'a self) -> ApiRef<'a, Self::Api>;
    }

- `Client` has a generic type `RA`.
- `ProvideRuntimeApi` implementation for `Client` requires that `RA` is subtype
  of `ConstructRuntimeApi`

    trait ConstructRuntimeApi {
       type RuntimeApi: ApiExt;
       fn construct_runtime_api<'a>(call: &'a C) -> ApiRef<'a, Self::RuntimeApi>;
    }

- `Client` implementation of `ProvideRuntimeApi` is fully provided by `RA`
  implementation of `ConstructRuntimeApi`.

- `Client` used for tests have the `RA` parameter set to
  `substrate_test_runtime::RuntimeApi` (provided by `impl_runtime_apis!`).

- `impl_runtime_apis!` creates the following types:
    - `RuntimeApi`: implements `ConstructRuntimeApi`
    - `RuntimeApiImpl` implements `ApiExt`


- The executor used by `TestClientBuilder` in `build_with_native_executor` is
  set to `WasmExecutionMethod::Interpreted` (i.e. Wasmi).

Execution Path Example: Block Creation
--------------------------------------

- `Client` impl for `BlockBuilderProvider::new_block_at` is called.
    - Calls `BlockBuilder::new(api: A, at: ...)`
        where `A: ProvideRuntimeApi`
              `A::Api: BlockBuilderApi + ApiExt`
    - As the `api` the `Client` instance is passed.

1. Get `ApiRef` instance from the `Client` impl of `ProvideRuntimeApi`

    `api = api.runtime_api()`

2. Call the api

    `api.initialize_block_with_context(at: BlockId, context: ExecutionContext,
                                       header: Header)`  // <- only this change depending on the api call

    `ExecutionContext` can be `Importing|Syncing|BlockConstruction|OffchainCall`

3. The `decl_runtime_api!` generated the `initialize_block_with_context`

4. It calls the function in `impl_runtime_apis!` (see expanded runtime):

    Core_initialize_block_runtime_api_impl(
        &self: RuntimeApiImpl,
        at, context,
        params: Option<(Header),  // <- this varies depending on the api call
        params_encoded: Vec<u8>
    ) {
        self.call_api_at(|call_runtime_at, changes, storage_tx_cache_recorder|
        {...}
    }

5. `call_api_at(&self: RuntimeApiImpl, CLOSURE)`

    A trampoline capable of rollback/commit the actions of the CLOSURE.

    Calls CLOSURE(self.call, ....)
                        ^ This implements `CallApiAt`

6. The "closure" calls
   `api_api::...::initialize_block_call_api_at(call_runtime_at)`

   A function defined by `decl_runtime_api!`.

7. Calls `CallApiAt::call_api_at` implementation of `Client`
   (client/service/client.rs)

   The method has parameter `CallApiParams` grouping together all previous params.

   The `ExecutionManager` is created using
   `self.execution_extensions.manager_and_extensions)`
   The result depends on the `context` and the registered strategies in the
   `Client::execution_extensions`.

8. Calls `self.executor.contextual_call(at, method, ...)`
                ^ LocalCallExecutor

9. `CallExecutor::contextual_call` implementation for `LocalCallExecutor`

    - the state is recovered from db backend for the required block (at);
    - runtime code is fetched from the state.
    - check for runtime overrides (`self.check_override(..)`)
    - `StateMachine` is constructed

    state_machine = new(state, overlay, self.executor, method, call_data,
                        runtime_code, ...)     ^ NativeElseWasmExecutor

        The state machine constructor register extensions to:
            - read version of runtime
            - execute (spawn) an async task

10. `StateMachine::execute_using_consensus_failure_handler(
        &self,
        exec_manager: ExecutionManager,
        native_call: Option<NC>)`

    Depending on the `ExecutionManager` the runtime may be executed as wasm or
    native.

11. `StateMachine::execute_aux` will perform the call.

    calls the `self.exec.call(self.runtime_core, self.method, use_native, ...)`
                      ^ NativeElseWasmExecutor

12. `CodeExecutor::call` implementation for `NativeElseWasmExecutor` is called.

    Here a CLOSURE is used to perform the call.
    - if native can be used then the `native_call` closure parameter is used.
    - else we execute wasm using the module `instance`

    In both cases the call is performed wrapped by `with_externalities_safe`

    This sets the Externalities (`Ext`) using `environmental` crate.
    Ext contains the `Extensions` we've registered at the beginning.

13. In case of native call the closure within the `native_call` `Option` will
    jump to the one passed in `contextual_call` to the
    `state_machine.execute_using_consensus_failure_handler`.

    In practice jumps into the `impl_runtime_apis!` implementation of the api.
