Block Builder
=============

- Crate: `sc-block-builder`

Used as an abstraction over the runtime api to initialize a block, push
extrinsics and finalize a block.

- Provides the `BlockBuilder` utility structure.
- Exposes the corresponding runtime api (`sp-block-builder::BlockBuilder` defs
  within `decl_runtime_apis!`)

- A `BlockBuilder` instance is obtained via the `BlockBuilderProvider::new_block_at` trait
  implementation. (e.g. `Client` implementation @ client/service/src/client/client.rs)
- `BlockBuilderProvider` trait is defined in this crate.

### Constructor

- `new()` constructor parameters:
    - Implementation of `ProvideRuntimeApi` trait and such that `ProvideRuntimeApi::Api` implements
      `BlockBuilderApi + ApiExt` traits.
      (everything satisfied by the `Client` implementation of `ProvideRuntimeApi`)
    - Parent number and hash
    - Inherents digests (`Digest`)
    - Backend
    - Proof recorder

- An `ApiRef` is created using `ProvideRuntimeApi::runtime_api()` method.
    - This is a newtype wrapping `ProvideRuntimeApi::Api`
    - As said `ProvideRuntimeApi` is implemented by `Client`
        - calls `RA::construct_runtime_api(self)`
            - RA is one of the `Client` generic parameters
                - should implement `ConstructRuntimeApi`.
            - This is implemented for `RuntimeApi` created via
              `impl_runtime_api!` in the runtime code (see expanded runtime).
