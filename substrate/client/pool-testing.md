SC-TRANSACTION-POOL
-------------------

### BasicPool

- maintined_pool() -> (BasicPool<TestApi>, Arc<TestApi>)
    - an Arc of TestApi is also within `BasicPool` (self.api)

- BasicPool is the pool used. Instanced in two ways:
    - `BasicPool::new_test(pool_api: Arc<PoolApi>)`. Used for tests.
    - `BasicPool::with_revalidation_type`. Used by the `FullPool`.

- Contains an `Arc<Pool>`

### Pool

- Extrinsics pool that performs validation
- Contains an `Arc<ValidatedPool<B: ChainApi>>`

### ValidatedPool

- Pool that deals with validated transactions.
- Contains a `BasePool`, `import_notification_sinks` (to notify about imported
  extrinsic hashes).

### BasePool

- Builds a dependency graph for all transactions in the pool and returns the
  ones that are currently ready to be executed.

TEST-UTILS
----------

Projectwide test utilities.

### Runtime (substrate-test-runtime)

- Definitions for `AccountId, AccountSignature  BlockNumber, Digest, Block,
  Header, Extrinsic, etc` generally used by the tests.

- transaction-pool sub module
    - `uxt(who, nonce) -> Extrinsic`
      Creates a signed `Extrinsic::Transfer`

### TestApi

- implements `sc_transaction_pool::ChainApi` to perform common pool operations
  fom chain.
- has a `ChainState`, a (test-only) storage for chain related stuff (nonces,
  blocks)

SC-TRANSACTION-POOL TESTS
-------------------------

-
