Storage
=======

(Notes from Sub0.1 - Shawn Tabrizi - Deep Dive in Substrate Storage)

Access to storage is divided in four separate layers:

Runtime Storage API:
- sp-io can write directly to storage with a given key and value
- higher lever API may be generated via `[pallet::storage]` macro.
  This allows to manipulate storage using high level types such as
  `StorageValue`, `StorageMap`, ...

Overlay Change Set:
- Read/write cache over some underlying storage.
- Two kind of changes:
    - Prospective changes: what may happen. Not yet committed to the db.
    - Committed changes: what will happen. Changes committed at the end of the block.

Patricia Merkle Trie
- Data structure on top of the key value database.
- Arbitrary key and value length.

Key-Value database
- Implemented with Rocks-dB or Parity-db.
- Hash to arbitrary byte array.
- In Substrate we're using Blake2-256 has hash function.

Patricia Merkle Trie
--------------------

Substrate uses a Base-16 Patricia Merkle Trie.

Merkle tree allows:
- to have a synthetic fingerprint of a huge data set
- generate a proof of existance for some data in the tree leafs.

Patricia Trie
- Space optimized for elemens which share a prefix
- Position in the tree defines the associated key
- Elements with common prefixes share nodes

A branch can have more than two children, in substrate we're using base-16 tree.
A single hex character is called a "nibble".

The trie is entirelly constructed as an abstraction over the simple KVDB.

The hash of the node is what exists in the key/value database.


Two Kind of Keys:
- Trie key path: is set by the user (e.g. ":CODE"). Has arbitrary length.
  Part of this key is encoded in the Trie-node in the "key" field to construct
  the trie.
- KVDB key hash: the tree node hash, and this is the value in the actual
  database.

Child Trie
----------

Within the normal state DB tree, we cann have a leaf node that as a value has a
hash that leads to beginning of a brand new tree in the database.

The data in the child trie is isolated. The referenced tree can be a tree that
is not implemented using the same implementation as the state-db. Changes to the
child tree will alter the state-db root anyway.

Prefix Trie
-----------

Similar to Child trie, but cannot get the root hash.

A some kind of child trie, a group of elements sharing the same prefix.

Probably something temporary.

Runtime Storage Trie Path
-------------------------

All modules use a prefix trie now (long term they probably become a child trie).

When some data is declared for a module, all the data will be stored in the
storage with the same prefix. E.g.

- StorageValue

    twox128(module) + twox128(storagename)

- Map

    twox128(module) + twox128(storagename) + hasher(key)

The data for the module will be part of the same child tree.


Pruning
-------

Given a state tree at a given block, if we import a new block that alters some
nodes of the tree, we preserve the nodes that were not touched.

Eventually we will prune data that is not referenced anymore.

We can also pin data.

Complexity
----------

Reads: O(log n) for tree traversal, in a normal database you can get O(1) reads.

Write: O(log n) for tree traversal, write the value, compute and write the nodes
new hashes up to the root, i.e. (log n) times.

Even though is not efficient, it is great to provide proofs of existence to
light clients.

Best Practices
--------------

Minimize runtime storage. Store only consensus critical data in runtime storage.
For example we can store blobs on IPFS and store the hash of the blob in the
blockchain.

Struct vs multiple values. A struct is more big so costs more to be loaded, but
separate values will increase the number of nodes. A struct, if modified,
requires storage migration. A single value is easier to modify or to add.

Hashing Algorithms
------------------

Blake2. It is secure but slow. To be used only when user can influence the input
of the hash.

XXHash (twox). Non cryptographically secure, but is faster. To be used only
when the user can't control the value. The user can create unbalanced trie.
