Grandpa Consensus Protocol
==========================

Definitions
-----------

- Commit GHOST: the block with highest number for which the cumulative votes of
  descendents and itself reach the finalization threshold.
- Round Base: the block that was previously finalized.
- Commit: set of votes with the proofs
- Justification: an extension of commit, i.e. Commit and block headers
  ancestries to be able to validate the proof (down to the canonical chain).
- Equivocation: a vote for two different things on the same round.

Algorithm Insights
------------------

Phases:
- propose: optional and done by 1 validator;
- prevote: estimate of what can be finalized;
- precommit: vote on what can be effectively be finalized (using the estimate)

Round

    Let T = 1s
        2s              2s                  2s          compleatable
    |-----------|------------------|-----------------|---------------|
                ^
                prevote             ^precommit                      ^ concluded
                                                                      (commit)

Prevote is casted after 2s or if round r is completable.

A round is compleatable if the remaining voters cannot influence the finality of
the voted block anymore.

When round r-1 is "compleatable" it can still influence the round r. The round
r-1 still runs in background to allow collecting votes from missing voters
because it can still influence the outcome of current round.

The background round r-1 cannot terminate until we don't finalize the
estimation.

The round r-1 is concluded as soon as nobody can influence it.
(??? OR IF ROUND R IS COMPLETABLE ???)

Once the protocol goes to concluded state it sends "commit".

We can't immediately terminate the prev round because the estimate can be pushed
backward.

The final "commit" message is not strictly necessary for the protocol itself. Is
to inform the others (not participating in the protocol, e.g. light clients)
about the result. The commit is basically the result and the proof (signed
votes set). The proof is also known as justification.

This is sent by the background round once it completes or if the random timer is
triggered. So the commit can be sent by more than one participant.
(??? WHY OR, shouldn't be AND ???

If we see one commit, then we don't commit.

Voting round ends when it is **completable** and the background round ends when
is **concluded**.

### Code structure

Grandpa code is mainly divided in 4 crates:
- substrate/client `sc-finality-grandpa`: client low level component;
- substrate/frame `pallet-grandpa`: runtime component;
- substrate/primitive `sp-finality-grandpa`: stuff used by both runtime and
  client;
- finality-grandpa: stand alone crate with protocol implementation.

### Startup

Grandpa can be disabled via `--no-grandpa` command line argument.

The client binary should call `sc_finality_grandpa::run_grandpa_voter`.

### Session Rotation

A Grandpa epoch is set to a session epoch.

Core Protocol Crate
-------------------

(`finality_grandpa`)

### Messages

`Message` is a protocol message or vote that are round-localized. Variants:
- `Prevote`
- `Precommit`
- `PrimaryPropose`

`SignedMessage` contains a `Message`, signature and id of signer.

`CompactCommit` commit message with compact representation of auth data.

`CatchUp` message with aggregate of prevotes and precommits necessary to
complete a round. E.g. node is offline, go back online. My last "seen" round was
N and I realize that now round is M>N. So I send this `CatchUp` message to some
node. This returns information for round M-1, so I can conclude round M-1 and
start round M.

### Voter

Incoming message sources:
- network messages (global or round-localized)
- timer fired;
- previous round update.

`CommunicationIn`: incoming message for not round-localized communication
- `Commit` sent at the end of a round, to agregate the votes;
- `Catchup` allows to go to the latest round.

`CommunicationOut`: outgoing message for not round-localized communication.
- Commit sent at the end of a round

Non round-localized messages are sent on a global topic, identified by SetId.

Users of the `finality-grandpa` crate needs to implement the `Environment` trait.

    trait Environment {
        type Timer;
        type BestChain;     // to compute the best chain to vote on
        type In: Stream<Item = SignedMessage>, // for input messages
        type Out: Sink<Message>,               // for output messages
    }

`RoundData`: data necessary to participate in a round. Returned by the
`round_data()` implementation of `Environment` trait. Contains timers futures
and i/o streams.

`Environment` contains a method to get the best chain containing a block
(`best_chain_containing`). Also has hooks to inform about events like
`prevoted`, `precommitted`, `proposed`, `finalize_block`, equivocations, ...

On this crate everything is in memory. Substrate `Environment` hooks persists
information "somehow".

Primitives Crate
----------------

Defines the crypto key type used by Grandpa (`app_crypto!(ed25519, Grandpa)`).

`ConsensusLog` variants `ScheduledChange`, `ForcedChange`, `OnDisabled`, `Pause`, `Resume`

`ScheduledChange` announces authority changes.
- similar to `NextEpochDescriptor` of babe;
- contains next authorities list and delay (in blocks).

Example (for standard changes):
- At block N the pallet emits a digest saying that the authorities will change;
- If delay is x, then when we finalize block N+x we should enact the changes.

`EquivocationProof` contains
- `SetId`: monotonic identifier of a grandpa authority set
- `Equivocation`: variants `Prevote` and `Precommit`, both wrapping a
  `grandpa_proto::Equivocation`, which contain round, identity, first and second
  equivocation votes.

Misc utility functions such as
- `check_equivocation_proof`: checks equivocation validity;
- `check_message_signature`: checks a `finality-grandpa::Message` signature;

Client Crate
------------

Main responsibilities:
- Provides networking communication to the `finality-grandpa` crate.
- Logic to enact the authorities changes.

Folder `communication`: stuff related to networking (gossiping).

### Structures

`VoterWork`: future for the voter.
Contains a reference to `Environment` and an instance to
`finality_grandpa::Voter`.

    `run_grandpa_voter -> VoterWork::new()`

A `finality_grandpa` voter instance is constructed by `rebuild_voter()`
Called on VoterWork construction, new session and pause.

`Environment`: implementation of `finality_grandpa::Environment`.

`LinkHalf`: link between block importer and background voter. Contains `client`,
`select_chain`, `justification_sender`, `justification_stream`, ...
Instanced together with `GrandpaBlockImport` via
`sp_finality_grandpa::block_import()`.

`NetworkBridge`: bridge between underlying network service, gossiping consensus
messages and grandpa.
Contains: `gossip_engine`, gossip `validator`, `neighbor_sender`.
Methods: `round_communication`, `global_communication`.
- `round_communication()` setup channels used for round communication.
    - Calls the inner `GossipValidator::note_round` (new round started).
      Internally this updates the timers to the do the gossip calculations
      about 1st or 2nd stage... and it also sends neighbor messages.
    - gossip topic based on set-id and round number.
- `global_communication()` setup channels for global communication.
    - gossip topic based on set-id.

### Until imported module (until_imported)

`BlockUntilImpoted`: something that needs to be withheld until specific blocks
are available. For example a commit message is not used until the corresponding
block is not available.

### Authorities Module (authorities)

    Babe =>    |---------------------------|-----------------------|----------------
    Gran =>            *       *   *               *

The block announcing the Babe's next epoch changes also contains a digest for
Grandpa containing the authorities set and the delay.

The authorities are not enacted immedietely

The "best block" rule is "overwritten" on the Substrate side to return the next
unfinalized block containing an EpochChange. Thus blocks containing epoch
changes are always explicitly finalized.

This means that Babe and Grandpa validators are independent. Babe can go on with
its validator set, but grandpa may require old epochs validators (to explicitly
finalize blocks containing old epoch changes).

`AuthoritySet` struct keeps track of the authorities state:
- contains current authorities list, set-id, pending-standard-changes that
  were already been signaled (e.g. block import) (ForkTree).
- On block finalization the code looks to the pending standard changes tree.
  E.g. block N has been finalized, are there any changes on fork-tree for block
  N? If yes, the corresponding entry is removed from the tree and `AuthoritySet`
  is updated with the changes.

"Standard changes" get applied whenever the block that signaled them gets
finalized. A digest for this is emitted by the runtime every time the session
changes

There could be forks, and when we finalize an epoch change block the proper
validators are set depending on the fork we are finalizing (that is why we
require a ForkTree).

"Forced changes" get applied whenever the block that signaled them gets
imported.
- These are used to overcome to the case where the validators that should
  validate a given block (to normally change the validators set) goes
  offline forever. Otherwise there is no way to perform this change.
- This breaks the security of the whole thing.
- Can only be sent out via `Sudo` or via `Governance` invoking the
  `note_stalled` callable of `grandpa` pallet.

In principle the Grandpa and Babe validators sets can be disjointed. In practice
in Polkadot are the same.

### Network saturation

To prevent network saturation we start sending messages to a small subset of
connected peers (`first_stage_peers`), if the round doesn't conclude within a
given time then we go with `second_stage_peers`

We check if we are allowed to send a message to a `PeerId` using the
`round_message_allowed` method. Here we check if the peer is part of the peers
set of the stage we are in.

`ROUND_DURATION` set to 5 (measured in periods of gossip duration)

We wait of 1.5 round duration times propagation (`PROPAGATION_SOME`) to use the
first stage peers set. And 3 round duration times propagation (`PROPAGATION_ALL`)
to use the second stage peers set.

The max entries in these HashSet is `LUCKY_PEERS` (4).

Other timers (stored in `voter::RoundData`):
- 2 gossip durations for prevotes
- 2 gossip duraitons for precommits
- 1 gossip duration for precommits to spread

Round commit timer (`round_commit_timer`) set to a random value between 0 and 2
times the gossip duration.

In Polkadot the `gossip_duration` value is set to 1 second (withing
grandpa::Config).


### Communication

Everything related to networking/communication.

- `Round`: wrapper struct around `sp_grandpa::Round` number.
- `SetId`: wrapper struct around `sp_grandpa::SetId` number.

We have two different gossip streams:
- 1 global: that is "per set" (identified by SetId). Everyone in the same
  set id subscribes to this topic.
- per round: used for messages specific for a given round to get prevotes
  and precommits.

`GossipMessage` is the actual message that is transmitted. Variants:
- `Vote` wraps round, set-id and `finality-grandpa::SignedMessage` (wrapping
  Prevote, Precommit, PrimaryPropose)
- `Commit` wraps `finality-grandpa::CompactCommit`
- `Neighbor` wraps NeighborPacket.
- `CatchUpRequest` wraps round and set-id.
- `CatchUp` wraps set-id and `finality-grandpa::CatchUp`.

There are a bunch of rules for "polite" behavior to limit the amount of gossip
messages:
- You only send messages for round r-1, r, r+1
- If we receive an impolite message, we reduce peer reputation. If we receive
  polite messages, reputation goes up.
- Actions weights on reputation in
  `sc-finality-grandpa::communication::{cost, benefit}`
- Handled in networking layer by `ReputationChange` struct, if
  reputation becomes too much low node disconnects unpolite peer.

`Neighbor` message sent in unicast to peers to share node's state.
`CatchUpRequest` message sent depending on the `Neighbor` messages.

`GossipValidator` is a validator for gossip messages, wraps:
- `Inner` instance;
- `PeerReport`s sink (e.g. to network layer);
- `SharedVoterState`, wraps
    - `VoterSetState`: `Live` or `Paused` variants;
    - Map from `RoundNumber` to `AuthorityId`

`Inner` wraps:
- `LocalView` of protocol state: has round, set-id, last-commit, round-start.
   Similar to `View` but with additional round and set-id when the last-commit
   has been observed (block height) and the instant at which the current round
   started.
- `Peers` updated based on the neighbor messages we receive.
- Live topics: `KeepTopics`
- `Config`
- `PendingCatchUp``
- `CatchUpConfig`

`Peers` is a locally stored structure containing:
- map of connected peers (`PeerInfo` values);
- first stage peers, second stage peers, lucky light peers(?!?)
- `reshuffle` method to randomly select peers into the three sets on every round

`PeerInfo` wraps observed peers information:
- `ObservedRole`: `Full`, `Light`, `Authority`.
- `View` of protocol: round, set-id, last-commit (commit-finalized block height)
- We use this data struct to decide to send a message to a peer or not.
  (`consider_vote` and `consider_global` methods)


### Environment implementation

- `environment` module.
- Implementation of `finality-grandpa::Environment` for `voter::Environment`.
- I/O streams for a particular round are created via
  `communication::NetworkBridge::round_communication`.


Pallet Crate
------------

- Grandpa pallet to handle events related to grandpa

- Authorities are stored in the runtime `storage` module
  (`frame/support/src/storage`). So not using the classic frame `storage` macro.

### `Config` trait

- `HandleEquivocation`: equivocation handler type.
- `MaxAuthorities`: `Get<u32>`
- `KeyOwnerProof`: proof of key ownership type. Used for equivocation
  reports validation.
- `KeyOwnerIdentification`: for reporting equivocations.
- `KeyOwnerProofSystem`: system for proving ownership of keys (i.e. a key is
  part of the validator set). Used for validating equivocation reports.

### `Hooks` implementation

- `on_finalize`:
    - to enact pending authorities changes when it is the time.
      (thus this is checked after each block execution)
    - to change state (e.g. PendingPaused => Paused)

### `OnSessionHandler` implementation

- `on_new_session`
    - (called from `Session::on_initialize`
        - if `ShouldEndSession::should_end_session(n)` (provided by Babe)
            -> `Session::rotate_session`
    - Changes the authority set
    - If authority set is changed then `schedule_change` is called and
      `PendingChange` is written (contains `delay`, `scheduled_at`,
      `next_authorities`.
    - If `PendingChange` is Some, then the `on_finalize` Hook adds the
      `ConsensusLog::ScheduledChange` in the block header (if this block number
      is equal to the `scheduled_at` block).
        - If `scheduled_at + delay` is equal to the block number the authority
          set changes are enacted.

### Calls

- `report_equivocation`:
    - report a voter equivocation/misbehavior, this verifies the equivocation.
    - calls `do_report_equivocaton`.
    - Checks proof, session, and eventually report offence.
        - This is eventually used by the staking module (OffenceHandler) to
          perform the slash. Is configurable. Similar in Babe.

Open Questions
--------------

- Why the authorities set is not using the "classic" FRAME `[pallet::storage]`?
    - Is this an optimization to allow the node to easily fetch the data without
      calling into the runtime?
      Looks like this `GRANDPA_AUTHORITIES_KEY` is not used anywhere directly but
      the call goes through the runtime anyway.

- What is the state of the grandpa code refactory branch?

- In the video you say that we don't report equivocations in the background
  round. It was a bug in the current version? Meaning that in the current
  version we don't report equivocations in the BG round?

- Background rounds in `FuturesUnordered` list.
  What is the exact condition to actually terminate a background round?
  I know that once the round N starts then it can only be influenced by N-1 (if
  compleatable).
  But N-2 might still miss some votes that may generate some equivocations to
  report.
