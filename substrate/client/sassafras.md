Sassafras Protocol
==================

This article contains some notes about the Sassafras consensus engine.

Developed by Jeff Burdges, Alistair Stewart, Sergey Vasilyev and others, it aims
to fix some short-comings of Babe and thus making the Polkadot network more
secure.

Keywords
--------

* ZKP: Zero Knowledge Proof.
* Witness: the information to prove knowledge of in a ZKP.
* zk-SNARK: kind of non interactive zero knowledge proof.
* VRF: Verifiable Random Function.
* RVRF: Ring-VRF.
* RVRF-CRS: Common Reference String (aka secret λ parameter used for SNARK).

Introduction
------------

Sassafras is a constant-time block production protocol that aims to ensure that
there is exactly one block produced with constant time intervals rather than
multiple or none.

We run a lottery to distribute block production slots in an epoch and to fix the
order validators produce blocks by the beginning of an epoch.

Each validator signs the same VRF input and publish the output on-chain. This
value is their lottery ticket that can be validated against their public key.

We want to keep lottery winners secret, that is at the begin of the epoch all
the validators tickets are published on-chain and validated without knowing
exactly what is the public key.

A valid ticket is finally claimed by a validator at the time of block
production.

To prevent submission of invalid tickets, resulting in empty slots, when a
ticket is submitted is validated in a privacy-preserving way. In practice, the
ticket is accompaned with a SNARK of the statement: "Here's my ticket that has
been generated using the given VRF input and my secret key. I'm not telling you
my keys, but my public key is among those of the nominated validator".

To anonymously publish the ticket to the chain a validator sends their tickets
to a random validator who later writes it on-chain via a transaction.

Protocol
--------

In the paper, the protocol description starts from the nomination of a new set
of validators, bootstrapping is left unspecified.

As for Babe, epoch e(m) randomness r(m) is produced as:

    r(m) = H(σ, r(m-1), m)

In epoch e(m) we use r(m) as RVRF input to produce a number of outputs and
publish them on-chain as tickets.

After tickets are on-chain we sort them and their order defines the order for
block production for epoch E(m+2)

    epoch     e(m)            e(m+1)          e(m+2)
      ---|---------------|---------------|---------------|------->
          ^
          produce tickets for e(m+2) using r(m)

### Parameters

* V : set of validators for a given epoch.
* s : number of slots per epoch.
* x : redundancy factor. For an epoch of s slots we want to have s∙x tickets
      (in practice we set x = 2).
* a : attempts number, i.e. number of tickets generated per validator in epoch.
* L : a bound on a number of tickets that can be gossipped (for DoS resistance).

### Keys

In addition to regular keys, for each validator a new keypair on a SNARK
friendly curve [Jubjub](https://z.cash/technology/jubjub) is introduced.

Given λ a security parameter and r some randomness

    KeyGen-RVRF: (λ, r) -> (sk, pk),

Q: is λ related to SNARK security setup?

Validator keypair should be generated before the randomness for the epoch they
are used in is determined (similar to Babe).

As an optimization we introduce an aggregate public key (apk) for the whole set
of validators (also called a *commitment* in Jeff's paper). Basically a Merkle
tree root built on a list of a public keys.

In conjunction we use the opening proof (Merkle copath) ask(v), i.e. aggregated
secret key for validator v, to identify a public key in the tree as a private
input to a SNARK.

    Aggregate-RVRF: (v, {pk(v)}) -> (apk, ask(v)) , with v in V

### Setup

When a new set of validators V gets nominated or some parameter changes, the
protocol is reinitialized with new values.

The public keys of the validators should be already beeing registered.

Each validator v in V:
1. Computes T = (x∙s)/(a∙|V|).
2. Computes the apk and ask(v).
3. Obtains the SNARK CRS and checks for subversion if it has changed or v hasn't
   done it earlier.

Note: the parameters are chosen such that T < 1.

### Tickets Generation

We aim of having at least 's' VRF outputs (tickets) published on-chain.

At epoch e(m), we use the randomness r(m) (as Babe)

    r(m) = H(m, r(m-1), ⍴)

With ⍴ the VRF output of every block collected during epoch e(m-2) (as Babe).

We use r(m) to create inputs for the RVRF in order to produce the tickets to be
consumend in epoch e(m+2).

Follows that we run regular VRF and RVRF in parallel. This is because RVRF
outputs will be revealed in epoch e(m) and hence if we use RVRF outputs for
randomness r(m+1) whould be revealed too early. Thus we use VRFs that are
unrevealed until the corresponding blocks are produced.

Each validator v in V:

1. Given r(m), computes RVRF outputs for the inputs in(m,i) for i = 1,..,a

       Compute-RVRF(ask(v), in(m,i)) -> out(v,m,i)

Q: here the w3f doc reports sk(v) instead of ask(v).

2. Selects the outputs that are below the threshold T:

       Bake(out(v,m,i)) < T, with 'Bake': RVRF-output -> [0,1]

   IW is the set of indices for the outputs below the threshold.

3. Uses its ask(v) to generate proofs for selected inputs i in IW

       Prove-RVRF(sk(v), spk(v), in(m,i)) -> π(v,m,i)

   With π(v,m,i) consisting of the SNARK and its public inputs apk, i.

Q: maybe cpk here is apk (or rpk)? Looks like the symbols are not used
consistently in the paper.

As the result of this phase every validator obtains a number of, possibly 0,
winning tickets together with proofs of their validity (i, out(v,m,i), π(v,m,i))
that needs to be published on-chain.

The randomness of IW's size prevents the adversary to predict how many more
blocks a validator is going to produce (min=0, max=a).

### Publishing Phase

On epoch e(m+1) the tickets in IW are published on-chain.

Q: the tickets to the proxy can be sent in e(m)? I expect that in e(m+1) the
proxy starts publishing on-chain the tickets gathered during e(m).

Validators don't publish winning tickets themselfs but relay them to another
randomly selected proxy validator who publishes it.

The proxy index v' is chosen based on the value of out(v,m,i):

    v' = out(v,m,i) mod |V|

The validator v sends the message:

    [v, l, E(v', out(v,m,i), π(v,m,i)]

With E an encryption function using v' public key.

The winning outputs are numbered using l ranging from 0 to L-1 and gossip them.
If we have more winning outputs than L, it gossips only the lowest L.

When a proxy receives a message it checks if in this epoch it already received a
message for the same v and l. If so, the message is discarded.

Otherwise it gossips the message and decrypts it to find out if he is the
inteded proxy.

Q: forwards it if he is not the intended recipient? He is the only one able to
decrypt it.

A proxy submits a transaction including out(v,m,i), π(v,m,i) at some fixed block
number for inclusion on the blockchain.

If a validator ticket is not included after a given block number, then it can
publish it directly.

We can economically incentivize validators to submit tickets in the name of the
original validator. Drawback, validators may submit it directly for profit.

The protocol is not completelly anonymous. Even though RVRF proof doesn't reveal
anything, the proxy knows the ticket owner identity.

### Verification

Before tickets are persisted on-chain, they need to be verified.

To verify a published transaction (out(v,m,i), π(v,m,i)) we need to verity the
SNARK. For this we need:

* the corresponding input in(m,i), which can be calculated from i and r(m)
* the published output out(v,m,i)
* the aggregated public key apk (ring public key)

    Verify(rpk, in(m,i), out(v,m,i), π(v,m,i)) -> bool

### Sorting

In epoch m+2 we have the list {out(m,k)} with k = 1,..,K of verified tickets
generated during epoch m which are finalized on-chain.

For each of these outputs, we combine the output with the randomness r'=r(m+1)
and we compute out'(m,k) = H(out(m,k) || r')

The validator then sorts the list of out'(m,k) in ascending order and, if K > s
drops the largest K-s values.

Now given out'(m,k) ordered we assign them to the slots *outside-in* starting
from the end.

    out'(m,2), out'(m,4), ... ,  out'(m,3), out'(m,1)

If K < s we fallback to AURA in the middle of the epoch.

#================================================================================
#RESUME REVIEW FROM Here
#================================================================================

### Claiming the slots

To produce a block in the assigned slot, the validator needs to include the
ticket, a VRF out(v,m,i) and the non-anonymous proof that this is the output of
their VRF.

Q: Isn't the VRF out(v,m,i) the ticket itself?

```
Reveal_RVRF: sv_v,out -> tau
Check_RVRFL tau, out -> true/false
```

These are basically Schnorr knowledge of exponent proofs (PoKe).

When validating the block nodes verify this proof.

The validator must also include a VRF output, called the Babe VRF above. This
can be done using the same technique as Babe.

zk-SNARK
--------

Used as a primitive for RVRF implementation.

zk-SNARK (Zero Knowledge Succing Non-Interactive Argument of Knowledge) is a
non-interactive zero knowledge proof, that is the Prover doesn't require any
interaction with a Provider in order to proove his knowledge.

Succint because the proof have small size (\~order of 100 bytes).

zk-SNARK proofs depends on an initial configuration requiring a set of public parameters.

Composed of three algorithm: G, P and V.

### Key Generator G

Requires a secret parameter lambda and a program C, generates two keys:

* proving key (pk)
* verification key (vk) These two keys should be generated once for each program C.

### Prover P

Requires the proving key (pk), a public number (x) and a witness (w) in order to generate a proof:

```
proof = P(pk, x, w)
```

### Verifier

Requires the proof (prf) and the public number (x) to validate with the verification key (vk) that the proof is correct, retirning true or false

```
valid = V(vk, x, proof)
```

### Lambda parameter

The security of zk-SNARK is reduced to the way that lambda has been generated. Who posses this value can generate valid proofs.

The secret lambda, aka Common Reference String (CRS), requires a very a high secure process. Once generated, any information about his generation (toxic waste) should be eliminated. A Secure Multipary Computation protocol is ideal.

The generation of public parameters and the keys is called "Setup Cerymony"

Implementation Notes
--------------------

Two VRF are generated at each block:

* Ticket RVRF: which is used for proof of block production rights;
* Block VRF: used for randomess collection.

For ticket RVRF three epochs are tracked:

* At epoch N validators generate ticket RVRFs, anch select the ones below a threshold;
* At epoch N+1 validators send RVRF Proof to the proxy;
* At epoch N+2 proofs included are sorted in outside-in order, and each proposer is required to show they are the person who generated the proof.

Three digests are included in the header:

* `PreDigest` (`PreRuntime` type): contains the proof of the ticket RVRF and block VRF.
* `PostBlockDescriptor` (`Consensus` type): tracks ticket RVRF commitments to be included in the current block (generated by runtime).
* `NextEpochDescriptor` (`Consensus` type): descriptor for the next epoch, same as Babe. Note that the exacted validators only start to validate 2 epochs later.

### Verifier and BlockImport Logic

Pre-runtime digest verification:

1. Verify pre-runtime digest
2. Verify that the slot is increasing, and not in the future
3. CHeck signature
4. Check that the ticket VRF is of a valid index in auxiliary.validating
5. Check taht the ticket VRF is valid
6. Check that the post-block VRF is valid

Post-block digest verification:

* Push any commitments of ticket RVRF

Next-epoch descriptor digest verification

1. Check descriptor validity
2. SOrt the validating proofs in "outside-in" order
3. Pus in pool auxiliary

TODOs

* Proposing and runtime helpers
* Refactor some Babe code in a separate crate for reuse (mostly VRF, threshold, and secondary pre-digest)
* Figure out a way to implement networking during publishin phase

### Sassafrass via Schnorrkel

(Notes from https://corepaper.org/substrate/consensus/sassafras)

A simplified version of Sassafras, directly using Schnorrkel, mostly used as an
intermediate process towards Sassafras via RVRF, and thus does not have the same
strong security properties as Sassafras. The overall process works as follows:

1. At epoch N, each validator generates Schnorrkel VRFs (VRFOutput, VRFProof),
   select those H1(VRFOutput) passing the threshold, and send them to
   H2(VRFOutput).
2. At epoch N+1, each validator publishes the VRFs they received, directly as
   transactions on-chain.
   * Runtime does not validate the VRF. Rather, it is validated by consensus
     engine at the end of the block. Invalid VRFs are simply ignored.
   * The reason for publishing as transactions is for future reward system
     extension, which will be handled by runtime, and for reuse of transaction
     pool.
3. At the end block of epoch N+1, consensus sorts the VRF gathered as this
   epoch, using "outside-in" order.
4. The block proposers at epoch N+2 will be VRFs produced above.

References
----------

* [implementation attempt](https://github.com/paritytech/substrate/pull/4600)
* [w3f introduction](https://research.web3.foundation/en/latest/polkadot/block-production/SASSAFRAS.html): "friendly" overview od the protocol building blocks.
* [hackmd](https://hackmd.io/inivuSmzR2uePJhnJtBWRQ): collaborative platform for the Sassafras overview document.
* [research paper](https://github.com/w3f/research/tree/master/docs/papers/sass)
* [ring-vrg Rust implementation](https://github.com/w3f/ring-vrf) by Jeff
* [ring-vrg paper](https://github.com/w3f/ring-vrf/papers/ring_vrf)
* [zcash zk-snarks](https://arxiv.org/pdf/2008.00881.pdf)
* [my notes](https://hackmd.io/BzZjlgITQX-_LASx0NCldQ)



Integration Notes
-----------------

- RVRF is implemented via zk-SNARKs. It is not a separate thing.
