Runtime API call after unmigrated storage
=========================================

Refer to issue: https://github.com/paritytech/substrate/issues/9997

- Runtime has a version.
- New pallet not compatible with old storage for that pallet.

### Migration

When runtime is upgraded you should call the `on_initialize` this can check if
we upgraded the runtime and call the on runtime upgrade.

This was performed always.

For example: for System pallet we set the block number, etc...

We moved this call BEFORE calling into the runtime (for perfomance) in the block
that follows.
But now this doesn't cal the `on_initialize`:
1. next extrinsic uses the new runtime
2. the storage has not been migrated
