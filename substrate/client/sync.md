Blockchain Sychronization
=========================

The system can be in four different states with respect to synchronization:
- initial sync
- warp sync
- state sync
- normal synced

Currently this is handled by a single subsystem with a bunch of "if else"
blocks.

New Architecture
----------------

#### Initial Sync

Not necessarily triggered at genesis, could also start if the node disconnects
from the network for a while.

Started if the node is behind the "best" seen block by constant (currently 5).
(If I have more that 5 blocks in my queue then we are in major sync mode).

If I'm in major sync mode, I need to find the common ancestor to start.

#### Warp Sync

Node gets finality proof and jump directly to a block.

We check the finality proofs (justifications) from genesis to that block.

Ask peers for justifications, download the block and verify.

After this, we switch to "state sync" mode.

### State Sync

Download the state grabbing chunks from different peers.

It can now check the state root with the block it downloaded before.

There is also a "Fast sync" mode:
- sync the state
- we download blocks and we don't execute them.

After it has done the warp sync it can download the blocks in the background.

#### Normal Synchronized

Node is synchronized, it can start authorship.

It also annouces blocks to Sync module (to check if initial sync should be
restarted).

If we go back to Initial sync then we kill Block authoriship task.
