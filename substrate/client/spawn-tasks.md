Spawn Tasks
===========

`LocalCallExecutor` implements `CallExecutor`

	LocalCallExecutor::new(
		backend: Arc<B>,
		executor: E,
		spawn_handle: Box<dyn SpawnNamed>,
		client_config: ClientConfig<Block>,
    )

With `E` implementing `CodeExecutor` trait.

Tasks are spawned via implementation of `SpawnNamed`.

## Testing

(crate: substrate-test-runtime-client)

`Client` instance is created `TestClientBuilder::build_with_native_executor`.

Here the `LocalCallExecutor` is created using a testing task executor:

    sp_core::testing::TaskExecutor

This is a wrapper over `futures::executor::ThreadPool` with a pool size of 8.

Its purpose is to spawn tasks for the test client.

## Node

Client is instanced via `sc_service::new_full_parts` which takes a generic
`TExec` implementing `CodeExecutor` trait.

`new_full_parts` calls `sc_service::new_client` with the `CodeExecutor`
implementation.

Here the `LocalCallExecutor` is created and used to instance a `Client`
instance.

The `spawn_handle` is passed by `new_full_parts` using
`TaskManager::spawn_handle` method.

`TaskManager` is a struct to manage background/async tasks in client Service.

Task manager uses tokio as backend.

Its `spawn_handle` method returns a `SpawnTaskHandle`, a structure implementing
`SpawnNamed`.

`new_full_parts` returns both the Client and the TaskManager.

The `TaskManager` will be further used during node initialization (in
`new_partial` and `new_full` of node code) to spawn other basic tasks:
telemetry, transaction-pool, import-queue, network, offchain-worker, rpc,
proposer (block authorship), authority-discovery, aura/babe, grandpa-voter
