BABE Protocol
=============

Blind Assignment for Blockchain Extension protocol (BABE) assigns block
production slots using roughly the randomness cycle from Ouroboros Praos.

Not every node has the same view of non-finalized chain. This is because in
one slot there can be multiple validators allowed to create a block

Equivocation: two different blocks produced by the same validator in the same
slot.

### Best Chain

Validator works on the best chain, by default the one with haviest weight.

Two block authoring mechanisms:
- primary: validator selected via a VRF (weight 1).
- secondary: validator selected via round robin mechanism (weight 0).

Epochs and Slots
----------------

- Timline divided in epochs
- Epoch divided in slots
- Slot numeric value is a function of local machine time
  (i.e. slot = unixtime / slot_duration)

When an epoch X begins, the parameters for epoch X+1 are constructed.
- Onchain data: VRF seed, validators set.
- Offchain data: next epoch start slot, next epoch duration.

Epoch 0 is special. When starts also announces its own parameters.

Epoch parameters are announced by the epoch first block author and are inserted
within the block header PreRuntime Digest.

       epoch              E0            E1            E2
                |---|-------------|-------------|------------|----->
        slot #  0   1             11            21           31
    announces      (E0,E1)        (E2)          (E3)

### Sessions and Epochs

The session pallet uses an external mean to detect session changes. In the
Polkadot implementation this external component is Babe and the begin of an
epoch signals the begin of a session.

Offchain Epoch Data
-------------------

Epochs changes information is maintained within the `EpochChanges` structure,
which internally uses the `ForkTree` data structure.

Blocks announcing epoch changes may belong to different blockchain forks


    epoch       E0            E1            E2
          |-------------|----+--------|------------|----->
                              \-------|------------|----->

Epoch-changes tree

          (E0,E1)------(E2)-----------(E3a)
                         \------------(E3b)--------(E4)

Note that E2 started on two different forks, thus E3 has been announced with two
different sets of parameters.

Verifiable Random Function
--------------------------

Within a given slot, block validators are selected via a VRF. Because it is
random, 0+ validators may author a block.

Only the block author knowns before the fact if he is a valid author for the
next block.

This property is given by the VRF taking as input the validator candidate
private key. The public key of each validator resides onchain to allow everyone
to verify if a validator block proposal is legitimate.

    VRF(private-key, seed) => (rand, proof)
    Verify(public-key, rand, proof) => [true, false]

The resulting rand determines who has the right to produce a block
(e.g. rand % n-validators)

Any node can check if the received block author was having the right to propose
the block in the given round. If not the block is dropped.

The VRF randomness seed for for epoch N (announced at the beginning of epoch
N-1) is computed from:
- target epoch number N
- random seed of epoch r-2
- randomness deposited by each block in epoch N-2

       epoch    N-3            N-2                 N-1           N
             ----------|-+---+---------+--|-+----------------|------->
                         ^   ^         ^    ^ epoch N seed
                         r1  r2   ...  rk     and validators

       N seed = H [N, N-2 seed, r1, ..., rk ]

The validators for epoch N (announced at begin of epoch N-1) should be
registered onchain at most at the end of epoch N-3. The reason is to make sure
that the VRF keys of the new validators are added to the chain before the
randomness of the epoch that they are going to be active is revealed.

The VRF input includes some public randomness that is unknown when the validator
registered its key (this is to prevent validators searching through the keys
until they find some that gives some advantage over block production).

### Keys

In Substrate BABE implementation, we use the same `sr25519` key for both signing
the messages (`Seal`) and VRF.

-
See [here](https://github.com/paritytech/substrate/blob/master/primitives/consensus/babe/src/lib.rs)
for details about the `app_crypto!` sage.
- claim primary slot usign VRF [here](https://github.com/paritytech/substrate/blob/6e5ee5155aaf8f9c25a1a2292f73a041ab501ed7/client/consensus/babe/src/authorship.rs#L226)

Block Proposal Methods
----------------------

A validator can produce a block depending on the output a one of these three
methods:
- Primary: VRF output lower than a configurable threshold;
- Secondary: pseudo random depending on epoch randomness;
- Secondary-VRF: slot claiming works like the normal secondary method, but it
  also contain the VRF output.

### Primary

For primary, the threshold has the following formula
(`calculate_primary_threshold):

c: probability of a slot of being full (0.25 for polkadot)
p: threshold for validator
theta: validator_weight / sum(all_validators_weights)

    p = 1 - (1-c)^theta_i

A validator has the right to author a block if VFR output is less than
the threshold p.

The bigger is theta the greater is p (i.e. a validator with bigger weight
has greater probability to author a block.

### Secondary

The formula is:

    author_index = blake2_256(randomness, slot) % num_authorities

# Secondary-VRF

The VRF output is verified but not used to collect epoch randomness in the
runtime.

The reason to include VRF is that is still usefull as a source of randomness
just for this block.

Polkadot has this enabled, but the demo node in Substrate not.

Babe Digests
------------

All these digests are added to the `Digest` log entry of the block header using
the `DigestItem` structure.

Babe adds to the block header the following digest types:
- `PreDigest`: primary or secondary data (e.g. authority-index, slot, vfr-output,
  vrf-proof) (as `DigestItem::PreRuntime`);
- `NextEpochDescriptor`: authorities and randomness for the next epoch (as
  `DigestItem::Consensus`)
- `NextEpochConfig`: algorithm configuration for the next epoch (e.g. the `c`
  value used for the threshold) (as `DigestItem::Consensus`);
- `AuthoritySignature`: signature of the header after all the data has been
  pushed to the header, excluded the seal itself (added as `DigestItem::Seal`).

The hash of the header without the seal is called the *pre-hash*.

The `Seal` is guaranteed to always be the last item in the `Digest` log.

To verify the signature we remove the `Seal` from the header, we compute the
pre-hash and we verify the signature.

Block Verification
------------------

Babe implements the `sc_consensus::import_queue::Verifier` trait.

This implementation is used by the import queue to verify a block before calling
the `BlockImport::import_block` pipeline.

This type of verification doesn't require the parent to be imported. The idea is
that this verification could be run in parallel (not implemented yet).

We get the slot from Babe's pre-digest and we use the `epoch_changes` tree
structure to get a viable epoch for the block.

The header is then verified (`verification::check_header()`). In particular we
check that the slot found in the pre-digest is greater than the slot found in
the current machine (using Babe's inherent data provider), if not we return a
`CheckedHeader::Deferred` to signal to the import queue that this block
verification should be repeated later.

The header check now proceeds depending on the pre-digest type. For primary, we
check the VFR proof and we verify that the block is signed by the expected
author.

If the header verification is successfull we check and report equivocations
(i.e. if this is a block for slot N, we check if we've already seen a block for
slot N from the same author). The equivocation is eventually reported to a
handler, e.g. runtime for slashing.

We validate the timestamp using the runtime. We use the runtime to check that
internally-set timestamp in the inherents actually matches the slot set in the
seal.

Finally we add the epoch descriptor for the block's viable epoch to the
`BlockImportParams::intermediates` field (with key `INTERMEDIATE_KEY`).

Block Import
------------

Babe implements the `sc_consensus::block_import::BlockImport` trait.

Here we are doing the verification:
- make sure that block slot claim is correct (primary/secondary);
- if the epoch is expected to change (because announced by the first block in
  current epoch) check that the block contains the `NextEpochDescriptor`.

The slot in the pre-digest is expected to be greater that the parent slot.

The viable epoch for the imported block is recovered from the
`BlockImportParams::intermediates` field (with key `INTERMEDIATE_KEY`). This
intermediate data is pushed by the import queue on `Verifier::verify()` call or
by the authoring algorithm. This is a completelly local way used to pass data
between internal components.

If the block is found to be the first block in the viable epoch then is expected
to contain a `NextEpochDescriptor` digest, i.e. we expect to contain epoch
changes data. In this case we also add the next epoch descriptor to the epoch
chanees tree.

Babe Pallet
-----------

Is the runtime that decides when a new epoch should start. Thus it is the
runtime that generates and push `DigestItems::Consensus` variations.

### On Initialize

On block initialization we extract and save the relevant information from block
`PreDigest` digest (such as the current slot).

The digest is then saved within the `Initialized` storage item for further
processing in the finalization handler.

Note that on block execution the VRF block randomness refers to the one of the
previous block, that is the randomness found in the `PreDigest` header is not
yet used.

### On Finalization

On block finalizaton (`finalize`) the block randomness is refreshed.

If the previously saved block digest contains a VRF then we save it withing the
`AuthorVrfRandomness`. Furthermore, if primary, we deposit the block randomness
into the `UnderConstruction` randomness (the accumulator for the next epoch
randomness).

### Epoch change

Triggered by the `Session` module on when a new session starts.

The session module has hooks to call when a new session begins
(`OneSessionHandler` trait implementation for Babe pallet).

In turn, the `Session` module requires an external component to tell it when a
new session should start. This functionality is provided by Babe via the
`pallet_session::Config::ShouldEndSession`.

So these modules are providing functionalities to each other:

    Babe -(ShouldEndSession)-> Session -(OneSessionHandler)-> Babe

This is not a static dependency and is constructed by the user when it
constructs the runtime.

The `OneSessionHandler::on_new_session` implementation calls the
`enact_epoch_change`. This computes the next epoch randomness using the
collected VRF outputs and push it in the `Digest` log as a `Consensus`
`DigestItem`.

Authorship
----------

There is a generic `Epoch` trait in `sc-consensus-epochs` to deal with epochs.
It offers generics methods to get start/end slots and to increment the epoch.

The `Epoch`trait is implemented by Babe, to contain Babe's specific data such as
authorities and randomness.

The slot based algorithm is generic and used by both Babe and Aura. The only
thing that changes is how the slot is claimed.

There is a timer running calling the `SlotWorker::on_slot` every time there is a
new slot (~6 seconds).

There is also the trait `SimpleSlotWorker` which besids of `on_slot` allows to
implement other stuff such as `epoch_data`, `claim_slot`, `proposer`,
`block_import`, `authorities_len`, etc...

The `SimpleSlotWorker` has already an implementation for the `on_slot` method.
All the other methods are provided by the actual consensus algorithm (e.g.
Babe).

The `on_slot` every time is called, it fetches the epoch data (e.g. in Babe
implementation it uses the Epoch changes structure) and eventually tries to
claim the slot.

The claim slot for Babe tries first to claim a primary slot, if it fails it then
tries to claim a secondary slot.

If we can author a block uses the `proposer` to create the block, i.e. fetches the
transactions from tx pool, executes runtime initialize, finalize, etc...

We construct the `BlockImportParams` calling into the slot worker
implementation. The Babe's `block_import_params()` method creates the Seal, adds
it to the digest log and adds the epoch descriptor to the
`BlockimportParams::intermediate` entry (using `INTERMEDIATE_KEY` in the same
way as done by the `Verifier`).

Finally the proposed block is locally imported.

References
----------

- [w3f article](https://research.web3.foundation/en/latest/polkadot/block-production/Babe.html)
- [fork-tree](../utils/fork-tree.md)
