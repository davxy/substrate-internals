Permissioned Network
====================

- Using `node-authorization` pallet.
- Configure a set of nodes for a permissioned network.
- Each node identified by PeerId. Each PeerId is owned by an AccountId that
  claims it.
- If is a normal PeerId then any user can claim it.
- The claim shall be performed before starting the node (revealing its PeerId).
- Node owner can add/remove connections for their node.
- Well known node connections cannot be changed. They are always connected with
  each other. Instead, you can manipulate connections between a well known node
  and a normal node or between two nodemal nodes and sub-nodes.
- An offchain worker is used by the pallet to configure its connections.
- For non authority nodes offchain worked shall be enabled explicitly via CLI
  flag.

### Keys

Nodes PeerIds and Keys can be generated using subkey:

```
    subkey generate-node-key
```

For Alice's well known node:

```
    # node key
    c12b6d18942f5ee8528c8e2baf4e147b5c5c18710926ea492d09cbd9f6c9f82a

    # peerid, generated from node key
    12D3KooWBmAwcd4PJNJvfV89HwE48nwkRmAgo8Vy3uQEyNNHBox2

    # bs58 decoded peer id in hex:
    0024080112201ce5f00ef6e89374afb625f1ae4c1546d31234e87e3c3f51a62b91dd6bfa57df
```

For Bob's well known node:

```
# node key
6ce3be907dbcabf20a9a5a60a712b4256a54196000a8ed4050d352bc113f8c58

# peer id, generated from node key
12D3KooWQYV9dGMFoRzNStwpXztXaBUjtPqi6aU76ZgUriHhKust

# bs58 decoded peer id in hex:
002408011220dacde7714d8551f674b8bb4b54239383c76a2b286fa436e93b2b7eb226bf4de7
```

For Charlie's NOT well known node:

```
    # node key
    3a9d5b35b9fb4c42aafadeca046f6bf56107bd2579687f069b42646684b94d9e

    # peer id, generated from node key
    12D3KooWJvyP3VJYymTqG7eH4PM5rN4T2agk5cdNCfNymAqwqcvZ

    # bs58 decoded peer id in hex:
    002408011220876a7b4984f98006dc8d666e28b60de307309835d775e7755cc770328cdacf2e
```

For Dave's sub-node (to Charlie, more below):

```
# node key
a99331ff4f0e0a0434a6263da0a5823ea3afcfffe590c9f3014e6cf620f2b19a

# peer id, generated from node key
12D3KooWPHWFrfaJzxPnqnAYAoRUyAHHKqACmEycGTVmeVhQYuZN

# bs58 decoded peer id in hex:
002408011220c81bc1d7057a1511eb9496f056f6f53cdfe0e14c8bd5ffca47c70a8d76c1326d
```

The nodes of Alice and Bob are already configured in genesis storage and serve
as well known nodes. We will later add Charlie's node into the set of well known
nodes. Finally we will add the connection between Charlie's node and Dave's node
without making Dave's node as a well known node.

Custom Types
------------

Settings -> Developer

```
// add this as is, or with other required types you have set already:
{
  "PeerId": "(Vec<u8>)"
}
```

Discovery
----------

If your nodes are not on the same local network, you don't need mDNS and should
use --no-mdns to disable it. If running node in a public internet, you need to
use --reserved-nodes flag to assign reachable nodes when starting Charlie's
node. Otherwise put the reachable nodes as bootnodes of Chain Spec.
