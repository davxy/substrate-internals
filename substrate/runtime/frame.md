FRAME
=====

Components
----------

*Framework for Runtime Aggregation of Modularized Entities*

Set of modules and support libraries for user runtime construction.

The modules are called **pallets** and each one hosts domain-specific logic to
be included in the chain's runtime.

Runtime should not depend on `std` library.

System Library
--------------

Defined by
  [`frame_system`](https://substrate.dev/rustdocs/latest/frame_system/index.html)
  module.

All pallets depend on system library as the basis of substrate runtime.

It has its own `Pallet` struct.

The `Config` trait provides some of the core types used by the overall system
e.g. `Origin`, `BlockNumber`, `AccountId`, `Hash`, `Header`, `Version`, etc.
(also by the native node via the `Block` implementation of `Block` trait),

The pallet has persistent storage for all registered `Account`s, current block
hash, number, events, etc.

Low level functions to access chain storage, verify extrinsics, etc.
(which???)

Executive Library
-----------------

Defined by
[`frame_executive`](https://substrate.dev/rustdocs/latest/frame_executive/index.html)
module.

Orchestration layer for the runtime. Dispatches incoming extrinsic calls to the
respective runtime's modules.

Doesn't have a `Pallet` struct.

Provides:
- Extrinsics validation and application.
- Block initialization, execution, finalization.
- Start off-chain workers.

All these functionalities are typically exposed by the runtime via the runtime
api defined by the user using the `sp_api::impl_runtime_apis!` macro.

Support Library
---------------

Defined by
[`frame_support`](https://substrate.dev/rustdocs/latest/frame_support/index.html)
module.

Provides types, traits and functions that simplify pallets development.

Also provides macros to reduce common boilerplate code (e.g. `#[pallet]`,
`construct_runtime!`, `parameter_types!`).


Pallets provided by Sybstrate
-----------------------------

- Assets: deals with fungible assets.
- Atomic Swap: atomically send funds from origin to a target. Target can cancel
  the operation.
- AURA: extends Aura consensus by managing offline reporting.
- Authority Discovery: used to retrive the current set of authorities, learn its
  own authority id, sign/verify messages to/from other authorities.
- Authorship: tracks current block author and recent uncles.
- BABE: extends BABE consensus by collecting on-chain randomness from VRF
  outputs and managing epoch transitions.
- Balances: functionality for handling accounts and balances.
- Benchmark: common runtime patterns for benchmarking and testing.
- Collective: allows a set of accounts to make their collective feelings known
  through dispatched calls from specialized origins (???).
- Contracts: functionality for the runtime to deploy and execute wasm
  smart-contracts.
- Democracy: democratic system that handles administration of general
  stakeholder voting.
- Elections Phragmen: election module based on
  [sequential Phragmen](https://wiki.polkadot.network/docs/learn-phragmen)
  (???).
- Elections: election module for stake-weigthed membership selection of a
  collective (???).
- Example Offchain Worker: demonstration of concepts, APIs and structures common
  to most offchain workers.
- Example: demonstration of concepts, APIS and structures common to most
  pallets.
- GRANDPA: extends GRANDPA consensus by managing authirity set ready for the
  native code.
- Identity: federated naming system, allowing for multiple registrars to be
  added from a specified origin. Registrars can set a fee to provide
  identity-verification service.
- I'm Online: allows validators to gossip heartbeat transaction with each new
  session to signal that the node is online.
- Indices: allocate indices (short form of address) for newly created accounts.
- Membership: control membership of a set of AccountId's, useful for managing
  membership of a collective.
- Multisig: for doing multi-signature dispatches.
- Nicks: tracks account names on-chain.
- Offences: tracks reported offences.
- Proxy: allow accounts to give permission to other accounts to dispatch types
  of calls from their signed origin.
- Randomness Collective Flip: provides random material based on previous 81
  blocks hashes.
- Recovery: social recovery tool for users to gain access to their accounts if
  their private key (or other auth mechanism) is lost. A threshold M out of N
  friends are needed to give another account access to the recoverable account.
- Scheduler: capability for scheduling dispatches to occur at a specified block
  number or at a specified period.
- Scored Pool: maintains a scored membership pool where the highest scoring
  entities are made members.
- Session: allows validators to manage their session keys.
- Society: economic game which incentivizes users to participate and maintain a
  membership society.
- Staking: manage funds at stake by network maintainers (???).
- Sudo: allows a single account ("sudo key") to execute dispatchable functions
  that require a `Root` origin or nominate a new "sudo key".
- Timestamp: get and set on-chain time (the `set` origin must be an unsigned
  `Inherent`).
- Transaction Payment: basic logic to compute pre-dispatch transaction fees.
- Treasury: provides a "pot" of funds that can be managed by stakeholders in the
  system and a structure for making spending proposals from this pot.
- Utility: helpers for dispatch management.
- Vesting: provides a means of placing a linear curve on an account's locked
  balance. Ensures that there is a lock in place preventing the balance to drop
  below the unvested amount for any reason other thatn transaction fee payment.

References
----------

- https://substrate.dev/docs/en/knowledgebase/runtime/frame#system-library
- https://docs.substrate.io/tutorials/v3/add-a-pallet/
