Forkless Runtime Upgrade
========================

- Use the `set_code` function of System pallet. Designed to consume the max
  block weight to be the only extrinsic in a block.
- The new code is required to increase the `spec_version` and same `spec_name`
  of the RuntimeVersion instance in runtime lib.rs.
- `can_set_code` in `frame_system` pallet performs checks.

### Runtime build

- To build the runtime: `cargo build -p node-template-runtime`.
- The artifact will be in:
  `target/debug/wbuild/node-template-runtime/node_template_runtime.compact.wasm`

### Direct upgrade using Sudo pallet

- open the polkadot.js interface.
- submit an estrinsic using the Sudo pallet.
- Call `sudoUncheckedWeight` passing weight 0 and calling the System
  pallet's `set_code` function.

### Scheduled upgrade using Scheduler pallet

- Use the `schedule` function of Scheduler pallet.
- The schedule origin has been set to `Root (in the Config instance)`
- Refresh polkadot js to make so that extrinsics for the scheduler show.
- NOT WORKING
