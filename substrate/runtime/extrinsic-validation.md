Extrinsics Validation
=====================

### Unsigned transactions

To implement custom logic to validate `Unsigned` transactions:
- implement `ValidateUnsigned` for `Pallet<T>`;
- add the `ValidateUnsigned` entry in `construct_runtime!` for the pallet
  implementing it, e.g.:
    `MyPallet: pallet_mypallet::{Pallet, Call, Event<T>, ValidateUnsigned}`
