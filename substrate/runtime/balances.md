Runtime Balances
================

- Check if simple crowdfund child storage use other accounts. ???

- Charity: an account used as a pot. Account not contraolled via a user keypair
  but directly from the pallet (account private key is unknown).

- Convenience types to get a trait associated type.
    - `type AccountIdOf<T> = <T as frame_system::Config>::AccountId;`
    - `type BalanceOf<T> = <<T as Config>::Currency as Currency<AccountIdOf<T>>>::Balance`

- `Balances` is a concrete type for `Currency` trait. In expanded runtime:

    type Balances = pallet_balances::Pallet<Runtime>;
    ^ results from `construct_runtime!` macro ^

- Balance unit == inteface unit * 10^-12

- Balances pallet `Config` contains `AccountStore`, a struct to hold balances.
  Shall implement `StoredMap<AccountId, AccountData<Balance>>`

- System pallet implements `StoredMap` as well, in example runtime the
  `AccountStore` is set to `System`.
- The system version is more rich of information. Indeed as a `pallet::storage`
  system pallet has a `StorageMap` mapping `AccountId` to `AccountInfo`.

    ```
    AccountInfo {
        nonce,
        consumers,
        providers,
        sufficients,
        data: AccountData
    }
    ```

    - `AccountData` type is part of `System::Config` and is initialized in the
      runtime as `pallet_balances::AccountData<Balance>`

- Difference between `LockableCurrency` and `ReservableCurrency`?
    `LockableCurrency::lock()` takes lock name and an extraordinary withdraw
    reason.
