Runtime Version
===============

(sp-version)

Fields of `RuntimeVersion` struct.

- authoring-version: version of the authorship interface.

- spec-name: runtime identifier.
- spec-version: version of the runtime specification.

- impl-name: implementation of the spec. Ignored by the node.
- impl-version: version of the implementation of the spec. Ignored by the node.

- apis: list of "features" along with their versions.

A node will use native code only if spec-name, spec-version and
authoring-version are equal to the one in the runtime.

Authoring version should always be equal to the native.


### Usage

Typically instances within the runtime sources as `RUNTIME`.

The `apis` entry is set to `RUNTIME_API_VERSIONS`. A `ApisVec` instance, i.e. a
statically allocated (via the `impl_runtime_apis` macro) slice of tuples.
