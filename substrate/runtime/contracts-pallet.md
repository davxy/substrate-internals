Contracts Pallet
================

### Config

Require `parameter_types!`:
- ContractDeposit: deposit that must be placed into the contract's account to
  instantiate it. This is in addition to `pallet_balances::Pallet::ExistentialDeposit`
  The min balance for a contract's account can be queried using
  `Pallet::subsistence_threshold`.
- DeletionWeightLimit: max weight that can be consumed per block for lazy trie
  removal.
- DeletionQueueDepth: max number of tries that can be queued for deletion.
- Schedule: cost schedule and limits.

- Time: Time trait implementation to supply timestamps to contracts through
  `seal_now`.
- Randomness: Randomness trait implementation to supply randomness to contracts
  through `seal_random`.
- Currency: currency in which fees are paid and contract balances are held.
- WeightPrice: to answer to contracts queries about current weight price. Not
  used to compute the actual fee and is only informative.
- WeightInfo: weights of the dispatchables of this module and also used to
  construct a default cost schedule.
- ChainExtension: allows runtime authors to add new host functions for a
  contract to call.

### `claim_surcharge` ???

Allows block producers to claim a small reward for evicting a contract. If a
block producer fails to do so, a regular users will be allowed to claim the
reward.

In case of a successful eviction no fees are charged from the sender. However,
the reward is capped by the total amount of rent that was paid by the contract
while it was alive.

If contract is not evicted as a result of this call,
[`Error::ContractNotEvictable`] is returned and the sender is not eligible for
the reward.

### Expose contract API & RPC

- Implement the runtime API that the `contracts-rpc-runtiime-api` exposes.
- The implementation is performed within the `impl_runtime_api!` macro.
- To allow RPC interactions we shall add the rpc api to the node crate.
- Add it in the `crate_full()` function


INK language
------------

DSL language (Rust subset) to implement contracts in substrate.

INK CLI Installation
- `apt install binaryen`
- `cargo install cargo-contract --vers ^0.14 --force --locked`

### Debugging

- Debugging print macro:`ink_env::debug_println!`.
- Run the node with debugging enabled: `--log error,rungime=debug`
- Substrate errors (`scale::Error`) within the wasm contract sandbox do not
  carry dynamic metadata. This data is instead available when compiled outside
  wasm.
- With `CallBuilder` set `gas_limit` to 0 during development.
