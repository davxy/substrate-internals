Offchain Storage
================

### CLI

- To enable node offchain worker: `--enable-offchain`
- To enable node offchain indexing: `--enable-offchain-indexing true`

### Api Usage

Read/Write to offchain db from offchain worker:

    ```
    let data = StorageValueRef::persistent(&key);
    let val = data.get::<Type>();
    data.set(&val);
    ```

Write to offchain db from runtime:

    ```
    sp_io::offchain_index::set(&key, &data.encoded);
    ```

Example:

Write from runtime (onchain):

    ```
    let val = MyValue;
    sp_io::offchain_index::set(&key, &val.encode());
    ```

Read from offchain worker:

    ```
    let data = StorageValueRef::persistent(&key);
    let val = data.get::<MyValue>();
    ```
