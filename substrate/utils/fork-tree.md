ForkTree
========

Tree-like ordered data with logic for pruning the tree while finalizing nodes.

ForkTree stores several nodes across multiple branches.

Top-level brances are called roots.

The tree has functionality for finalizing nodes, which means that the node is
traversed, and all competing branches are pruned.

It guarantees that nodes in the tree are finalized in order.

Each node is identified by its hash but can be ordered by its number.

In order to build the tree an external function must be provided when
interacting with the tree to establish a node's ancestry.

Implementation Notes
--------------------

Most operations on the tree are performed with depth-first search starting from
the leftmost node at every level.

Because this structure is used in a blockchain context, a good heuristic is that
the node we'll be looking for will likely be in one of the deepest chains (i.e.
the longest ones).

Methods
-------

### Rebalance

- Sort child nodes by max branch depth (decreasing).
- Implementation is recursive.

EpochChanges
============

### Helpers

- `is_descendent_of` checks if a block is descendent of another given their
  hash.
- `descendent_query` is a helper builder to produce a descendent query object
  given the client. This builder can take an optional `current` paramenter. This
  is used if the node we're looking for is not yet in the backend DB and the
  current parameter contains the parent (that is in the DB) of the current node
  we're querying.

### Rebalance

- Forwards call to ForkTree::rebalance

### Epoch Descriptor for Child Of

- Finds the epoch for a child of the given block, assuming the given slot
  number.

- If returns `UnimportedGenesis` epoch, it should be imported into the tree.

### Viable Epoch

- Gets the epoch over which a block is imported. Returns a `ViableEpoch`
  instance.
- Takes a `ViableEpochDescriptor` as parameter.

- Is also used when we change the epoch. The `ViableEpoch` has an `increment`
  method.
    - returns an `IncrementedEpoch` instance. This is a structure that can be
      imported on the underlying `ForkTree`.

### Import

- Adds a new epoch-change, signaled at a given block.
- Takes the `descendent_of_builder`, hash and number of the block signaling the
  next epoch.
- Assumes that the given block is not imported yet (by the backed) but parent
  has. This is why the parent hash is provided as parameter.
    - The `descendent_of` is built using the using as `current` the couple
      (hash, parent_hash).

- on block #1 `import` a special `IncrementedEpoch` entry is returned by
  `ViableEpoch::increment` containing both the epoch #0 (current) and epoch #1
  (next) entries (`ViableEpoch` set to `UnimportedGenesis`).
- Subsequent `IncrementedEpoch` instances will contain only the next epoch.
  (`ViableEpoch` is set to `Signaled`).


### Prune Finalized

- Prune finalized epochs, except for the ancestor of the finalized block.
- Requires a `descendent_of_builder`
- The predicate checks if the finalized slot is greater than or equal the node
  we're checking. If is greater than or...
