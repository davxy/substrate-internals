Import Queue
============

AURA
----

### Block import construction

Construction stack trace

    - cumulus_client_consensus_common::ParachainBlockImport::new()
    - cumulus_client_consensus_aura::import_queue::import_queue()
    - polkadot_parachain::service::rococo_parachain_build_import_queue()
    - <closure>
    - polkadot_parachain:service::new_partial()
        - (the build queue procedure is passed as a closure by the caller)
    - polkadot_parachain::service::start_node_impl
    - polkadot_parachain::service::start_rococo_parachain_node

`aura_build_import_queue` function returns a `BasicQueue` with import fuction
set as

    ParachainBlockImport::new(client.clone())

The `ParachainBlockImport` implements `BlockImport` trait only to overwrite the
fork choice rule in the `BlockImportParams` with a custom `ForkChoiceStrategy`
(the best block is determined by the relay chain).

Then just forwards the import block to the inner `BlockImport` implementation,
i.e. the one implemented by the `Client`.

### Block import path

    - cumulus_client_consensus_common::ParachainBlockImport::import_block()
    - wrapped `BlockImport` implementation called.

DB Layering
-----------

sc_client::Backend {
    sc_client::StorageDb{
        sc_state_db::StateDb {
            sc_state_db::StateDbSync {
                sc_state_db::NonCanonicalOverlay
            }
        }
    }
}

Insert block call stack:

    - sc_state_db::noncanonical::NonCanonicalOverlay::insert()  // <- max siblings 32 check
    - sc_state_db::StateDbSync::insert_block()
    - sc_state_db::StateDb::insert_block()
    - sc_client_db::Backend::try_commit_operation()
    - sc_client_db::commit_operation()
    - sc_client::lock_import_and_run()
