Cumulus
=======

Cumulus is a Substrate extension to simplify parachains development.

Provides XCMP, out-of-the-box collator node setup, embedded client to query the
relaychain, block authorship compatibility.

References
----------

- Cumulus Overview: https://github.com/paritytech/cumulus/blob/master/docs/overview.md
