Ecosystem Networks
==================

Networks full parachains list can be explored via:
- https://polkadot.js.org/apps/#/explorer
- https://parachains.info

Relay Chains
------------

### Polkadot

- Description: Polkadot mainnet
- Currency: DOT
- Governance: community
- Parachains:
    - Statemint - common good parachain for asset management
    - Moonbeam - ethereum compatible smart contracts
    - Acala
    - Astar
    - Parallel
    - ...

### Kusama

- Description: Polkadot testnet
- Currency: KSM
- Governance: community
- Parachains:
    - Statemine - common good parachain for asset management
    - Moonriver - ethereum compatible smart contracts
    - ...

### Rococo

- Description: testnet for parachains, cumulus and related technlogy (e.g. HRMP, XCM)
- Governance: parity (sudo)
- Parachains:
    - Rockmine - common good parachain for asset management
    - Contracts
    - Tick, Trick, Track - first three test parachains (retired)

### Westend

- Description: testnet
- Currency: WND (Westies)
- Governance: parity (sudo)
- Parachains:
    - Westmint - common good parachain for asset management
    - ...

Additional Notes
----------------

Difference between Westend and Rococo:
- [Stackexchange question](https://substrate.stackexchange.com/questions/2736/wider-parachain-testing-westend-or-rococo/2759#2759)
- Rococo is the testnet for all parachains in the Ecosystem
- Rococo has all features available on both Kusama and Polkadot (e.g. HRMP and XCM) and some experimental features (e.g. Beefy)
- To connect to Rococo you have to request a slot by creating an issue [here](https://github.com/paritytech/subport)
- Westend is a network where releasess are finally tested before launched on production on Kusama/Polkadot.
- THUS, LOOKS LIKE WESTEND IS CLOSER TO PRODUCTION

Questions
- With Rococo around, is Westend still relevant? If yes, what is its use case when compared to Rococo?
