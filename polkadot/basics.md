General Concepts
----------------

- Extrinsic. State change coming from outside the system (i.e. inherents or
  transactions).
- Inherent. Extrinsics that are inherently true. Not gossipped and are put
  directly into the block by the block author. Are not signed.

- Runtime. The state transition function. Determines the next state given the
  current one.

- Polkadot Host. Environment in which a runtime module can be executed.
  Parachains must support the Polkadot Host. External chains that do not will
  have to use a bridge.

- Tokenization. Process of replacing sensitive data with non-sensitive data.

- Re-Genesis. Process of exporting the current chain state and create a new
  chain thet builds on it. Chain should be stopped. Can be viewed as a fork.

- Teleport. Send asset from account on one chain to one in a different chain.
  Burning on a chain and minting in another. Currently not supported by Polkadot
  mainnet, Rococo testnet and their respective parachains.
- Witness. Cryptographic proof statement of data validity.
- Hard Spoon. New blockchain that inherits the state of an existing blockchain.

- Planck. Polkadot balance unit. 1 DOT = 10^10 Plancks. Internally balances are
  held in Plancks.

- Mainnet. Fully functional and acting chain that runs its own network.
- Testnet. Experimental network where development takes place.

Governance
----------

- Governance. A governance system controlled by mechanisms on the blockchain.
  Allows decisions to be made transparently. Mechanisms: majority voting,
  adaptive quorum biasing, identity-based quadratic voting, etc.
- Council. On-chain entity consisting of several accounts. Acts as
  representative for "passive" (no token holders) stakeholders. Tasks:
  controlling the treasury, proposing sensible referenda, cancelling malicious
  referenda, electing the technical committee.
- Treasury. Pot of funds collected through transaction fees, slashing, staking
  inefficiencies, etc. Funds spent making a spending proposal that should be
  approved by the Council.
- Referenda. Stake-based voting schemes (democracy pallet). Each referenda has a
  proposal in the form of privileged function call in runtime. Always binary.

- Community Queue. Proposals originating from individual accounts waiting to
  become referenda.
- External Queue. Proposals originating from Polkadot council waiting to become
  referenda. To be proposed a referendum shall be approved by at least 51%
  members of the council.

- Start types: public, council, part of enactment of prior referendum, emergency
  from technical committee.
- Tabling. Bringing a proposal to a vote via a referendum.
- Enactment delay. Time between proposal approval and changes being enacted.
  Variable debending on how referenda is started.

- Adaptive Quorum Biasing. Functions as a lever that the council uses to alter
  the effective super-majority required to make it easier or more difficult for
  a proposal to pass.
- Positive turnout bias (used for public proposals). A super-majority of aye
  votes is requried to carry at low turnouts. As turnout approaches 100% it
  becomes a simple majority. With low turnouts nay votes counts more.
- Negative turnout bias (used for council unanimous proposals). A super-majority
  of nay votes is required to carry at low turnouts. As turnout approaches 100%
  it becames a simple majority. With low turnouts nay votes counts less.

- Lock period. Funds locking for increased vote multiplier. While token is
  locked you can use it for voting and staking.

Consensus
---------

- NPOS. Polkadot uses Phragmen method to allocate stake to nominees.
- Bonding. tokens are frozen in exchange of some service or benefit.

- Validator. Produce blocks on the Relay chain. Also accept proofs of valid
  state transition from collators. In return they receive rewards.
- Nominator. Bond their stake to a particular validator in order to help them
  get into the active validators set. In return receive a portion of reward.

- Oversubscribed. If more that max number of nominators nominate a validator.
  Only the top staked nominators are paid rewards. Current value 256.
- Active Nomination. Validator that a nominator has selected to nominate and is
  actively validating this era.
- Equivocation. Providing conflicting information to the network. E.g. BABE
  creating multiple blocks in the same slot or GRANDPA signing multiple
  conflicting chains.

- Epoch. Time duration in BABE protocol that is broken into smaller time slots.
  Each slot has at least one slot leader who has the right to propose a block.
  In Kusama has the same duration of a session.
- Session. A period with a constant set of validators. Validators can join/exit
  at session change.
- Session Certificate. Message containing signature of the concatenation of all
  the session keys. Signed by the controller. (Is this pallet set-keys)?
- Session Key. Hot keys used for network operations by validators (e.g. sign
  GRANDPA commit messages).
- Era. A number of sessions, which is the period the validator set is
  recalculated and rewards are paid out (~25h on Polkadot, ~6h on Kusama).

- Capacity. Max number of nominators signalling the intent to nominate a
  validator (and this potentially activelly nominate the validator in the next
  session).
- Commission. Where validators can set a variable commission rate, which is
  initially subtracted from the toral rewards that validator is entitled to (for
  that period), where the commission determines the rate of distribution for the
  remaining rewards set out fot the nominators backing that validator.

- Fast Tracked Referenda. Urgent referenda requested by technical committee.
  Is possible to have at most one referenda and one fast tracked referenda
  active at the same time.

Translations
------------

- Auction: asta.
- Turnout: partecipazione, affluenza, afflusso (in voting tokens, does not
  include conviction).
- Enactment: varo, attuazione.
- Tallying: conteggio (tally: conteggiare).
- Quorum: minimum members required.
- Veto: opposizione preculusiva di un soggetto ad un atto legislativo,
  giudiziario o politico. Expressa con voto contrario.
