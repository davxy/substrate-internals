Staking
=======

- Nominators captures part of the inflation along with validators.
- Stake associated to the nominated validators is distributed by the algorithm
  automatically.
- Rewards are higher if the nominator bets on the validator with less at stake.
- Validators are rewarded equally regardless of the stake.
- Slash is proportional to the validator stake.
- Slashing motivations (by severity):
    1. offline validator (from 2nd missed slot) => ~0.1% slash
    2. double signing block (blocks conflicting with each other) => ~10% slash
        May happen if you have 2 nodes using the same keys for redundancy.
    3. attack to the network, e.g. by voting on a chain that has already been
       finalized => ~200%
        May happen if you hack the software.

- Reputation of validators?

References
----------

[Official](https://wiki.polkadot.network/docs/maintain-guides-how-to-nominate-polkadot)
[Guide](https://earnstash.medium.com/how-to-stake-polkadot-fc0a90570d5f)
