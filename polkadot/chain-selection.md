Chain Selection
===============

Chain selection is the component that figures out what is the best chain, it
will use specific parachain logic to make sure that we only build on top of
blocks whose parachain blocks have already been backed.

Provides safety for relay chains by taking approval votes and disputes into
account. It doesn't choose, as best, blocks that are containing unconfirmed
parachain blocks.

It offers safe way to get:
- leaves
- best chain
- finality target

Implementation details ----------------------

(node/service/src/relay_chain_selection.rs)

`SelectRelayChain` implements the `SelectChain` trait thus it is directly usable
by Substrate components.

The node is set to use this chain selection (via "overseer",
`SelectRelayChain::new_with_overseer`) if the node is a collator or authority
(and has a local keystore). Else it will just use the "common"
`sc_consensus::LongestChain`.

An imported block is added to the leafs only if `inherited_viability` is none.
parent.non_viable_ancestor_for_child(). The method to add a new block is
`chain-selection::add_block`.

### Database details

Column within the parachains db: `COL_CHAIN_SELECTION_DATA`

Data prefixes:
- `BLOCK_ENTRY_PREFIX`
- `BLOCK_HEIGHT_PREFIX`
- `STAGNANT_AT_PREFIX`
- `LEAVES_KEY`  The first element is the "best" chain

### Overseer messages

The `chain_selection::run_until_error` has a loop to handle messages received by
overseer (e.g. import of new blocks).
