Parachain Consensus
===================

Grandpa is accountably Safe: something that cannot be reverted unless a number
of validators (+33%) colluded. And if this happens then we can identify the
malicious actors and eventually slash them.

- Backing: how new parachain blocks are submitted to relay chain and how
  validators are assigned to each parachain.
- Availability: make sure that parachain block is available to all validators.
- Approvals: validators randomly selects themselves to check parablocks.
- Disputes: if there is disagreement between validators.

### Parachain Blocks Execution

Parachain Validation Function (PVF) is a wasm blob in charge of checking block
validity.

Inputs:
- Context: the last block that the relay chain recognizes as the valid.
- Block: the transactions set to be executed.
- State: the state of the parachain.

Output: true or false.

This is executed both on the backing phase and on the approval phase.

If you dispute something that in the end is good, then there is a penality.

### Backing

We partition validators and assign them to a different parachain.

Parachain collator will forward the block to a relay chain validator.

The validator then forwards it to the other validators assigned to the same
parachain. The size of the quorum is not important (currently 50%).

A malicious validator (or group) can't censor entirelly a parachain because
actually the validators assigned to a parachain always change (~each min).

There is no slashing actually for this kind of behaviour. (Collator provides
liveness, Relay chain provides safety)

### Availability

Relaychain validators do not keep the full relay chain block.

Erasure Coding is a technique to split data into pieces such that we are
allowed to reconstruct the whole data if some of the pieces are lost.

If we have N nodes we distribute the data such that we can recover the data
with less than 1/3 of malicious nodes.

Only when at least 2/3+1 of the validators has their piece we can eventually
build the block.

### Approval

We have a random set of validators re-run the validation functions.

Similar to rollups in Ethereum. It is an optimistic system, someone posts
something, waits, and if nobody objects then it is assumed to be good.

Divided in two phases: assignment and approval.

#### Assignment

We split time in "tranches" (e.g. in kusama we have 75 tranches in 20 min).

A set of validators are required to check a parachain block during a tranche. A
validator is assigned to a tranche using a VRF output.

The thing is constructed so that most of the time enough validators are
assigned to tranche 0. So that trace 0 is already enough and we don't require
to wait for tranche 2.

#### Approval

Finally the validators recover the block data and checks it (again) using the
PVF. Here eventually they start a dispute.

Validators sign for a tranche only if required (given the result of the previous
tranche, i.e. if not sufficient).

For the most part the timestamp is not important. Is more important that the
time elapses with the same pace. If this doesn't happen then the worst case is
that more validators jumps in, but is not a security concern.

### Block inclusion

Finally the selected relaychain validator will include the block data (from other
parachains as well) in the next relaychain block.

In practice the data is include via an inherent, a special type of extrinsic
submitted by the block producer.
