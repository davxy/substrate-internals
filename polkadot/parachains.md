Parachains
==========

General Concepts
----------------

### Keywords

- Relaychain. blockchain system in charge of maintaining the security of
  parachains by validating their blocks.
- Parachain. Parallelized chain.
- Parathread. Chains that bid on a per-block basis to be included in the relay
  chain. Can connect on a "pay as you go" basis. All parachains starts as
  parathreads.
- ParaID. Parathread identifier.
- Common Good Parachain. A parachain that does not win a parachain slot due to
  winning a slot auction, but for providing a useful service to the network.

- Collator. Full node on the parachain which collects parachain transactions
  in a block candidate and produces state transition proofs for the validators
  on the relaychain. Can also send/receive XCMP messages.

- Bridge. a parachain node that acts as an intermediary between the relaychain
  and an external chain.

- Lease Period. Amount of time during which the parachain can connect to Relay.
- Auction. How a parathread wins a slot to become a parachain (a variant of
  "Candle Auction" is used).
- Crowdloan. mechanism for potential parachains to temporarily source tokens
  from the community to win an auction. The funds are returned to the voters
  after the lease period ends.

- Proof of Validity. Produced by parachain collators. Based on this proof and
  parachain registry, relaychain validators can verify that a parachain has
  properly executed his state transition function.
- Shared Security. All chains are equally secured. Achieved by placing PoV of
  parachain blocks into the relaychain state. To revert the finality of a
  single parachain, an attacker whould need to attack the entire relaychain.
- Sealing. Process of adding a block to the relaychain. Finalization is a
  separate process. Blocks are finalized some time after they are sealed.

- Attestation. A message that validators broadcast that says whether a parachain
  candidate block is valid or not.

- Duty Roster. Lookup table that specifies the job of a particular relaychain
  validator (i.e. to attest to the validity of a specific parachain block).
  The duty roster routinely shuffles the validators set into different subsets
  per parachain.

- Dispute. Negative votes on a parachain block from the validators in relay
  chain. Those on the wrong side will be slashed. We require the PoV to remain
  available in validators for a period of time during which it can be checked.

- XCM (Cross Chain Message). Message sent from one parachain to another through
  a dedicated channel. Transported via XCMP.
- XCMP (XCM Protocol). Allows parachains to send messages to each other.
- HRMP (Horizonal Relay-routed Message Passing). Precursor of XCMP. Mimics
  interface and semantics of XCMP except it stores all messages in the Relay
  chain storage, thus is more expensive.
- Vertical Message Passing. Two types: downward (DMP) are from relaychain to
  parachain and upward (UMP) are from parachain to relay. DMP may also originate
  from another parachain via HRMP.
- Watermark. In parachain messaging scheme, it is the min processed send-height
  of the received parachain. All messages on all channels that are sent to
  this parachain at or before the watermark are guaranteed to be processed.
  (TODO: NOT CLEAR)

### Slots

Limited number of slots (~100), some of them are reserved to run parathreads.

Acquisition is granted by governance (e.g. for common good), auctions, parathreads.

Common Good Parachain usually do not have their own economic models and help
remove transactions processing from the relaychain.

During auctions parachain teams can bid with their own relaychain tokens or
source them from the community using the crowdload functionality.

For the duration of the slot the bid tokens are locked up. Thus a parachain tend
to reserve the slot only for the required periods.

At the end of the lease period, tokens from the auction are returned and if slot
is not extended a parachain becomes a parathread.

### Economy

Parachain collators are generally paid in the local parathread currency.
However, relaychain transacts with its native token only (e.g. DOT). Collators
should thus submit block candidates with an associated bid in DOT.

A parachain can swap a slot with a parathread, so that the parathread becomes a
full parachain and parachain becomes a parathread.

A parachain can stop being a parachain anytime. Becoming a parathread.

### Block Production

Collators only need to produce blocks and forward on relaychain for validation,
they act as untrusted block producers.

Block finality is always provided by the relaychain, which guarantees valid
state transitions using the parachain PoV.

### Validators Drop

Minimal safe ratio of relaychain validators per parachain is 5:1.

If a validator group goes offline, the parachains whose validator groups are too
small to validate a block will skip these blocks. Their block production will
slow down until the situation is resolved.

Collators can monitor relaychain state, thus if required they can stop and
restart producing block candidates depending on the situation.

### Common Good chains

Two main types:
- System level chain: moves functionality from relaychain minimizing
  administrative use of relaychain (e.g. for governance, crowdloans, ...).
- Public utility chain: creates a new service that doesn't exist yet and which
  is opinionated (bridges, smart contract platforms, assets chains, ..).

Most common good chains use the relaychain token as native token. For example,
"Statemint" trusts messages about balances from the relaychain and vice versa.

Identifiers schema:
- Common good parachain ID < 1999
- System parachain ID < 1000

### Parathreads

Parathreads compete block by block to have their block candidate included, they
pay per block.

On Polkadot, DOTs from parathread bids will be split 80% to Treasury and 20% to
block author, i.e. the same split as the normal transaction fees.

Block Validation Protocol
-------------------------

Collators are selected with logics that are specific to each parachain (for
example PoS, PoW, etc).

### Connection Establishment

Each parachain has a set of relaychain validators to accept and validate blocks
and move them towards finality.

Validators use the Babe VRF to determine which parachain validate next. This
changes on a block basis.

Once a validator knows its parachain, it finds collators from that parachain to
establish a connection.

A single honest collator is sufficient.

### Block Production

Collator collects transactions and prepare a block.

Parachain state modification is captured by recording the modifications enacted
during block execution to the blockchain state trie.

PoV is constituted by state trie root, the recorded access to trie leaves and
hashes needed to reconstruct the merkle tree root.

Collator sends the PoV to a relaychain validator.

More correctly, the exact PoV construction depends on the parachain and is
opaque, the parachain has previously registered a validation function within the
relay chain that is able to interpret the PoV.

### Validation

PoV is gossipped among the other validators assigned to the same parachain and
if more that 50% of them agree that the state transition is valid then they
prepare to announce its validity.

A candidate receipt (this is what it goes into the relaychain block) and an
"erasure coding" are constructed and sent to all validators in the network.

Only the candidate receipt and erasure coding is spread over the full relaychain
network.

Polkadot doesn't guarantee a valid state, it only guarantees a valid execution
of the parachain validation function given the PoV.

Validators have the wasm validation function of all the parachains.

### Candidate Receipts

Validator signs the parachain-id, collator-id, collator signature, hash of
parent block's candidate receipt, Merkle root of block's PoV erasure coding,
Merkle root of any outgoing messages, hash of the PoV, state root of parachain
before block execution, state root of parachain after block execution, any fees.

### Erasure Coding

Is a bigger message of block information and PoV but encoded such that it can be
reconstructed with a fraction (1/3) of the bigger message.

The erasure coding is divided in N chunks, with N the number of validators, and
stored within a Merkle tree. The root of the tree is included within the
candidate receipt.

One "chunk" of erasure coding plus the candidate receipt is distributed to each
validator of the relaychain.

### Relaychain Block Construction

Relaychain next block author will only include candidate receipts that have
their parent candidate receipt in an earlier relaychain block (this is to ensure
that parachain follows a valid chain) and for which it has an erasure coding
chunk.

Receipts are then inserted in relaychain state via inherents extrinsics.

### Return of the Collators

To reconstruct their block, collators start asking validators for the chunks
that correspond to their chain and report unavailability.

Having spreaded the block and Pov using erasure coding, we prevent a form of
attack where a collator proposes a valid block, then it goes offline.
Without erasure coding, in this case the parachain has no way to know what was
the proposed block and it is stuck forever since it can never made another state
transition without first executing the "lost" block. We must be sure that
whatever data is finalized is also available.

### Fishermans

Nodes that can ask for PoV and blocks, execute it and report invalidity.
Collators are in excellent position to be fisherman.

Fisherman put some relaychain tokens at stake to have this power. Invalid
reports are slashed.

Fisherman send alerts but validators have the ultimate responsibility.

### Secondary Checks (NOT CLEAR)

Before a block is finalized on the relaychain a second check is performed.

Validators are randomly selected, not the same validators that were chosen
before.

Only after the secondary check has been done the validators cast a pre-vote or
pre-commit for a block in GRANDPA. Again we require 1/3.

### Best Practices

Always have one or more relaychain validators than the total connected
parachains.

Implementation Details
----------------------

### Details about relaychain

A "backable" candidate is a parachain block received by a validator and that
should be approved by a quorum.

Once enough signed statements are received by the assigned validators the
block is backed in the relaychain with "pending availability".

It is not considered included as part of the parachain until is proven
"available". The candidate should be proven available within a fixed amount of
the time, otherwise is discarded.

Once a block is "available" it is still "pending approval". In this state the
relaychain can already start accepting children for that block.

The approval process allows us to rollback if a block is found to be invalid
(invalidating descendents as well). Blocks remain in this state for a time
window (secondary check window). During this phase random validators re-run the
validation function, in the end the block is rejected or approved.

### Database

Relaychain validators have a physically separated database for parachains data,
stored within the same folder of the main database (db-root ++ "parachains").

Parachains db columns (`node/service/src/parachain_db`):
- `COL_AVAILABILITY_DATA`: availability store for data;
- `COL_AVAILABILITY_META`: availability store for meta info;
- `COL_APPROVAL_DATA`: approval voting for data;
- `COL_CHAIN_SELECTION_DATA`: chain selection data;
- `COL_DISPUTE_COORDINATOR_DATA`: dispute coordinator data.

References
----------

- https://w3f.github.io/parachain-implementers-guide/index.html
- https://polkadot.network/blog/the-path-of-a-parachain-block/
- [Parachain Block Path (video)](https://www.crowdcast.io/e/polkadot-path-of-a-parachain-block?utm_source=profile&utm_medium=profile_web&utm_campaign=profile)
- Cumulus Overview: https://github.com/paritytech/cumulus/blob/master/docs/overview.md
