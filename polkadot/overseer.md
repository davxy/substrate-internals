Overseer
========

Message passing system used by Polkadot.

Startup
-------

Within the `service::new_full`, the overseer is instanced via the
`OverseerGen::generate` function.

Instanced via `OverseerGen` trait implementation for `RealOverseerGen`.

The `generate` method calls `prepared_overseer_builder`.

`OverseerGenArgs` have many elements, importants:
- `chain_selection_config` and `dispute_coordinator_config`: each having a field
  indicating the parachain db column that are using (`col_data`).
- `runtime_client`: a clone of the `Client`.
- `parachains_db`: backend to read/write special database dedicated to
  parachains related data.

The `service::new_full` function also creates and start two tasks:
`forward_events` and `run`.

### overseer::forward_events

Glues together overseer and blochchain events, in practice listens on Client's
`finality_notification_stream` and `import_notification_stream`.

Example: block import execution flow:

- when a new block import is notified via the stream, the
  `handle.block_imported()` is called.

- `Handle::block_imported()` sends a `Event::BlockImported` event to overseer
  (`overseer::run` has the rx end of `Handle`)

### overseer::run

Manages the events and broadcasting the messages to all subsystems.

Example: `Event::BlockImported` received

- The `block_imported` handler is invoked;
- On import leaf becomes active while the parent is deactivated;
- The hash is stored within the `known_leaves` LRU cache.

If the import operation activated or deactivated some leaves then there is an
`ActiveLeavesUpdate` instance to process.
- The `broadcast_signal(OverseerSignal::ActiveLeaves(update))` is called;
- This message is received by all the subsystems (for example the
  chain-selection one)

Active Leaves
--------------

A leaf becomes active when is imported (calls `on_head_activated`).

Overseer maintains:
- a list of active leaves Vec<(Hash, BlockNumber)>
- a list of receivers waiting for some leaf to become active
  (`activation_external_listeners`)

Subsystems
----------

### Creation / Startup

`node::overseer::Overseer` structure construction assisted via `overlord`
procedural macro.

Takes care to call `start` function of each overseer subsystem.

The message type each subsystem can handle is declared within the [overlord]
macro. Messages are received along with the generic `Context`.
